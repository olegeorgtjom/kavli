﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using QCommon.Extensions;
using Qis.Common.Metamodel;
using Qis.Common.Metamodel.DialogDescriptions;

namespace Qis.Module.Scripts
{
    public class TabDisplayName
    {
        #region Public Properties

        public string DisplayName
        {
            get;
            set;
        }

        public string Name
        {
            get;
            set;
        }

        #endregion
    }

    public static class DynamicMetamodel
    {
        #region Constants

        private const string c_dlgExtension = ".xdlg";

        #endregion

        #region Fields

        private static readonly IEnumerable<string> s_defaultTabsOrder =
            new[]
            {
                "Audits",
                "SubDescribe",
                "SubAssociate",
                "SubRevision",
                "SubPrcRisks",
                "SubCirculation",
                "SubStatus",
                "SubProjectStatus",
            };

        #endregion

        #region Private Methods

        private static void AddSubTemplate(
            IMetamodel metamodel,
            TemplateDefinition existingTemplate,
            TemplateDefinition parentTemplate,
            DialogDescription parentDialog,
            IEnumerable<TabDisplayName> displayNames)
        {
            var mergedAttributes = MergeAttributes(
                parentTemplate.Attributes.Cast<AttributeDefinitionBase>(),
                existingTemplate.Attributes.Cast<AttributeDefinitionBase>());

            metamodel.Set(
                new TemplateDefinition(
                    existingTemplate.Name,
                    existingTemplate.Description,
                    mergedAttributes,
                    existingTemplate.InheritanceList
                        .Concat(parentTemplate.Name.AsCollection()),
                    existingTemplate.ShareName,
                    existingTemplate.IconData,
                    existingTemplate.Hidden,
                    existingTemplate.Abstract));

            if (parentDialog == null)
            {
                return;
            }

            var existingDialog = GetDialogOrNull(metamodel, CreateDialogName(existingTemplate.Name));
            if (existingDialog == null)
            {
                return;
            }

            var tabs = existingDialog.Tabs.ToDictionary(tab => tab.Name);
            parentDialog.Tabs
                .DoForEach(
                    parentTab =>
                    {
                        if (!tabs.ContainsKey(parentTab.Name))
                        {
                            tabs.Add(parentTab.Name, parentTab);
                        }
                    });

            if (displayNames != null)
            {
                ChangeTabsDisplayName(tabs, displayNames);
            }

            metamodel.Set(
                new DialogDescription(
                    existingDialog.Name,
                    existingDialog
                        .InheritanceList
                        .Concat(parentDialog.Name.AsCollection()),
                    tabs.Values,
                    existingDialog.Layouts));
        }

        private static void ChangeTabsDisplayName(
            IDictionary<string, DialogTab> tabs,
            IEnumerable<TabDisplayName> displayNames)
        {
            displayNames
                .Where(dn => !string.IsNullOrEmpty(dn.Name) && !string.IsNullOrEmpty(dn.DisplayName))
                .DoForEach(
                    dn =>
                    {
                        DialogTab existingTab;
                        if (!tabs.TryGetValue(dn.Name, out existingTab))
                        {
                            return;
                        }

                        if (existingTab.DisplayName == dn.DisplayName)
                        {
                            return;
                        }

                        tabs[dn.Name] = new DialogTab(
                            existingTab.Name,
                            dn.DisplayName,
                            existingTab.UseMode,
                            existingTab.Layouts);
                    });
        }

        private static string CreateDialogName(string templateName)
        {
            return templateName.EndsWith(c_dlgExtension) ? templateName : templateName + c_dlgExtension;
        }

        private static string ExtractTemplateName(string dialogName)
        {
            return Path.GetFileNameWithoutExtension(dialogName);
        }

        private static DialogDescription GetDialogOrNull(IMetamodel metamodel, string dialogName)
        {
            return metamodel.FindDialogDirect(dialogName);
        }

        private static IEnumerable<DialogDescription> GetDialogs(
            IMetamodel metamodel,
            IEnumerable<string> dialogNames)
        {
            return dialogNames
                .Select(dn => GetDialogOrNull(metamodel, dn))
                .Where(d => d != null)
                .Where(d => d.Name != metamodel.DefaultDialog)
                .ToList();
        }

        private static TemplateDefinition GetTemplateOrNull(IMetamodel metamodel, string templateName)
        {
            return metamodel.FindTemplateDirect(templateName);
        }

        private static IEnumerable<AttributeDefinitionBase> MergeAttributes(
            IEnumerable<AttributeDefinitionBase> existingAttributes,
            IEnumerable<AttributeDefinitionBase> newAttributes)
        {
            var result = (newAttributes ?? Enumerable.Empty<AttributeDefinitionBase>())
                .Where(a => !a.IsPredefined)
                .ToDictionary(a => a.Name);

            existingAttributes
                .Where(a => !a.IsPredefined)
                .DoForEach(
                    a =>
                    {
                        if (!result.ContainsKey(a.Name))
                        {
                            result.Add(a.Name, a);
                        }
                    });

            return result.Values;
        }

        private static LayoutTemplate RemoveExcessiveSpaces(LayoutTemplate layout)
        {
            string[] lineFeeds = {"\n", "\r\n"};

            var content = layout.Content.Split(lineFeeds, StringSplitOptions.None);

            return new LayoutTemplate(
                layout.Type,
                content
                    .Select(l => l.Trim())
                    .Join("\n"));
        }

        private static DialogTab RemoveExcessiveSpaces(DialogTab tab)
        {
            return new DialogTab(
                tab.Name,
                tab.DisplayName,
                tab.UseMode,
                tab.Layouts
                    .Select(RemoveExcessiveSpaces)
                    .ToList());
        }

        private static void RemoveSubTemplate(
            IMetamodel metamodel,
            TemplateDefinition existingTemplate,
            string parentName,
            DialogDescription parentDialog)
        {
            // We should not delete attributes, only representation
            metamodel.Set(
                new TemplateDefinition(
                    existingTemplate.Name,
                    existingTemplate.Description,
                    existingTemplate
                        .Attributes
                        .Cast<AttributeDefinitionBase>()
                        .Where(a => !a.IsPredefined),
                    existingTemplate
                        .InheritanceList
                        .Except(parentName.AsCollection()),
                    existingTemplate.ShareName,
                    existingTemplate.IconData,
                    existingTemplate.Hidden,
                    existingTemplate.Abstract));

            if (parentDialog == null)
            {
                return;
            }

            var existingDialog = GetDialogOrNull(metamodel, CreateDialogName(existingTemplate.Name));
            if (existingDialog == null)
            {
                return;
            }

            var tabs = existingDialog.Tabs.ToDictionary(tab => tab.Name);
            parentDialog.Tabs
                .DoForEach(
                    parentTab =>
                    {
                        if (tabs.ContainsKey(parentTab.Name))
                        {
                            tabs.Remove(parentTab.Name);
                        }
                    });

            metamodel.Set(
                new DialogDescription(
                    existingDialog.Name,
                    existingDialog.InheritanceList.Except(CreateDialogName(parentName).AsArray()),
                    tabs.Values,
                    existingDialog.Layouts));
        }

        private static IEnumerable<DialogTab> ReorderTabs(
            IEnumerable<string> tabOrder,
            IEnumerable<DialogTab> tabs)
        {
            #region Argument Checks

            if (tabOrder == null)
            {
                throw new ArgumentNullException("tabOrder");
            }

            if (tabs == null)
            {
                throw new ArgumentNullException("tabs");
            }

            #endregion

            IEnumerable<DialogTab> orderedPart = tabOrder
                .Join(tabs, s => s, t => t.Name, (s, t) => t)
                .ToList();

            IEnumerable<DialogTab> unorderedPart = tabs
                .Except(orderedPart)
                .ToList();

            return orderedPart.Concat(unorderedPart);
        }

        #endregion

        #region Public Methods

        /// <summary>
        ///     Extends target template with parent's attributes.
        ///     This also affects default dialog of the template.
        /// </summary>
        /// <param name="metamodel">
        ///     Instance of <see cref="IMetamodel" /> to make changes through.
        /// </param>
        /// <param name="templateName">
        ///     Name of template to extend.
        /// </param>
        /// <param name="parentName">
        ///     Name of parent template to take attributes from.
        /// </param>
        public static void AddTemplateParent(
            IMetamodel metamodel,
            string templateName,
            string parentName,
            IEnumerable<TabDisplayName> displayNames = null)
        {
            #region Argument Checks

            if (metamodel == null)
            {
                throw new ArgumentNullException("metamodel");
            }

            if (templateName.IsBlank())
            {
                throw new ArgumentException("Template name cannot be blank.", "templateName");
            }

            if (parentName.IsBlank())
            {
                throw new ArgumentException("Parent template name cannot be blank.", "parentName");
            }

            #endregion

            var existingTemplate = GetTemplateOrNull(metamodel, templateName);
            if (existingTemplate == null)
            {
                return;
            }

            if (existingTemplate.InheritanceList.Contains(parentName))
            {
                return;
            }

            var parentTemplate = GetTemplateOrNull(metamodel, parentName);
            if (parentTemplate == null)
            {
                return;
            }

            var parentDialog = GetDialogOrNull(metamodel, parentName + c_dlgExtension);

            AddSubTemplate(metamodel, existingTemplate, parentTemplate, parentDialog, displayNames);

            // I would like to optimize it forever, by checking whether template is abstract or hidden.
            // But we can have normal template Class, which is parent for normal template Vision in UML metamodel.
            // So we need to make this search quick in future.
            if (existingTemplate.Hidden) // Don't use Abstract here, it lies.
            {
                metamodel
                    .Templates
                    .Where(t => t.InheritanceList.Contains(existingTemplate.Name))
                    .Where(t => !t.InheritanceList.Contains(parentTemplate.Name))
                    .DoForEach(t => AddSubTemplate(metamodel, t, parentTemplate, parentDialog, displayNames));
            }
        }

        /// <summary>
        ///     Gets all kind of layouts in target dialog.
        /// </summary>
        /// <param name="metamodel">
        ///     Instance of <see cref="IMetamodel" /> to make changes through.
        /// </param>
        /// <param name="dialogName">
        ///     Full name of the dialog including role and/or customization (e.g. role/customization/name.xdlg).
        /// </param>
        /// <returns>
        ///     List of dialog layouts; otherwise empty list.
        /// </returns>
        public static IEnumerable<LayoutTemplate> GetDialogLayouts(
            IMetamodel metamodel,
            string dialogName)
        {
            #region Argument Checks

            if (metamodel == null)
            {
                throw new ArgumentNullException("metamodel");
            }

            if (dialogName.IsBlank())
            {
                throw new ArgumentException("Dialog name cannot be blank", "dialogName");
            }

            #endregion

            var dialog = GetDialogOrNull(metamodel, dialogName);
            return dialog == null ? Enumerable.Empty<LayoutTemplate>() : dialog.Layouts;
        }

        /// <summary>
        ///     Modifies (if found) or creates new attribute definition in target template.
        /// </summary>
        /// <param name="metamodel">
        ///     Instance of <see cref="IMetamodel" /> to make changes through.
        /// </param>
        /// <param name="templateName">
        ///     Name of template to change.
        /// </param>
        /// <param name="attribute">
        ///     New definition of the attribute.
        /// </param>
        public static void ModifyAttribute(
            IMetamodel metamodel,
            string templateName,
            AttributeDefinitionBase attribute)
        {
            #region Argument Checks

            if (metamodel == null)
            {
                throw new ArgumentNullException("metamodel");
            }

            if (templateName.IsBlank())
            {
                throw new ArgumentException("Template name cannot be blank.", "templateName");
            }

            if (attribute == null)
            {
                throw new ArgumentNullException("attribute");
            }

            #endregion

            var exisitingTemplate = GetTemplateOrNull(metamodel, templateName);
            if (exisitingTemplate == null)
            {
                return;
            }

            metamodel.Set(
                new TemplateDefinition(
                    exisitingTemplate.Name,
                    exisitingTemplate.Description,
                    exisitingTemplate
                        .Attributes
                        .Cast<AttributeDefinitionBase>()
                        .Where(a => !a.IsPredefined)
                        .Where(a => a.Name != attribute.Name)
                        .Concat(attribute.AsArray()),
                    exisitingTemplate.InheritanceList,
                    exisitingTemplate.ShareName,
                    exisitingTemplate.IconData,
                    exisitingTemplate.Hidden,
                    exisitingTemplate.Abstract));
        }

        /// <summary>
        ///     Modifies (if found) or creates new dialog definition.
        /// </summary>
        /// <param name="metamodel">
        ///     Instance of <see cref="IMetamodel" /> to make changes through.
        /// </param>
        /// <param name="dialogName">
        ///     Full name of the dialog including role and/or customization (e.g. role/customization/name.xdlg).
        /// </param>
        /// <param name="parentNames">
        ///     List of parent dialogs to inherit from (including role and/or customization).
        /// </param>
        /// <param name="displayNames">
        ///     List of <see cref="TabDisplayName" />. Used to rename tabs (usually inherited) in dialog. May be null.
        /// </param>
        /// <param name="tabs">
        ///     List of explicit tab definitions including layout (may be extended by inheritance).
        /// </param>
        /// <param name="layouts">
        ///     List of dialog layouts.
        /// </param>
        public static void ModifyDialog(
            IMetamodel metamodel,
            string dialogName,
            IEnumerable<string> parentNames,
            IEnumerable<TabDisplayName> displayNames,
            IEnumerable<DialogTab> tabs,
            IEnumerable<LayoutTemplate> layouts)
        {
            #region Argument Checks

            if (metamodel == null)
            {
                throw new ArgumentNullException("metamodel");
            }

            if (dialogName.IsBlank())
            {
                throw new ArgumentException("Dialog name cannot be blank", "dialogName");
            }

            #endregion

            var initialTabs = (tabs ?? Enumerable.Empty<DialogTab>());
            var resultDialogLayouts = (layouts ?? Enumerable.Empty<LayoutTemplate>()).ToDictionary(l => l.Name);
            var resultTabs = initialTabs.ToDictionary(t => t.Name);

            var template = GetTemplateOrNull(metamodel, ExtractTemplateName(dialogName));
            if (template == null)
            {
                return;
            }

            // List of parent dialogs can be bigger then template parents.
            var validParentNames = (parentNames ?? Enumerable.Empty<string>())
                .Where(name => template.InheritanceList.Contains(ExtractTemplateName(name)));

            var dialogNames = dialogName
                .AsArray()
                .Concat(validParentNames)
                .ToList();

            var dialogs = GetDialogs(metamodel, dialogNames);

            dialogs
                .SelectMany(t => t.Tabs)
                .DoForEach(
                    t =>
                    {
                        if (!resultTabs.ContainsKey(t.Name))
                        {
                            resultTabs.Add(t.Name, t);
                        }
                    });

            dialogs
                .SelectMany(l => l.Layouts)
                .DoForEach(
                    l =>
                    {
                        if (!resultDialogLayouts.ContainsKey(l.Name))
                        {
                            resultDialogLayouts.Add(l.Name, l);
                        }
                    });

            if (!resultDialogLayouts.Any() && !metamodel.DefaultDialog.IsBlank())
            {
                resultDialogLayouts = GetDialogLayouts(metamodel, metamodel.DefaultDialog)
                    .ToDictionary(l => l.Name);
            }

            if (displayNames != null)
            {
                ChangeTabsDisplayName(resultTabs, displayNames);
            }

            var orderedTabs = ReorderTabs(
                initialTabs
                    .Select(t => t.Name)
                    .Union(s_defaultTabsOrder),
                resultTabs.Values);

            var clearedTabs = orderedTabs
                .Select(RemoveExcessiveSpaces)
                .ToList();
            var clearedDialogLayouts = resultDialogLayouts.Values
                .Select(RemoveExcessiveSpaces)
                .ToList();

            metamodel.Set(
                new DialogDescription(
                    dialogName,
                    null,
                    clearedTabs,
                    clearedDialogLayouts));
        }

        /// <summary>
        ///     Modifies tab definition in target dialog.
        /// </summary>
        /// <param name="metamodel">
        ///     Instance of <see cref="IMetamodel" /> to make changes through.
        /// </param>
        /// <param name="dialogName">
        ///     Full name of the dialog including role and/or customization (e.g. role/customization/name.xdlg).
        /// </param>
        /// <param name="tab">
        ///     New definition of tab including all kinds of layout.
        /// </param>
        public static void ModifyTab(
            IMetamodel metamodel,
            string dialogName,
            DialogTab tab)
        {
            #region Argument Checks

            if (metamodel == null)
            {
                throw new ArgumentNullException("metamodel");
            }

            if (dialogName.IsBlank())
            {
                throw new ArgumentException("Dialog name cannot be blank", "dialogName");
            }

            if (tab == null)
            {
                throw new ArgumentNullException("tab");
            }

            #endregion

            var dialog = GetDialogOrNull(metamodel, dialogName);
            if (dialog == null)
            {
                return;
            }

            if (dialog.Tabs.All(t => t.Name != tab.Name))
            {
                return;
            }

            var clearTab = RemoveExcessiveSpaces(tab);

            metamodel.Set(
                new DialogDescription(
                    dialog.Name,
                    dialog.InheritanceList,
                    dialog.Tabs
                        .Select(t => t.Name != tab.Name ? t : clearTab),
                    dialog.Layouts));

            var template = GetTemplateOrNull(metamodel, ExtractTemplateName(dialogName));
            if (template == null)
            {
                return;
            }

            if (!template.Abstract)
            {
                return;
            }

            var dialogs = metamodel.Dialogs
                .Where(d => d.Name != dialogName)
                .Where(d => d.Tabs.Any(t => t.Name == tab.Name));

            dialogs.DoForEach(
                d =>
                    metamodel.Set(
                        new DialogDescription(
                            d.Name,
                            d.InheritanceList,
                            d.Tabs
                                .Select(t => t.Name != tab.Name ? t : clearTab),
                            d.Layouts)));
        }

        /// <summary>
        ///     Modifies (if found) or creates new template definition.
        /// </summary>
        /// <param name="metamodel">
        ///     Instance of <see cref="IMetamodel" /> to make changes through.
        /// </param>
        /// <param name="templateName">
        ///     Name of template to modify or create.
        /// </param>
        /// <param name="parentNames">
        ///     List of parent templates to inherit from.
        /// </param>
        /// <param name="hidden">
        ///     <b>true</b> to hide template in UI; otherwise <b>false</b>
        /// </param>
        /// <param name="attributes">
        ///     List of explicit attributes (may be extended by inheritance).
        /// </param>
        public static void ModifyTemplate(
            IMetamodel metamodel,
            string templateName,
            IEnumerable<string> parentNames,
            bool hidden,
            IEnumerable<AttributeDefinitionBase> attributes)
        {
            #region Argument Checks

            if (metamodel == null)
            {
                throw new ArgumentNullException("metamodel");
            }

            if (templateName.IsBlank())
            {
                throw new ArgumentException("Template name cannot be blank", "templateName");
            }

            #endregion

            var template = GetTemplateOrNull(metamodel, templateName);

            var existingAttributes = templateName
                .AsArray()
                .Concat(parentNames ?? Enumerable.Empty<string>())
                .Reverse()
                .Select(t => GetTemplateOrNull(metamodel, t))
                .Where(t => t != null)
                .SelectMany(t => t.Attributes)
                .Where(a => !a.IsPredefined)
                .Cast<AttributeDefinitionBase>()
                .ToList();

            var result = MergeAttributes(existingAttributes, attributes);

            bool isHidden = (template != null && template.Abstract) || hidden;
            bool isAbstract = template != null ? template.Abstract : hidden;

            metamodel.Set(
                new TemplateDefinition(
                    templateName,
                    template != null ? template.Description : null,
                    result,
                    null,
                    template != null && template.ShareName,
                    template != null ? template.IconData : new byte[0],
                    isHidden,
                    isAbstract));
        }

        /// <summary>
        ///     Removes tabs in template's default dialog which were introduced by parent template default dialog.
        ///     This doesn't affect attribute definitions in template, but only default dialog of the template.
        /// </summary>
        /// <param name="metamodel">
        ///     Instance of <see cref="IMetamodel" /> to make changes through.
        /// </param>
        /// <param name="templateName">
        ///     Name of template to process.
        /// </param>
        /// <param name="parentName">
        ///     Name of parent template to remove.
        /// </param>
        public static void RemoveTemplateParent(
            IMetamodel metamodel,
            string templateName,
            string parentName)
        {
            #region Argument Checks

            if (metamodel == null)
            {
                throw new ArgumentNullException("metamodel");
            }

            if (templateName.IsBlank())
            {
                throw new ArgumentException("Template name cannot be blank.", "templateName");
            }

            if (parentName.IsBlank())
            {
                throw new ArgumentException("Parent template name cannot be blank.", "parentName");
            }

            #endregion

            var existingTemplate = GetTemplateOrNull(metamodel, templateName);
            if (existingTemplate == null)
            {
                return;
            }

            if (!existingTemplate.InheritanceList.Contains(parentName))
            {
                return;
            }

            var parentDialog = GetDialogOrNull(metamodel, parentName + c_dlgExtension);

            RemoveSubTemplate(metamodel, existingTemplate, parentName, parentDialog);

            if (existingTemplate.Abstract)
            {
                metamodel
                    .Templates
                    .Where(t => t.InheritanceList.Contains(existingTemplate.Name))
                    .Where(t => t.InheritanceList.Contains(parentName))
                    .DoForEach(t => RemoveSubTemplate(metamodel, t, parentName, parentDialog));
            }
        }

        /// <summary>
        ///     Reorders tabs in <paramref name="dialogName" /> according to given order in <paramref name="tabOrder" />.
        /// </summary>
        /// <param name="metamodel">
        ///     Instance of <see cref="IMetamodel" /> to make changes through.
        /// </param>
        /// <param name="dialogName">
        ///     Full name of the dialog including role and/or customization (e.g. role/customization/name.xdlg).
        /// </param>
        /// <param name="tabOrder">
        ///     Desired order of tabs. Can be list of tab names or display names.
        /// </param>
        public static void ReorderTabs(
            IMetamodel metamodel,
            string dialogName,
            IEnumerable<string> tabOrder)
        {
            #region Argument Checks

            if (metamodel == null)
            {
                throw new ArgumentNullException("metamodel");
            }

            if (dialogName.IsBlank())
            {
                throw new ArgumentException("Dialog name cannot be blank", "templateName");
            }

            if (tabOrder == null || !tabOrder.Any())
            {
                throw new ArgumentException("Suggested tabs order cannot be null or empty");
            }

            #endregion

            var dialog = GetDialogOrNull(metamodel, dialogName);
            if (dialog == null)
            {
                return;
            }

            var tabs = dialog.Tabs;
            var orderedTabs = tabOrder
                .Select(
                    tabName => tabs
                        .FirstOrDefault(tab => tab.DisplayName == tabName || tab.Name == tabName))
                .Where(tab => tab != null)
                .ToList();
            var unorderedTabs = tabs
                .Except(orderedTabs)
                .ToList();

            metamodel.Set(
                new DialogDescription(
                    dialog.Name,
                    dialog.InheritanceList,
                    orderedTabs
                        .Concat(unorderedTabs),
                    dialog.Layouts));
        }

        /// <summary>
        ///     Sets visibility of target template.
        /// </summary>
        /// <param name="metamodel">
        ///     Instance of <see cref="IMetamodel" /> to make changes through.
        /// </param>
        /// <param name="templateName">
        ///     Name of template to change visibility for.
        /// </param>
        /// <param name="hidden">
        ///     <b>true</b> to hide template in UI; otherwise <b>false</b>
        /// </param>
        public static void SetTemplateVisibility(
            IMetamodel metamodel,
            string templateName,
            bool hidden)
        {
            #region Argument Checks

            if (metamodel == null)
            {
                throw new ArgumentNullException("metamodel");
            }

            if (templateName.IsBlank())
            {
                throw new ArgumentException("Template name cannot be blank", "templateName");
            }

            #endregion

            var exisitingTemplate = GetTemplateOrNull(metamodel, templateName);
            if (exisitingTemplate == null)
            {
                return;
            }

            metamodel.Set(
                new TemplateDefinition(
                    exisitingTemplate.Name,
                    exisitingTemplate.Description,
                    exisitingTemplate
                        .Attributes
                        .Cast<AttributeDefinitionBase>()
                        .Where(a => !a.IsPredefined),
                    exisitingTemplate.InheritanceList,
                    exisitingTemplate.ShareName,
                    exisitingTemplate.IconData,
                    hidden,
                    exisitingTemplate.Abstract));
        }

        #endregion
    }
}
