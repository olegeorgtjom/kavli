﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Mail;
using QCommon.Extensions;
using QCommon.Log;
using Qdc.Common;
using Qef.Common;
using Qef.Common.Log;
using Qef.Common.Mail;
using Qef.Common.Users;
using Qis.Common;
using Qis.Common.AttributeValues;
using Qis.Common.Utils.Web;
using Qis.Web.Forms.Common;
using Qis.Web.Forms.Common.Scripting;
using Qis.Web.Forms.Common.UIActions;
using IRepository = Qis.Common.IRepository;
using IUser = Qef.Common.Users.IUser;
using Qef.Common.Security;

namespace Qis.Module.Scripts
{
    /// <summary>
    ///     Provides methods relevant to Change Management System (CMS) functionality.
    /// </summary>
    public static class CMS
    {
        #region Constants

        private const string c_qlmDateTimeFormat = "yyyy MM dd HH.mm:ss";
        private const string c_supervisorRole = "Supervisor";
        internal const string c_approvedApprovalState = "Approved";
        internal const string c_circulatedCirculatedState = "Circulation";
        internal const string c_developmentApprovalState = "Development";
        internal const string c_draftApprovalState = "Draft";
        internal const string c_draftApprovalStateDK = "Udkast";

        #endregion

        #region Fields

        private static string s_approvalSubjectFormat = "QualiWare Approval: {0}";
        private static string s_approvalTextFormat;
        private static string s_circulationSubjectFormat = "QualiWare Circulation: {0}";
        private static string s_circulationTextFormat;
        private static bool s_mailBodyHtml;

        /// <summary>
        ///     Set mail server settings name to name defined in QEF Admin Console.
        ///     Mail server settings set to null will use default mail sever settings.
        /// </summary>
        private static string s_mailServerSettingsName = null;
        private static MailAddress s_qualiWareAddress = new MailAddress("no-reply@qualiware.com", "QualiWare");

        #endregion

        #region Constructors and Destructors

        static CMS()
        {
            s_mailBodyHtml = true;
            var newLine = s_mailBodyHtml ? "<br />" : Environment.NewLine;
            s_circulationTextFormat = string.Format(
                "Dear {{0}},{0}{0}" +
                    "Circulation: {{1}}{0}{0}" +
                    "Link: {{2}}{0}{0}" +
                    "Further information:{0}" +
                    "{{3}}{0}{0}{0}" +
                    "Best Regards{0}" +
                    "{{4}}",
                newLine);
            s_approvalTextFormat = string.Format(
                "Dear {{0}},{0}{0}" +
                    "Approved: {{1}}{0}{0}" +
                    "Link: {{2}}{0}{0}" +
                    "Further information:{0}" +
                    "{{3}}{0}{0}{0}" +
                    "Best Regards{0}" +
                    "{{4}}",
                newLine);
        }

        #endregion

        #region Private Methods

        private static bool CanApprove(
            IConfiguration configuration,
            RepositoryObject repositoryObject,
            RepositoryObject user,
            bool adminApproval = false)
        {
            #region Argument Check

            if (repositoryObject == null)
            {
                throw new ArgumentNullException("repositoryObject");
            }

            if (configuration == null)
            {
                throw new ArgumentNullException("configuration");
            }

            #endregion

            if (repositoryObject.IsFrozen)
            {
                return false;
            }

            const string canApproveAttrName = Metamodel.Attributes.CanApprove;

            if (!repositoryObject.Attributes.Contains(canApproveAttrName))
            {
                return true;
            }

            QHyperLinkList canApproveHyperLinkList =
                repositoryObject.Attributes[canApproveAttrName].Value.GetHyperLinks();

            if (canApproveHyperLinkList.Count == 0 || adminApproval)
            {
                return true;
            }

            if (user == null)
            {
                return false;
            }

            var roleData = configuration.GetRepository().GetCurrentRepositoryRoleEffectiveData();

            if (roleData != null && roleData.Name == c_supervisorRole)
            {
                return true;
            }

            return canApproveHyperLinkList.Where(item => item.ObjectId.Id == user.Id.Id).Count() > 0;
        }

        private static MailMessage CreateMailMessage(
            MailAddress fromAddress,
            MailAddress toAddress,
            string subject,
            string body)
        {
            MailMessage message = new MailMessage();

            message.Subject = subject;
            message.Body = body;
            message.IsBodyHtml = s_mailBodyHtml;
            message.From = fromAddress;
            message.To.Add(toAddress);

            return message;
        }

        private static void SetApprovalAttributes(
            RepositoryObject repositoryObject,
            RepositoryObject approvalChangedBy,
            string approvalState)
        {
            #region Argument Check

            if (repositoryObject == null)
            {
                throw new ArgumentNullException("repositoryObject");
            }

            if (approvalChangedBy == null)
            {
                throw new ArgumentNullException("approvalChangedBy");
            }

            #endregion

            const string approvalStateAttrName = Metamodel.Attributes.ApprovalState;
            const string approvalDateAttrName = Metamodel.Attributes.ApprovalDate;
            const string approvalChangedByAttrName = Metamodel.Attributes.ApprovalChangedBy;
            //const string eMailAttrName = Metamodel.Attributes.EMail;

            // Make sure that object is not reserved. Object may be reserved by DMS.CheckOutDocument.
            var reserveStatus = repositoryObject.GetReserveStatus();

            if (reserveStatus.Type == ObjectReserveResultType.Reserved)
            {
                var configuration = repositoryObject.GetConfiguration();
                var repository = configuration.GetRepository();

                repository.RemoveReservation(repositoryObject.Id.Id, reserveStatus.UserId);
            }

            // Change the ApprovalState to Approved.
            ObjectScripts.SetAttributeValue(repositoryObject, approvalStateAttrName, new PlainText(approvalState));

            // Change ApprovalDate to current date.
            ObjectScripts.SetAttributeValue(repositoryObject, approvalDateAttrName, new QDateTime(DateTime.UtcNow));

            // Change ApprovalChangedBy to current user. If user does not exist, new user is created.
            QHyperLink approvalChangedByLink = approvalChangedBy.GetRemoteRepositoryHyperlink();

            ObjectScripts.SetAttributeValue(
                repositoryObject,
                approvalChangedByAttrName,
                new SingleLink(approvalChangedByLink));
        }

        private static OidResultList DoApproveObjectSet(
            IRepository repository,
            IConfiguration configuration,
            IMailService mailService,
            RepositoryObject rootRepositoryObject,
            RepositoryObjectList containedRepositoryObjects,
            RepositoryObject approvalChangedBy,
            string approvalState,
            bool sendMailNotification,
            bool rollbackOnFailure = false,
            bool adminApproval = false)
        {
            #region Argument Check

            if (rootRepositoryObject == null)
            {
                throw new ArgumentNullException("rootRepositoryObject");
            }
            if (containedRepositoryObjects == null)
            {
                throw new ArgumentNullException("containedRepositoryObjects");
            }
            if (approvalChangedBy == null)
            {
                throw new ArgumentNullException("approvalChangedBy");
            }

            #endregion

            bool rollback = false;
            var lockedObjects = new List<RepositoryObject>();

            var resultProxy = containedRepositoryObjects.ToDictionary(o => o.Id, o => (Exception)null);
            resultProxy[rootRepositoryObject.Id] = null;

            if (rootRepositoryObject.IsFrozen)
            {
                var error = new QInvalidScriptOperationException(
                    string.Format(
                        "{0} \"{1}\": {2}.",
                        Messages.CannotApproveMessage,
                        rootRepositoryObject.Name,
                        Messages.IsFrozenMessage));

                return new OidResultList(new OidResult(rootRepositoryObject.Id, error).AsArray());
            }

            // Lock root object. Report error if root object cannot be locked or cannot be approved.
            var lockResult = rootRepositoryObject.LockObject();
            if (lockResult.Type != ObjectLockResultType.Locked)
            {
                var error = new QInvalidScriptOperationException(
                    Common.GetLockResultMessage(
                        Messages.CannotApproveMessage,
                        rootRepositoryObject.Name,
                        lockResult.Type));

                return new OidResultList(new OidResult(rootRepositoryObject.Id, error).AsArray());
            }

            if (!CanApprove(rootRepositoryObject, approvalChangedBy, adminApproval))
            {
                var error = new QInvalidScriptOperationException(
                    string.Format(
                        "{0} \"{1}\": {2}.",
                        Messages.CannotApproveMessage,
                        rootRepositoryObject.Name,
                        Messages.NotAllowedToApproveMessage));

                return new OidResultList(new OidResult(rootRepositoryObject.Id, error).AsArray());
            }

            lockedObjects.Add(rootRepositoryObject);

            // Lock contained objects.
            foreach (RepositoryObject obj in containedRepositoryObjects)
            {
                if (rollbackOnFailure && rollback)
                {
                    break;
                }

                if (obj.IsFrozen)
                {
                    // Skip silently.
                    /* 
                    resultProxy[obj.Id] = new QInvalidScriptOperationException(
                        string.Format(
                            "{0} \"{1}\": {2}.",
                            Messages.CannotApproveMessage,
                            obj.Name,
                            Messages.IsFrozenMessage));
                     */
                    rollback = true;

                    continue;
                }

                lockResult = obj.LockObject();

                if (lockResult.Type != ObjectLockResultType.Locked)
                {
                    // Skip silently.
                    /* 
                    resultProxy[obj.Id] = new QInvalidScriptOperationException(
                        Common.GetLockResultMessage(
                            Messages.CannotApproveMessage,
                            obj.Name,
                            lockResult.Type));
                        */
                    rollback = true;
                    continue;
                }

                lockedObjects.Add(obj);
            }

            var toApproveObjects = new List<RepositoryObject>();

            // Validate objects for approve.
            if (!(rollbackOnFailure && rollback))
            {
                foreach (RepositoryObject lockedObject in lockedObjects)
                {
                    if (lockedObject == rootRepositoryObject)
                    {
                        toApproveObjects.Add(lockedObject);

                        continue;
                    }

                    if (!CanApprove(lockedObject, approvalChangedBy))
                    {
                        // Skip silently.
                        /* 
                        resultProxy[lockedObject.Id] = new QInvalidScriptOperationException(
                            string.Format(
                                "{0} \"{1}\": {2}.",
                                Messages.CannotApproveMessage,
                                lockedObject.Name,
                                Messages.NotAllowedToApproveMessage));
                         */
                        rollback = true;

                        continue;
                    }

                    toApproveObjects.Add(lockedObject);
                }
            }


            // Approve objects.
            if (!(rollbackOnFailure && rollback))
            {
                toApproveObjects.DoForEach(
                    o =>
                    {
                        SetApprovalAttributes(
                            o,
                            approvalChangedBy,
                            approvalState);
                    });

                var saveResultSet = new RepositoryObjectList(toApproveObjects).SaveData();
                foreach (var saveResult in saveResultSet)
                {
                    resultProxy[saveResult.Id] = saveResult.Error;
                }

                var frozenResultSet = repository.FreezeObjectSet(
                    new OidList(
                        toApproveObjects
                            .Where(
                                o =>
                                {
                                    Exception err;
                                    return !resultProxy.TryGetValue(o.Id, out err) || err == null;
                                })
                            .Select(o => o.Id)));
                foreach (var frozenResult in frozenResultSet)
                {
                    resultProxy[frozenResult.Id] = frozenResult.Error;
                }

                if (sendMailNotification && mailService != null)
                {
                    toApproveObjects
                    .Where(
                        o =>
                        {
                            Exception err;
                            return !resultProxy.TryGetValue(o.Id, out err) || err == null;
                        })
                    .DoForEach(o => SendMailNotificationToSubscribers(mailService, o));

                    //msg += string.Format("Sending mails: {0}ms\n", sw.ElapsedMilliseconds - prev);
                    //prev = sw.ElapsedMilliseconds;
                }
            }

            lockedObjects.DoForEach(item => item.UnlockObject());

            if (resultProxy.ContainsKey(rootRepositoryObject.Id) && resultProxy[rootRepositoryObject.Id] == null)
            {
                rootRepositoryObject.RefreshData();
            }

            return new OidResultList(resultProxy.Select(pair => new OidResult(pair.Key, pair.Value)));
        }

        private static string GetApprovalMessage(
            RepositoryObject repositoryObject,
            string from,
            string to)
        {
            const string descriptionTextAttrName = Metamodel.Attributes.Description;

            IAttributeValue attributeValue = ObjectScripts.GetAttributeValue(repositoryObject, descriptionTextAttrName);

            string link = string.Format(
                "http://{0}/QEF/WebModules/WebForms/GeneratedHTML/{1}/{2}/{3}/{4}.html",
                Environment.MachineName,
                repositoryObject.RepositoryId,
                repositoryObject.GetConfiguration().GetData().Name.Replace(" ", "%20"),
                repositoryObject.Template,
                repositoryObject.Id.Id);

            string message = string.Format(
                s_approvalTextFormat,
                to,
                repositoryObject.Name,
                link,
                attributeValue != null
                    ? attributeValue.GetContent()
                    : string.Empty,
                from);

            return message;
        }

        private static string GetCirculationMessage(
            RepositoryObject repositoryObject,
            string from,
            string to)
        {
            const string circulationTextAttrName = Metamodel.Attributes.CirculationText;

            IAttributeValue attributeValue = ObjectScripts.GetAttributeValue(repositoryObject, circulationTextAttrName);

            string link = string.Format(
                "http://{0}/QEF/WebModules/WebForms/GeneratedHTML/{1}/{2}/{3}/{4}.html",
                Environment.MachineName,
                repositoryObject.RepositoryId,
                repositoryObject.GetConfiguration().GetData().Name.Replace(" ", "%20"),
                repositoryObject.Template,
                repositoryObject.Id.Id);

            string message = string.Format(
                s_circulationTextFormat,
                to,
                repositoryObject.Name,
                link,
                attributeValue != null
                    ? attributeValue.GetContent()
                    : string.Empty,
                from);

            return message;
        }

        private static void SendMailMessage(
            IMailService mailService,
            MailMessage message)
        {
            #region Argument Check

            if (mailService == null)
            {
                throw new ArgumentNullException("mailService");
            }

            if (message == null)
            {
                throw new ArgumentNullException("message");
            }

            #endregion

            if (s_mailServerSettingsName.IsBlank())
            {
                mailService.SendMailAsync(new SerializableMailMessage(message), null, null);

                return;
            }

            var mailServerSettings = mailService.GetAllServerSettings()
                .FirstOrDefault(
                    item => StringComparer.OrdinalIgnoreCase.Equals(item.GetData().Name, s_mailServerSettingsName));

            if (mailServerSettings == null)
            {
                throw new Exception(
                    string.Format(
                        "Cannot send e-mail. Mail server settings \"{0}\" are not found.",
                        s_mailServerSettingsName));
            }

            mailService.SendMailAsync(mailServerSettings.Id, new SerializableMailMessage(message), null, null);
        }

        private static void SendMailMessages(
            IMailService mailService,
            List<MailMessage> messages)
        {
            #region Argument Check

            if (mailService == null)
            {
                throw new ArgumentNullException("mailService");
            }

            if (messages == null)
            {
                throw new ArgumentNullException("messages");
            }

            #endregion

            Exception exception = null;

            foreach (MailMessage message in messages)
            {
                try
                {
                    SendMailMessage(mailService, message);
                }
                catch (Exception ex)
                {
                    exception = ex;
                }
            }

            if (exception != null)
            {
                throw exception;
            }
        }

        #endregion

        #region Public Methods

        public static void AcknowledgeObject(
            IUserStorageService userStorageService,
            IRepository repository,
            IConfiguration configuration,
            RepositoryObject acknowledge,
            Guid userId)
        {
            #region Argument Check

            if (userStorageService == null)
            {
                throw new ArgumentNullException("userStorageService");
            }

            if (repository == null)
            {
                throw new ArgumentNullException("repository");
            }

            if (configuration == null)
            {
                throw new ArgumentNullException("configuration");
            }

            if (acknowledge == null)
            {
                throw new ArgumentNullException("acknowledge");
            }

            #endregion

            var user = configuration.ResolveUserObject();

            var ackwoledgeList = Search.Scripts.FindObjects(
                repository,
                configuration,
                new[] { Metamodel.Templates.AcknowledgeList },
                new Dictionary<string, Search.Scripts.SearchValue>
                {
                    {
                        Metamodel.Attributes.AssociatedWith,
                        new Search.Scripts.SearchValue(user.Id.Id.ToString().AsList(), Search.Scripts.SearchType.Or)
                    },
                },
                Search.Scripts.SearchType.Or,
                false);

            var acknowledgeHyperLink = acknowledge.GetRemoteRepositoryHyperlink();

            var saveList = new RepositoryObjectList();
            ackwoledgeList.DoForEach(
                item =>
                {
                    var acknowledgeHyperLinkList =
                        item.Attributes[Metamodel.Attributes.ToBeAcknowledged].Value.GetHyperLinks();

                    var validHLinkList =
                        new QHyperLinkList(
                            acknowledgeHyperLinkList.Where(item2 => item2.ObjectId == acknowledgeHyperLink.ObjectId));

                    if (!validHLinkList.Any())
                    {
                        return;
                    }

                    acknowledgeHyperLinkList =
                        new QHyperLinkList(
                            acknowledgeHyperLinkList.Where(item2 => item2.ObjectId != acknowledgeHyperLink.ObjectId));
                    ObjectScripts.SetAttributeValue(
                        item,
                        Metamodel.Attributes.ToBeAcknowledged,
                        new MultiLink(acknowledgeHyperLinkList));

                    var acknowledgedHyperLinkList =
                        item.Attributes[Metamodel.Attributes.HasBeenAcknowledged].Value.GetHyperLinks();
                    acknowledgeHyperLink.CustomAttributes.Add(
                        QisConst.LinkValueKey,
                        String.Format(
                            "{0}\t{1}",
                            acknowledge.Id.Revision.ToString(),
                            DateTime.Now.ToString(c_qlmDateTimeFormat)));

                    acknowledgedHyperLinkList.Add(acknowledgeHyperLink);
                    ObjectScripts.SetAttributeValue(
                        item,
                        Metamodel.Attributes.HasBeenAcknowledged,
                        new MultiLink(acknowledgedHyperLinkList));

                    saveList.Add(item);
                });

            saveList.SaveData();
        }

        /// <summary>
        ///     Approves the specified object.
        /// </summary>
        /// <param name="userStorageService">
        ///     QEF service to retrieve data about users and groups.
        /// </param>
        /// <param name="mailService">
        ///     QEF service to send mail messages.
        /// </param>
        /// <param name="repositoryObject">
        ///     Object to approve.
        /// </param>
        /// <param name="approvalChangedBy">
        ///     Identifier of the person, who approves the object.
        /// </param>
        /// <param name="approveContains">
        ///     <b>true</b> to approve objects that the <paramref name="repositoryObject" /> object contains; <b>false</b>
        ///     otherwise.
        /// </param>
        /// <param name="sendMailNotification">
        ///     <b>true</b> to send e-mail message notification to all subscribers; <b>false</b> otherwise.
        /// </param>
        /// <param name="rollbackOnFailure"></param>
        /// <param name="adminApproval"></param>
        /// <returns>
        ///     List containing result entry for every approved object.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        ///     <paramref name="userStorageService" /> is null.<br />
        ///     -or-<br />
        ///     <paramref name="repositoryObject" /> is null.
        /// </exception>
        /// <exception cref="QInvalidScriptOperationException">
        ///     Person, who approves the object, cannot be found.
        /// </exception>
        public static OidResultList ApproveObject(
            IUserStorageService userStorageService,
            IMailService mailService,
            RepositoryObject repositoryObject,
            Guid approvalChangedBy,
            bool approveContains,
            bool sendMailNotification,
            bool rollbackOnFailure = false,
            bool adminApproval = false)
        {
            return ApproveObject(
                userStorageService,
                mailService,
                repositoryObject,
                approvalChangedBy,
                c_approvedApprovalState,
                approveContains,
                sendMailNotification,
                rollbackOnFailure:rollbackOnFailure,
                adminApproval:adminApproval);
        }

        /// <summary>
        ///     Approves the specified object.
        /// </summary>
        /// <param name="userStorageService">
        ///     QEF service to retreive data about users and groups.
        /// </param>
        /// <param name="mailService">
        ///     QEF service to send mail messages.
        /// </param>
        /// <param name="repositoryObject">
        ///     Object to approve.
        /// </param>
        /// <param name="approvalChangedBy">
        ///     Identifier of the person, who approves the object.
        /// </param>
        /// <param name="approvalState">
        ///     Approval state.
        /// </param>
        /// <param name="approveContains">
        ///     <b>true</b> to approve objects that the <paramref name="repositoryObject" /> object contains; <b>false</b>
        ///     otherwise.
        /// </param>
        /// <param name="sendMailNotification">
        ///     <b>true</b> to send e-mail message notification to all subscribers; <b>false</b> otherwise.
        /// </param>
        /// <param name="rollbackOnFailure">
        ///     <b>true</b> to rollback approval action, if one of the objects cannot be approved; <b>false</b> to skip objects
        ///     that cannot be approved.
        /// </param>
        /// <param name="adminApproval"></param>
        /// <param name="SkipGweContent"></param>
        /// <returns>
        ///     List containing result entry for every approved object.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        ///     <paramref name="userStorageService" /> is null.<br />
        ///     -or-<br />
        ///     <paramref name="repositoryObject" /> is null.
        /// </exception>
        /// <exception cref="QInvalidScriptOperationException">
        ///     Person, who approves the object, cannot be found.
        /// </exception>
        public static OidResultList ApproveObject(
            IUserStorageService userStorageService,
            IMailService mailService,
            RepositoryObject repositoryObject,
            Guid approvalChangedBy,
            string approvalState,
            bool approveContains,
            bool sendMailNotification,
            bool rollbackOnFailure = false,
            bool adminApproval = false,
            bool SkipGweContent = false)
        {
            #region Argument Check

            if (userStorageService == null)
            {
                throw new ArgumentNullException("userStorageService");
            }

            if (mailService == null)
            {
                throw new ArgumentNullException("mailService");
            }

            if (repositoryObject == null)
            {
                throw new ArgumentNullException("repositoryObject");
            }

            #endregion

            IConfiguration configuration = repositoryObject.GetConfiguration();
            IRepository repository = configuration.GetRepository();

            // Throws exception if user cannot be found and be synchronized.
            RepositoryObject approvalChangedByObject = configuration.ResolveUserObject();

            RepositoryObjectList objectList = new RepositoryObjectList();

            if (approveContains)
            {
                if (SkipGweContent)
                {
                    objectList.AddRange(repositoryObject.GetContains().Where(item => !item.IsGweObject()));
                }
                else
                {
                    objectList.AddRange(repositoryObject.GetContains());
                }
            }

            OidResultList resultList = DoApproveObjectSet(
                repository,
                configuration,
                mailService,
                repositoryObject,
                objectList,
                approvalChangedByObject,
                approvalState,
                sendMailNotification,
                rollbackOnFailure,
                adminApproval);

            return resultList;
        }

        /// <summary>
        ///     Checks if the user can approve object.
        /// </summary>
        /// <param name="repositoryObject">
        ///     Object to approve.
        /// </param>
        /// <param name="user">
        ///     Object of the <see cref="Metamodel.Templates.Person" /> template representing user, which approves
        ///     object. Can be null.
        /// </param>
        /// <returns>
        ///     <b>true</b> if object can be approved; <b>false</b> otherwise.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        ///     <paramref name="repositoryObject" /> is null.
        /// </exception>
        public static bool CanApprove(RepositoryObject repositoryObject, RepositoryObject user,	bool adminApproval = false)
        {
            #region Argument Check

            if (repositoryObject == null)
            {
                throw new ArgumentNullException("repositoryObject");
            }

            #endregion

            IConfiguration configuration = repositoryObject.GetConfiguration();

            if (configuration == null)
            {
                throw new ArgumentException("Cannot get configuration.", "repositoryObject");
            }

            return CanApprove(configuration, repositoryObject, user, adminApproval);
        }

        /// <summary>
        ///     Circulates the specified object.
        /// </summary>
        /// <param name="userStorageService">
        ///     QEF service to retreive data about users and groups.
        /// </param>
        /// <param name="mailService">
        ///     QEF service to send mail messages.
        /// </param>
        /// <param name="repositoryObject">
        ///     Object to circulate.
        /// </param>
        /// <param name="userId">
        ///     Identifier of QEF user, whicj circulates object.
        /// </param>
        /// <param name="sendMailNotification">
        ///     <b>true</b> to send e-mail message notification to all persons registered for circulation;
        ///     <b>false</b> otherwise.
        /// </param>
        /// <exception cref="ArgumentNullException">
        ///     <paramref name="userStorageService" /> is null.<br />
        ///     -or-<br />
        ///     <paramref name="repositoryObject" /> is null.
        /// </exception>
        /// <exception cref="QInvalidScriptOperationException">
        ///     Recipients are not found.<br />
        ///     -or-<br />
        ///     Object is frozen.
        ///     -or-<br />
        ///     E-mail message notification cannot be sent.
        /// </exception>
        public static void CirculateObject(
            IUserStorageService userStorageService,
            IMailService mailService,
            RepositoryObject repositoryObject,
            Guid userId,
            bool sendMailNotification)
        {
            #region Argument Check

            if (userStorageService == null)
            {
                throw new ArgumentNullException("userStorageService");
            }

            if (mailService == null)
            {
                throw new ArgumentNullException("mailService");
            }

            if (repositoryObject == null)
            {
                throw new ArgumentNullException("repositoryObject");
            }

            if (repositoryObject.IsFrozen)
            {
                throw new QInvalidScriptOperationException(
                    string.Format(
                        "{0} \"{1}\": {2}.",
                        Messages.CannotCirculateMessage,
                        repositoryObject.Name,
                        Messages.IsFrozenMessage));
            }

            #endregion

            const string circulateToAttrName = Metamodel.Attributes.CirculateTo;
            const string eMailAttrName = Metamodel.Attributes.EMail;

            ObjectScripts.SetAttributeValue(
                repositoryObject,
                Metamodel.Attributes.ApprovalState,
                new PlainText(c_circulatedCirculatedState));

            repositoryObject.SaveData();

            var logWriter =
                repositoryObject.GetConfiguration()
                    .GetRepository()
                    .GetQisServer()
                    .GetQefService<ILogService>()
                    .GetLogWriter(QisConst.QisId, Source.Parse("Circulation"));

            logWriter.Info("Sending e-mai: " + sendMailNotification);

            // Send notofication.
            if (sendMailNotification)
            {
                IConfiguration configuration = repositoryObject.GetConfiguration();
                IRepository repository = configuration.GetRepository();

                List<MailAddress> addresses = new List<MailAddress>();

                IAttributeValue attributeValue;

                foreach (RepositoryObject circulateToObject in
                    ObjectScripts.GetAttributeRelations(configuration, repositoryObject.Id, circulateToAttrName))
                {
                    attributeValue = ObjectScripts.GetAttributeValue(circulateToObject, eMailAttrName);

                    if (attributeValue == null || attributeValue.IsEmpty)
                    {
                        continue;
                    }

                    string address = attributeValue.GetContent();

                    if (addresses.Where(item => item.Address == address).Count() > 0)
                    {
                        continue;
                    }

                    addresses.Add(new MailAddress(address, circulateToObject.Name));
                }

                if (addresses.Count == 0)
                {
                    return;
                }

                List<MailMessage> messages = new List<MailMessage>();

                RepositoryObject userObject = configuration.ResolveUserObject();

                if (userObject == null)
                {
                    throw new QInvalidScriptOperationException(
                        string.Format(
                            "{0}. Object \"{1}\": {2}.",
                            Messages.CannotSendMailMessageMessage,
                            repositoryObject.Name,
                            Messages.MissingSenderMessage));
                }

                string subject = string.Format(s_circulationSubjectFormat, repositoryObject.Name);

                foreach (MailAddress address in addresses)
                {
                    logWriter.Info("Sending to " + address.Address);
                    string body = GetCirculationMessage(repositoryObject, userObject.Name, address.DisplayName);

                    MailMessage message = CreateMailMessage(
                        s_qualiWareAddress,
                        address,
                        subject,
                        body);

                    messages.Add(message);
                }

                try
                {
                    SendMailMessages(mailService, messages);
                }
                catch (Exception ex)
                {
                    throw new QInvalidScriptOperationException(
                        string.Format(
                            "{0}. Object \"{1}\": {2}.",
                            Messages.CannotSendMailMessageMessage,
                            repositoryObject.Name,
                            ex.Message));
                }
            }
        }

        /// <summary>
        ///     Clears approve attributes of the specified object.
        /// </summary>
        /// <param name="repositoryObject">
        ///     Object, which approve attributes of to clear.
        /// </param>
        /// <exception cref="ArgumentNullException">
        ///     <paramref name="repositoryObject" /> is null.
        /// </exception>
        public static void ClearApproveAttributes(RepositoryObject repositoryObject)
        {
            #region Argument Check

            if (repositoryObject == null)
            {
                throw new ArgumentNullException("repositoryObject");
            }

            #endregion

            ObjectScripts.SetAttributeValue(
                repositoryObject,
                Metamodel.Attributes.ApprovalState,
                new PlainText(c_developmentApprovalState));
            ObjectScripts.ClearAttributeValue(repositoryObject, Metamodel.Attributes.ApprovalDate);
            ObjectScripts.ClearAttributeValue(repositoryObject, Metamodel.Attributes.ApprovalChangedBy);
        }

        /// <summary>
        ///     Creates a comment for the specified object.
        /// </summary>
        /// <param name="repositoryObject">
        ///     Object, which comment to create for.
        /// </param>
        /// <returns>
        /// </returns>
        /// <exception cref="ArgumentNullException">
        ///     <paramref name="repositoryObject" /> is null.
        /// </exception>
        public static RepositoryObject CommentObject(RepositoryObject repositoryObject)
        {
            #region Argument Check

            if (repositoryObject == null)
            {
                throw new ArgumentNullException("repositoryObject");
            }

            #endregion

            const string commentTemplateName = Metamodel.Templates.Comment;
            const string commentToAttrName = Metamodel.Attributes.CommentTo;

            IConfiguration configuration = repositoryObject.GetConfiguration();

            RepositoryObject commentObject = configuration.CreateObject(
                string.Format("New {0}", commentTemplateName),
                commentTemplateName);

            ObjectScripts.SetAttributeValue(
                commentObject,
                commentToAttrName,
                new SingleLink(repositoryObject.GetRemoteRepositoryHyperlink()));

            commentObject.SaveData();

            return commentObject;
        }

        public static RepositoryObjectList FindAcknowledgeList(
            IUserStorageService userStorageService,
            Guid userId,
            IRepository repository,
            IConfiguration configuration,
            bool createAcknowledgeList = true)
        {
            #region Argument Check

            if (userStorageService == null)
            {
                throw new ArgumentNullException("userStorageService");
            }

            if (repository == null)
            {
                throw new ArgumentNullException("repository");
            }

            if (configuration == null)
            {
                throw new ArgumentNullException("configuration");
            }

            #endregion

            var user = configuration.ResolveUserObject();

            var ackwoledgeList = Search.Scripts.FindObjects(
                repository,
                configuration,
                new[] {Metamodel.Templates.AcknowledgeList},
                new Dictionary<string, Search.Scripts.SearchValue>
                {
                    {
                        Metamodel.Attributes.AssociatedWith,
                        new Search.Scripts.SearchValue(user.Id.Id.ToString().AsList(), Search.Scripts.SearchType.Or)
                    },
                },
                Search.Scripts.SearchType.Or,
                false);

            if (ackwoledgeList.Count() <= 0 &&
                createAcknowledgeList)
            {
                var ackowledge = configuration.CreateObject(
                    user.Name,
                    Metamodel.Templates.AcknowledgeList);

                ObjectScripts.SetAttributeValue(
                    ackowledge,
                    Metamodel.Attributes.AssociatedWith,
                    new MultiLink(new QHyperLinkList(user.GetRemoteRepositoryHyperlink().AsArray())));

                ackowledge.SaveData();

                return new RepositoryObjectList();
            }

            var acknowlegeHyperlinkList = ackwoledgeList
                .SelectMany(item => item.Attributes[Metamodel.Attributes.ToBeAcknowledged].Value.GetHyperLinks())
                .Distinct();

            return configuration.GetObjectSet(new QHyperLinkList(acknowlegeHyperlinkList));
        }

        /// <summary>
        ///     Finds comments for the specified object.
        /// </summary>
        /// <param name="repositoryObject">
        ///     Object, comments for which to find.
        /// </param>
        /// <returns>
        ///     List of comments for the specified object.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        ///     <paramref name="repositoryObject" /> is null.
        /// </exception>
        public static RepositoryObjectList FindComments(RepositoryObject repositoryObject)
        {
            #region Argument Check

            if (repositoryObject == null)
            {
                throw new ArgumentNullException("repositoryObject");
            }

            #endregion

            const string commentToAttrName = Metamodel.Attributes.CommentTo;

            IConfiguration configuration = repositoryObject.GetConfiguration();

            RepositoryObjectList comments = ObjectScripts.GetBackAttributeRelations(
                configuration,
                repositoryObject.Id,
                commentToAttrName);

            return comments;
        }

        public static RepositoryObjectList FindSubscribers(RepositoryObject repositoryObject, bool onlyPersonalSubscriptions=false)
        {
            #region Argument Check

            if (repositoryObject == null)
            {
                throw new ArgumentNullException("repositoryObject");
            }

            #endregion

            const string subscribesToAttrName = Metamodel.Attributes.SubscribesTo;
            const string subscriptionOwnerAttrName = Metamodel.Attributes.HasSubscriptionOwner;

            IConfiguration configuration = repositoryObject.GetConfiguration();

            var subscriptions = ObjectScripts.GetBackAttributeRelations(
                configuration,
                repositoryObject.Id,
                subscribesToAttrName);

            var subscribers = new RepositoryObjectList();

            subscriptions.DoForEach(item => {
                    if (onlyPersonalSubscriptions)
                    {
                        var subscriber = ObjectScripts.GetAttributeRelations(configuration, item.Id, subscriptionOwnerAttrName);
                        if (subscriber.Count == 1)
                        {
                            if (subscriber[0].Template=="Person")
                            {
                                subscribers.AddRange(subscriber);
                            }
                        }
                    }
                    else
                    {
                        subscribers.AddRange(
                            ObjectScripts.GetAttributeRelations(configuration, item.Id, subscriptionOwnerAttrName));
                    }
            });

            return subscribers;
        }

        /// <summary>
        ///     Finds subscriptions for the specified user.
        /// </summary>
        /// <param name="userStorageService">
        ///     QEF service to retreive data about users and groups.
        /// </param>
        /// <param name="repository">
        ///     Repository, where to find subscriptions.
        /// </param>
        /// <param name="configuration">
        ///     Configuration, where to find subscriptions.
        /// </param>
        /// <param name="userId">
        ///     Identifier of QEF user, subscriptions of which to find.
        /// </param>
        /// <param name="createIfNotExist">
        ///     <b>true</b> to create a new subscription if it does not exist; <b>false</b> otherwise.
        /// </param>
        /// <returns>
        ///     List of subscriptions for the specified user.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        ///     <paramref name="userStorageService" /> is null.<br />
        ///     -or-<br />
        ///     <paramref name="configuration" /> is null.
        /// </exception>
        public static RepositoryObjectList FindSubscriptions(
            IUserStorageService userStorageService,
            IRepository repository,
            IConfiguration configuration,
            Guid userId,
            bool createIfNotExist,
            bool onlyMyOwnSubscription = false)
        {
            #region Argument Check

            if (userStorageService == null)
            {
                throw new ArgumentNullException("userStorageService");
            }

            if (configuration == null)
            {
                throw new ArgumentNullException("configuration");
            }

            #endregion

            const string subscriptionTemplateName = Metamodel.Templates.Subscription;
            const string subscriptionOwnerAttrName = Metamodel.Attributes.HasSubscriptionOwner;

            RepositoryObject user = configuration.ResolveUserObject();

            RepositoryObjectList subscriptions = ObjectScripts.GetBackAttributeRelations(
                configuration,
                user.Id,
                subscriptionOwnerAttrName);

            if (onlyMyOwnSubscription)
            {
                subscriptions =new RepositoryObjectList(subscriptions.Where(item => item.AttributeGetLinksSafely("HasSubscriptionOwner").Count==1));
            }

            if (createIfNotExist && subscriptions.Count <= 0)
            {
                RepositoryObject subscription = configuration.CreateObject(user.Name, subscriptionTemplateName);

                ObjectScripts.SetAttributeValue(
                    subscription,
                    subscriptionOwnerAttrName,
                    new MultiLink(
                        new QHyperLinkList
                        {
                            user.GetRemoteRepositoryHyperlink()
                        }));

                subscription.SaveData();

                subscriptions.Add(subscription);
            }

            return subscriptions;
        }

        /// <summary>
        ///     Finds subscriptions for the specified user.
        /// </summary>
        /// <param name="userStorageService">
        ///     QEF service to retreive data about users and groups.
        /// </param>
        /// <param name="repository">
        ///     Repository, where to find subscriptions.
        /// </param>
        /// <param name="configuration">
        ///     Configuration, where to find subscriptions.
        /// </param>
        /// <param name="userId">
        ///     Identifier of QEF user, subscriptions of which to find.
        /// </param>
        /// <returns>
        ///     List of subscriptions for the specified user.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        ///     <paramref name="userStorageService" /> is null.<br />
        ///     -or-<br />
        ///     <paramref name="configuration" /> is null.
        /// </exception>
        public static RepositoryObjectList FindSubscriptions(
            IUserStorageService userStorageService,
            IRepository repository,
            IConfiguration configuration,
            Guid userId)
        {
            return FindSubscriptions(userStorageService, repository, configuration, userId, false);
        }

        public static void SendMailNotificationToSubscribers(
            IMailService mailService,
            RepositoryObject repositoryObject)
        {
            #region Argument Check

            if (mailService == null)
            {
                throw new ArgumentNullException("mailService");
            }

            if (repositoryObject == null)
            {
                throw new ArgumentNullException("repositoryObject");
            }

            #endregion

            const string eMailAttrName = Metamodel.Attributes.EMail;
            const string approvalChangedByAttrName = Metamodel.Attributes.ApprovalChangedBy;

            RepositoryObjectList subscribers = FindSubscribers(repositoryObject);

            if (subscribers.Count == 0)
            {
                return;
            }

            List<MailAddress> addresses = new List<MailAddress>();

            foreach (RepositoryObject subscriber in subscribers)
            {
                IAttributeValue attributeValue = ObjectScripts.GetAttributeValue(subscriber, eMailAttrName);

                if (attributeValue == null || attributeValue.IsEmpty)
                {
                    continue;
                }

                string address = attributeValue.GetContent();

                if (addresses.Any(item => item.Address == address))
                {
                    continue;
                }

                addresses.Add(new MailAddress(address, subscriber.Name));
            }

            if (addresses.Count == 0)
            {
                return;
            }

            List<MailMessage> messages = new List<MailMessage>();
            string subject = string.Format(s_approvalSubjectFormat, repositoryObject.Name);

            IAttributeValue approvalChangedBy = ObjectScripts.GetAttributeValue(
                repositoryObject,
                approvalChangedByAttrName);

            foreach (MailAddress address in addresses)
            {
                string body = GetApprovalMessage(
                    repositoryObject,
                    approvalChangedBy != null
                        ? approvalChangedBy.GetContent()
                        : string.Empty,
                    address.DisplayName);

                MailMessage message = CreateMailMessage(
                    s_qualiWareAddress,
                    address,
                    subject,
                    body);

                messages.Add(message);
            }

            try
            {
                SendMailMessages(mailService, messages);
            }
            catch (Exception)
            {
                /*error = new QInvalidScriptOperationException(
                    string.Format("{0}. Object \"{1}\": {2}.",
                        Messages.CannotSendMailMessageMessage,
                        repositoryObject.Name,
                        ex.Message));*/
            }
        }

        /// <summary>
        ///     Subscribes user to the specified object.
        /// </summary>
        /// <param name="userStorageService">
        ///     QEF service to retreive data about users and groups.
        /// </param>
        /// <param name="repositoryObject">
        ///     Object, which user subscribes to.
        /// </param>
        /// <param name="userId">
        ///     Identifier of QEF user, which subscribes to object.
        /// </param>
        /// <returns>
        ///     List of subscriptions for the specified user.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        ///     <paramref name="userStorageService" /> is null.<br />
        ///     -or-<br />
        ///     <paramref name="repositoryObject" /> is null.
        /// </exception>
        public static RepositoryObjectList SubscribeToObject(
            IUserStorageService userStorageService,
            RepositoryObject repositoryObject,
            Guid userId,
            bool onlyMyOwnSubscription = false,
            bool createIfNotExists = false)
        {
            #region Argument Check

            if (userStorageService == null)
            {
                throw new ArgumentNullException("userStorageService");
            }

            if (repositoryObject == null)
            {
                throw new ArgumentNullException("repositoryObject");
            }

            #endregion

            const string subscribesToAttrName = Metamodel.Attributes.SubscribesTo;

            IConfiguration configuration = repositoryObject.GetConfiguration();
            IRepository repository = configuration.GetRepository();

            RepositoryObjectList subscriptions = FindSubscriptions(
                userStorageService,
                repository,
                configuration,
                userId,
                createIfNotExists,
                onlyMyOwnSubscription);

            var saveList = new RepositoryObjectList();
            subscriptions.DoForEach(
                delegate(RepositoryObject item)
                {
                    var subscribesToAttr = (MultiLink)item.Attributes[subscribesToAttrName].Value;
                    var subscribesToLinks = subscribesToAttr.GetHyperLinks();
                    if (subscribesToLinks.Contains(repositoryObject.GetRemoteRepositoryHyperlink()))
                    {
                        return;
                    }
                    subscribesToLinks.Add(repositoryObject.GetRemoteRepositoryHyperlink());
                    subscribesToAttr.SetHyperLinks(subscribesToLinks);

                    saveList.Add(item);
                });


            saveList.SaveData();

            return subscriptions;
        }

        /// <summary>
        ///     Unssubscribes user from the specified object.
        /// </summary>
        /// <param name="userStorageService">
        ///     QEF service to retreive data about users and groups.
        /// </param>
        /// <param name="repositoryObject">
        ///     Object, which user subscribes from.
        /// </param>
        /// <param name="userId">
        ///     Identifier of QEF user, which subscribes from object.
        /// </param>
        /// <returns>
        ///     List of subscriptions for the specified user.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        ///     <paramref name="userStorageService" /> is null.<br />
        ///     -or-<br />
        ///     <paramref name="repositoryObject" /> is null.
        /// </exception>
        public static RepositoryObjectList UnsubscribeFromObject(
            IUserStorageService userStorageService,
            RepositoryObject repositoryObject,
            Guid userId,
            bool onlyMyOwnSubscription = false)
        {
            #region Argument Check

            if (userStorageService == null)
            {
                throw new ArgumentNullException("userStorageService");
            }

            if (repositoryObject == null)
            {
                throw new ArgumentNullException("repositoryObject");
            }

            #endregion

            const string subscribesToAttrName = Metamodel.Attributes.SubscribesTo;

            IConfiguration configuration = repositoryObject.GetConfiguration();
            IRepository repository = configuration.GetRepository();

            RepositoryObjectList subscriptions = FindSubscriptions(
                userStorageService,
                repository,
                configuration,
                userId,
                false,
                onlyMyOwnSubscription);

            subscriptions.DoForEach(
                delegate(RepositoryObject item)
                {
                    MultiLink subscribesToAttr = (MultiLink)item.Attributes[subscribesToAttrName].Value;
                    QHyperLinkList subscribesToLinks = subscribesToAttr.GetHyperLinks();

                    subscribesToLinks = new QHyperLinkList(
                        subscribesToLinks.Where(
                            delegate(QHyperLink link)
                            {
                                return link.IsDefaultRevision
                                    ? link.ObjectId.Id != repositoryObject.Id.Id
                                    : link.ObjectId != repositoryObject.Id;
                            }));

                    subscribesToAttr.SetHyperLinks(subscribesToLinks);
                });
            
            subscriptions.SaveData();

            return subscriptions;
        }

        #endregion
    }

    /// <summary>
    ///     Provides handlers relevant to Change Management System (CMS) functionality.
    /// </summary>
    public static class CMSHandlers
    {
        #region Constants

        public const string ApproveContentParamName = "ApproveContent";
        public const string NotificationParamName = "Notification";
        public const string PromoteContentParamName = "PromoteContent";
        public const string SkipGweContentParamName = "SkipGweContent";
        public const string PromoteLinksParamName = "PromoteLinks";
        public const string SubscribeParamName = "Subscribe";
        public const string UserIdParamName = "UserId";

        #endregion

        #region Private Methods

        private static string CreateHtmlLink(QHyperLink link, IConfiguration configuration)
        {
            RepositoryObject repositoryObject = configuration.FindObject(link);
            string slink = string.Format(
                "<a href=\"GeneratedHTML/{0}/{1}/{2}/{3}.html\" title=\"{4}\">{4}</a>",
                repositoryObject.RepositoryId,
                configuration.GetData().Name,
                repositoryObject.Template,
                repositoryObject.Id.Id,
                repositoryObject.Name);

            return slink;
        }

        private static void RaiseOnObjectChanged(
            IRepository repository,
            IConfiguration configuration,
            RepositoryObject repositoryObject,
            string path,
            IDictionary<string, ServerInfo> sharePointServerInfos)
        {
            if (OnObjectChanged != null)
            {
                OnObjectChanged(repository, configuration, repositoryObject, path, sharePointServerInfos);
            }
        }

        #endregion

        #region Internal Methods

        /// <summary>
        ///     Approves the specified object. After the object is approved, the form is refreshed.
        /// </summary>
        /// <param name="args">
        ///     ObjectDialog event arguments.
        /// </param>
        /// <param name="approvalState">
        ///     Approval state.
        /// </param>
        internal static void ApproveObject(ObjectDialogEventArgs args, string approvalState)
        {
            try
            {
                IQisServer qisServer = args.QisServer;
                var userStorageService = qisServer.GetQefService<IUserStorageService>();
                var mailService = qisServer.GetQefService<IMailService>();
                var runtimeDiscoveryService = qisServer.GetQefService<IRuntimeDiscoveryService>();
                var sharePointServerInfos = runtimeDiscoveryService.GetSharePointServerInfos(qisServer.SecureToken);

                RepositoryObject repositoryObject = args.Object;

                if (repositoryObject.IsFrozen)
                {
                    IConfiguration configuration = repositoryObject.GetConfiguration();

                    IRepository repository = configuration.GetRepository();

                    string path = qisServer.GetGeneratedHtmlPath(repository.Id, configuration.GetData().Name);

                    //args.Message = string.Format("Object is already approved. (Press F5 to continue)");

                    RaiseOnObjectChanged(repository, configuration, repositoryObject, path, sharePointServerInfos);

                    return;
                }

                Guid userId = args.Session.UserId;

                ObjectScripts.UpdateAttributeValues(repositoryObject, args.Dialog.Controls);

                bool approveContent = true;

                if (!args.Request[ApproveContentParamName].IsBlank())
                {
                    Boolean.TryParse(args.Request[ApproveContentParamName], out approveContent);
                }

                bool sendMailNotification = false;

                if (!args.Request[NotificationParamName].IsBlank())
                {
                    Boolean.TryParse(args.Request[NotificationParamName], out sendMailNotification);
                }

                bool SkipGweContent = false;

                if (!args.Request[SkipGweContentParamName].IsBlank())
                {
                    Boolean.TryParse(args.Request[SkipGweContentParamName], out SkipGweContent);
                }

                OidResultList resultList = CMS.ApproveObject(
                    userStorageService,
                    mailService,
                    repositoryObject,
                    userId,
                    approvalState,
                    approveContent,
                    sendMailNotification,
                    rollbackOnFailure:false,
                    adminApproval:false,
                    SkipGweContent:SkipGweContent);

                if (resultList.HasErrors)
                {
                    resultList.Where(item => item.Error != null).DoForEach(
                        (item, index)
                            =>
                            args.Message += string.Format(
                                "{0}{1}",
                                item.Error.Message,
                                index < resultList.Count - 1 ? "\r\n" : string.Empty));

                    return;
                }
            }
            catch (QInvalidScriptOperationException invalidScriptOperationException)
            {
                args.Message = invalidScriptOperationException.Message;

                return;
            }
            catch (Exception exception)
            {
                args.Message = exception.Message + string.Format("\nStack Trace:\n{0}", exception.StackTrace);

                return;
            }
        }

        #endregion

        #region Public Events

        public static event Common.ObjectChangedDelegate OnObjectChanged;

        #endregion

        #region Public Methods

        public static bool AcknowledgeObject(ObjectDialogEventArgs args)
        {
            try
            {
                var userStorageService = args.QisServer.GetQefService<IUserStorageService>();
                Guid userId = args.Session.UserId;

                const string objectIdParam = "ObjId";

                string uri = args.Request[objectIdParam];

                if (string.IsNullOrEmpty(uri))
                {
                    throw new ArgumentException("Identifier of an object is not defined.");
                }

                QHyperLink link = new QHyperLink(new Oid(uri), true, string.Empty);

                IConfiguration configuration = args.Configuration;
                IRepository repository = configuration.GetRepository();
                RepositoryObject acknowledge = configuration.FindObject(link);

                CMS.AcknowledgeObject(userStorageService, repository, configuration, acknowledge, userId);

                return true;
            }
            catch (QInvalidScriptOperationException invalidScriptOperationException)
            {
                args.Message = invalidScriptOperationException.Message;

                return false;
            }
            catch (Exception exception)
            {
                args.Message = exception.Message + string.Format("\nStack Trace:\n{0}", exception.StackTrace);

                return false;
            }
        }

        /// <summary>
        ///     Approves the specified object. After the object is approved, the form is refreshed.
        /// </summary>
        /// <param name="args">
        ///     ObjectDialog event arguments.
        /// </param>
        public static void ApproveObject(ObjectDialogEventArgs args)
        {
            ApproveObject(args, CMS.c_approvedApprovalState);
        }

        /// <summary>
        ///     Circulates the specified object. After the object is circulated, the form is refreshed.
        /// </summary>
        /// <param name="args">
        ///     ObjectDialog event arguments.
        /// </param>
        public static void CirculateObject(ObjectDialogEventArgs args)
        {
            try
            {
                var userStorageService = args.QisServer.GetQefService<IUserStorageService>();
                var mailService = args.QisServer.GetQefService<IMailService>();
                RepositoryObject repositoryObject = args.Object;
                Guid userId = args.Session.UserId;

                bool notification = false;

                if (!args.Request[NotificationParamName].IsBlank())
                {
                    Boolean.TryParse(args.Request[NotificationParamName], out notification);
                }

                ObjectScripts.UpdateAttributeValues(args.Object, args.Dialog.Controls);

                CMS.CirculateObject(userStorageService, mailService, repositoryObject, userId, notification);

                UIScripts.RefreshTopForm(args.UIActions);
            }
            catch (QInvalidScriptOperationException invalidScriptOperationException)
            {
                args.Message = invalidScriptOperationException.Message;

                return;
            }
            catch (Exception exception)
            {
                args.Message = exception.Message + string.Format("\nStack Trace:\n{0}", exception.StackTrace);

                return;
            }
        }

        /// <summary>
        ///     Creates a comment for the specified object. After the commented is created, the form is refreshed.
        /// </summary>
        /// <param name="args">
        ///     ObjectDialog event arguments.
        /// </param>
        public static bool CommentObject(ObjectDialogEventArgs args)
        {
            try
            {
                RepositoryObject repositoryObject = args.Object;

                RepositoryObject commentObject = CMS.CommentObject(repositoryObject);

                UIScripts.RefreshObjectForEditing(
                    args.Session.Id,
                    commentObject,
                    args.TemplateCustomization,
                    args.UIActions);

                return true;
            }
            catch (QInvalidScriptOperationException invalidScriptOperationException)
            {
                args.Message = invalidScriptOperationException.Message;

                return false;
            }
            catch (Exception exception)
            {
                args.Message = exception.Message + string.Format("\nStack Trace:\n{0}", exception.StackTrace);

                return false;
            }
        }

        public static void FindAcknowledgeList(ObjectDialogEventArgs args)
        {
            RepositoryObjectList acknowledgeList = new RepositoryObjectList();

            try
            {
                IUserStorageService userStorageService = args.QisServer.GetQefService<IUserStorageService>();
                IConfiguration configuration = args.Configuration;
                IRepository repository = configuration.GetRepository();

                Guid userId = args.Session.UserId;

                if (!args.Request[UserIdParamName].IsBlank())
                {
                    userId = new Guid(args.Request[UserIdParamName]);
                }

                acknowledgeList = CMS.FindAcknowledgeList(
                    userStorageService,
                    userId,
                    repository,
                    configuration);

                string style = "";

                QHyperLinkList distinctlinks =
                    new QHyperLinkList(acknowledgeList.Select(item => item.GetRemoteRepositoryHyperlink()).Distinct());

                distinctlinks.DoForEach(item => style += CreateHtmlLink(item, configuration));

                string script = string.Format(
                    "$('body').prepend('<span class=\"UserAcknowledgeList\">{0}</span>');",
                    style);

                args.UIActions.Add(new JavaScriptUIAction(script));
            }
            catch (QInvalidScriptOperationException invalidScriptOperationException)
            {
                args.Message = invalidScriptOperationException.Message;

                return;
            }
            catch (Exception exception)
            {
                args.Message = exception.Message + string.Format("\nStack Trace:\n{0}", exception.StackTrace);

                return;
            }
        }

        public static RepositoryObjectList FindAcknowledgeListQRV(FilterScriptWrapperEventArgs args)
        {
            RepositoryObjectList acknowledgeList = new RepositoryObjectList();

            try
            {
                IUserStorageService userStorageService = args.QisServer.GetQefService<IUserStorageService>();
                IConfiguration configuration = args.Configuration;
                IRepository repository = configuration.GetRepository();

                Guid userId = args.Session.UserId;

                if (!args.Request[UserIdParamName].IsBlank())
                {
                    userId = new Guid(args.Request[UserIdParamName]);
                }

                acknowledgeList = CMS.FindAcknowledgeList(
                    userStorageService,
                    userId,
                    repository,
                    configuration);

                return acknowledgeList;
            }
            catch (QInvalidScriptOperationException invalidScriptOperationException)
            {
                args.Message = invalidScriptOperationException.Message;

                return acknowledgeList;
            }
            catch (Exception exception)
            {
                args.Message = exception.Message + string.Format("\nStack Trace:\n{0}", exception.StackTrace);

                return acknowledgeList;
            }
        }

        /// <summary>
        ///     Finds comments.
        /// </summary>
        /// <param name="args">
        ///     QRV event arguments.
        /// </param>
        /// <returns>
        ///     List of comments.
        /// </returns>
        public static RepositoryObjectList FindComments(FilterScriptWrapperEventArgs args)
        {
            RepositoryObjectList comments = new RepositoryObjectList();

            try
            {
                string uri = args.Request[WebFormsUrlParams.Id];

                if (string.IsNullOrEmpty(uri))
                {
                    throw new ArgumentException("Identifier of an object is not defined.");
                }

                QHyperLink link = new QHyperLink(new Uri(uri), string.Empty);

                IConfiguration configuration = args.Configuration;
                RepositoryObject repositoryObject = configuration.FindObject(link);

                comments = CMS.FindComments(repositoryObject);

                return comments;
            }
            catch (QInvalidScriptOperationException invalidScriptOperationException)
            {
                args.Message = invalidScriptOperationException.Message;

                return comments;
            }
            catch (Exception exception)
            {
                args.Message = exception.Message + string.Format("\nStack Trace:\n{0}", exception.StackTrace);

                return comments;
            }
        }

        /// <summary>
        ///     Finds subscriptions.
        /// </summary>
        /// <param name="args">
        ///     QRV event arguments.
        /// </param>
        /// <returns>
        ///     List of subsciptions.
        /// </returns>
        public static void FindSubscriptions(ObjectDialogEventArgs args)
        {
            try
            {
                const string subscribesToAttrName = Metamodel.Attributes.SubscribesTo;

                IUserStorageService userStorageService = (IUserStorageService)args.QisServer.GetQefService(
                    ServiceId.UserStorage);
                IConfiguration configuration = args.Object.GetConfiguration();
                IRepository repository = configuration.GetRepository();
                Guid userId = args.Session.UserId;

                RepositoryObjectList subscriptions = CMS.FindSubscriptions(
                    userStorageService,
                    repository,
                    configuration,
                    userId);

                string style = "";

                QHyperLinkList subscribesToLinks = new QHyperLinkList();

                subscriptions.DoForEach(
                    delegate(RepositoryObject item)
                    {
                        MultiLink subscribesToAttr =
                            (MultiLink)ObjectScripts.GetAttributeValue(item, subscribesToAttrName);

                        subscribesToAttr.GetHyperLinks().DoForEach(subscribesToLinks.Add);
                    });

                QHyperLinkList distinctlinks = new QHyperLinkList(subscribesToLinks.Distinct());

                distinctlinks.DoForEach(item => style += CreateHtmlLink(item, configuration));

                string script = string.Format(
                    "$('body').prepend('<span class=\"UserSubscriptions\">{0}</span>');",
                    style);

                args.UIActions.Add(new JavaScriptUIAction(script));
            }
            catch (QInvalidScriptOperationException invalidScriptOperationException)
            {
                args.Message = invalidScriptOperationException.Message;

                return;
            }
            catch (Exception exception)
            {
                args.Message = exception.Message + string.Format("\nStack Trace:\n{0}", exception.StackTrace);

                return;
            }
        }

        public static void PromoteObject(ObjectDialogEventArgs args)
        {
            try
            {
                IQisServer qisServer = args.QisServer;
                var runtimeDiscoveryService = qisServer.GetQefService<IRuntimeDiscoveryService>();
                var mailService = qisServer.GetQefService<IMailService>();
                var sharePointServerInfos = runtimeDiscoveryService.GetSharePointServerInfos(qisServer.SecureToken);

                RepositoryObject repositoryObject = args.Object;

                IConfiguration privateWorkspace = repositoryObject.GetConfiguration();

                IConfiguration baseConfiguration = repositoryObject.GetConfiguration().GetBaseConfiguration();

                IRepository repository = privateWorkspace.GetRepository();

                string privateWorkSpacePath = qisServer.GetGeneratedHtmlPath(
                    repository.Id,
                    privateWorkspace.GetData().Name);
                string configurationPath = qisServer.GetGeneratedHtmlPath(
                    repository.Id,
                    baseConfiguration.GetData().Name);

                if (repositoryObject.ConfigurationInfo.IsInheritedRevision)
                {
                    RaiseOnObjectChanged(
                        repository,
                        privateWorkspace,
                        repositoryObject,
                        privateWorkSpacePath,
                        sharePointServerInfos);

                    return;
                }

                bool promoteContent = true;

                if (!args.Request[PromoteContentParamName].IsBlank())
                {
                    Boolean.TryParse(args.Request[PromoteContentParamName], out promoteContent);
                }

                bool SkipGweContent = true;

                if (!args.Request[SkipGweContentParamName].IsBlank())
                {
                    Boolean.TryParse(args.Request[SkipGweContentParamName], out SkipGweContent);
                }

                bool promoteLinks = false;

                if (!args.Request[PromoteLinksParamName].IsBlank())
                {
                    Boolean.TryParse(args.Request[PromoteLinksParamName], out promoteLinks);
                }

                bool notification = false;

                if (!args.Request[NotificationParamName].IsBlank())
                {
                    Boolean.TryParse(args.Request[NotificationParamName], out notification);
                }

                if (!baseConfiguration.IsConnected)
                {
                    baseConfiguration.Connect();
                }

                if (promoteContent)
                {
                    var repositoryObjectList = new RepositoryObjectList();

                    if (SkipGweContent)
                    {
                        repositoryObjectList.AddRange(
                            repositoryObject.GetContains()
                                .Where(item => item.ConfigurationInfo.IsInheritedRevision == false)
                                .Where(item => !item.IsGweObject()));
                    }
                    else
                    {
                        repositoryObjectList.AddRange(
                            repositoryObject.GetContains()
                                .Where(item => item.ConfigurationInfo.IsInheritedRevision == false));
                    }

                    repositoryObjectList.DoForEach(
                        item =>
                        {
                            privateWorkspace.PromoteToBaseConfiguration(item.Id);
                            RaiseOnObjectChanged(
                                repository,
                                baseConfiguration,
                                item,
                                configurationPath,
                                sharePointServerInfos);

                            if (notification)
                            {
                                CMS.SendMailNotificationToSubscribers(
                                    mailService,
                                    baseConfiguration.FindObject(item.Id));
                            }
                        });
                }

                if (promoteLinks)
                {
                    var repositoryObjectList = new RepositoryObjectList();

                    repositoryObjectList.AddRange(
                        repositoryObject.GetLinksTo().Where(item => item.ConfigurationInfo.IsInheritedRevision == false));

                    repositoryObjectList.DoForEach(
                        item =>
                        {
                            privateWorkspace.PromoteToBaseConfiguration(item.Id);
                            RaiseOnObjectChanged(
                                repository,
                                baseConfiguration,
                                item,
                                configurationPath,
                                sharePointServerInfos);

                            if (notification)
                            {
                                CMS.SendMailNotificationToSubscribers(
                                    mailService,
                                    baseConfiguration.FindObject(item.Id));
                            }
                        });
                }

                privateWorkspace.PromoteToBaseConfiguration(repositoryObject.Id);
                RaiseOnObjectChanged(
                    repository,
                    baseConfiguration,
                    repositoryObject,
                    configurationPath,
                    sharePointServerInfos);

                if (notification)
                {
                    CMS.SendMailNotificationToSubscribers(
                        mailService,
                        baseConfiguration.FindObject(repositoryObject.Id));
                }
            }
            catch (QInvalidScriptOperationException invalidScriptOperationException)
            {
                args.Message = invalidScriptOperationException.Message;

                return;
            }
            catch (Exception exception)
            {
                args.Message = exception.Message + string.Format("\nStack Trace:\n{0}", exception.StackTrace);

                return;
            }
        }

        /// <summary>
        ///     Subscribes to the specified object. After the object is subscribed to, the form is refreshed.
        /// </summary>
        /// <param name="args">
        ///     ObjectDialog event arguments.
        /// </param>
        public static bool SubscribeToObject(ObjectDialogEventArgs args)
        {
            try
            {
                IUserStorageService userStorageService = args.QisServer.GetQefService<IUserStorageService>();
                Guid userId = args.Session.UserId;

                const string c_objid = "ObjId";

                string uri = args.Request[c_objid];

                if (string.IsNullOrEmpty(uri))
                {
                    throw new ArgumentException("Identifier of an object is not defined.");
                }

                QHyperLink link = new QHyperLink(new Oid(uri), true, string.Empty);

                IConfiguration configuration = args.Object.GetConfiguration();
                RepositoryObject repositoryObject = configuration.FindObject(link);

                bool subscribe = true;

                if (!args.Request[SubscribeParamName].IsBlank())
                {
                    Boolean.TryParse(args.Request[SubscribeParamName], out subscribe);
                }

                if (subscribe)
                {
                    CMS.SubscribeToObject(userStorageService, repositoryObject, userId);
                }
                else
                {
                    CMS.UnsubscribeFromObject(userStorageService, repositoryObject, userId);
                }

                return true;
            }
            catch (QInvalidScriptOperationException invalidScriptOperationException)
            {
                args.Message = invalidScriptOperationException.Message;

                return false;
            }
            catch (Exception exception)
            {
                args.Message = exception.Message + string.Format("\nStack Trace:\n{0}", exception.StackTrace);

                return false;
            }
        }

        /// <summary>
        ///     Unsubscribes from the specified object. After the object is unsubscribed from, the form is refreshed.
        /// </summary>
        /// <param name="args">
        ///     ObjectDialog event arguments.
        /// </param>
        public static bool UnsubscribeFromObject(ObjectDialogEventArgs args)
        {
            try
            {
                IUserStorageService userStorageService =
                    args.QisServer.GetQefService(ServiceId.UserStorage) as IUserStorageService;
                RepositoryObject repositoryObject = args.Object;
                Guid userId = args.Session.UserId;

                CMS.UnsubscribeFromObject(userStorageService, repositoryObject, userId);

                UIScripts.RefreshParentForm(args.UIActions);

                return true;
            }
            catch (QInvalidScriptOperationException invalidScriptOperationException)
            {
                args.Message = invalidScriptOperationException.Message;

                return false;
            }
            catch (Exception exception)
            {
                args.Message = exception.Message + string.Format("\nStack Trace:\n{0}", exception.StackTrace);

                return false;
            }
        }

        #endregion
    }
}
