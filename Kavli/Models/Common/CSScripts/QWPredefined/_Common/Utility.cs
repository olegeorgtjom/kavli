﻿using System;
using System.Linq;
using QCommon.Extensions;
using Qef.Common;
using Qef.Common.DataStorage;
using Qef.Common.Modules;
using Qis.Common;
using Qis.Common.Qsql;
using Qis.Common.Qsql.SelectQuery;
using Qis.Common.Scripting;
using Qis.Web.Forms.Common.Scripting;

namespace Qis.Module.Scripts
{
    public static class Utility
    {
        #region Private Methods

        private static RepositoryObjectList FindObjects(
            IRepository repository,
            IConfiguration configuration)
        {
            var options = new QueryOptions
            {
                Scope = ObjectsScope.AllRevisionsAndLanguageVariantsInRepository
            };

            var query = new SelectExpression(
                new ColumnExpressions(new ObjectIdExpression().AsArray()),
                new AllTemplatesTable(),
                null,
                null);

            return repository.RunQsqlExpression(query, configuration.Id, options);
        }

        private static Qid GetModuleId(string moduleNameOrId)
        {
            Qid moduleId = null;

            switch (moduleNameOrId.ToLower())
            {
                case "qef":
                    moduleId = QefConst.QefFullId;

                    break;
                case "qis":
                    moduleId = QisConst.QisId;

                    break;
                default:
                    try
                    {
                        moduleId = new Qid(moduleNameOrId);
                    }
                    catch
                    {
                        // Nothing to do.
                    }

                    break;
            }

            return moduleId;
        }

        #endregion

        #region Public Methods

        public static RepositoryObjectList FindReservedObjects(
            IRepository repository,
            IConfiguration configuration)
        {
            var documentRepositoryObjects = FindObjects(repository, configuration);

            return new RepositoryObjectList(
                documentRepositoryObjects.Where(
                    item => item.GetReserveStatus().Type == ObjectReserveResultType.Reserved));
        }

        public static void FindReservedObjects(ObjectDialogEventArgs args)
        {
            var repositoryObject = args.Object;
            var configuration = repositoryObject.GetConfiguration();
            var repository = configuration.GetRepository();

            var reservedRepositoryObjects = FindReservedObjects(repository, configuration);

            args.Message = string.Format("Number of reserved objects: " + reservedRepositoryObjects.Count);
        }

        public static void GetAddonDataStorageConnectionString(GeneralScriptArgs args)
        {
            var repositoryId = args.Parameters[0];
            var addonName = args.Parameters[1];
            var addonId = AddonId.Parse(addonName);
            var repository = args.Qis.FindRepository(RepId.Parse(repositoryId));

            if (repository == null)
            {
                return;
            }

            var addon = repository.GetAddon(addonId);

            if (addon == null)
            {
                return;
            }

            var dataStorageRefenrece = addon.Info.GetDataStorageReference();

            if (dataStorageRefenrece == null)
            {
                return;
            }

            args.Result = dataStorageRefenrece.ConnectionString;
        }

        public static void GetAddonDataStorageSchemaName(GeneralScriptArgs args)
        {
            var repositoryId = args.Parameters[0];
            var addonName = args.Parameters[1];
            var addonId = AddonId.Parse(addonName);
            var repository = args.Qis.FindRepository(RepId.Parse(repositoryId));

            if (repository == null)
            {
                return;
            }

            var addon = repository.GetAddon(addonId);

            if (addon == null)
            {
                return;
            }

            var dataStorageRefenrece = addon.Info.GetDataStorageReference();

            if (dataStorageRefenrece == null)
            {
                return;
            }

            args.Result = dataStorageRefenrece.SchemaName;
        }

        public static void GetAddonEnabled(GeneralScriptArgs args)
        {
            var repositoryId = args.Parameters[0];
            var addonName = args.Parameters[1];
            var addonId = AddonId.Parse(addonName);
            var repository = args.Qis.FindRepository(RepId.Parse(repositoryId));

            if (repository == null)
            {
                return;
            }

            var addon = repository.GetAddon(addonId);

            if (addon == null || addon.Info == null)
            {
                return;
            }

            args.Result = addon.Info.Enabled.ToString();
        }

        public static void GetDataSourceConnectionString(GeneralScriptArgs args)
        {
            var moduleName = args.Parameters[0];
            var dataSourceName = args.Parameters[1];
            var moduleId = GetModuleId(moduleName);

            var dataStorageService = args.Qis.GetQefService<IDataStorageService>();

            if (dataStorageService == null)
            {
                return;
            }

            var dataSource = dataStorageService.FindDataSource(moduleId, dataSourceName);

            if (dataSource == null)
            {
                return;
            }

            args.Result = dataSource.ConnectionData.ConnectionString;
        }

        public static void GetDataSourceSchemaName(GeneralScriptArgs args)
        {
            var moduleName = args.Parameters[0];
            var dataSourceName = args.Parameters[1];
            var moduleId = GetModuleId(moduleName);

            var dataStorageService = args.Qis.GetQefService<IDataStorageService>();

            if (dataStorageService == null)
            {
                return;
            }

            var dataSource = dataStorageService.FindDataSource(moduleId, dataSourceName);

            if (dataSource == null)
            {
                return;
            }

            args.Result = dataSource.ConnectionData.SchemaName;
        }

        public static void GetRepositoryDataStorageConnectionString(GeneralScriptArgs args)
        {
            var repositoryId = args.Parameters[0];
            var repository = args.Qis.FindRepository(RepId.Parse(repositoryId));

            if (repository == null)
            {
                return;
            }

            var repositoryData = repository.GetData();
            var dataStorageRefenrece = repositoryData.DataStorageReference;

            if (dataStorageRefenrece == null)
            {
                return;
            }

            args.Result = dataStorageRefenrece.ConnectionString;
        }

        public static void GetRepositoryDataStorageSchemaName(GeneralScriptArgs args)
        {
            var repositoryId = args.Parameters[0];
            var repository = args.Qis.FindRepository(RepId.Parse(repositoryId));

            if (repository == null)
            {
                return;
            }

            var repositoryData = repository.GetData();
            var dataStorageRefenrece = repositoryData.DataStorageReference;

            if (dataStorageRefenrece == null)
            {
                return;
            }

            args.Result = dataStorageRefenrece.SchemaName;
        }

        public static void GetRepositoryRoles(GeneralScriptArgs args)
        {
            var repositoryId = args.Parameters[0];
            var userIdStr = args.Parameters[1];

            var repository = args.Qis.FindRepository(RepId.Parse(repositoryId));
            if (repository == null)
            {
                return;
            }

            args.Result = (!userIdStr.IsBlank()
                ? repository.GetUserRepositoryRoles(new Guid(userIdStr))
                : repository.RepositoryRoleMap.Select(rm => rm.RoleId))
                .Select(r => r.ToString())
                .Distinct()
                .Join(";");
        }

        public static void RemoveReservation(ObjectDialogEventArgs args)
        {
            var repositoryObject = args.Object;
            var configuration = repositoryObject.GetConfiguration();
            var repository = configuration.GetRepository();

            string message = string.Empty;

            int numberOfUnreservedObjects = 0;

            var reservedRepositoryObjects = FindReservedObjects(repository, configuration);

            foreach (RepositoryObject reservedRepositoryObject in reservedRepositoryObjects)
            {
                int attempt = 0;
                ObjectReserveResult result;

                do
                {
                    attempt++;

                    result = reservedRepositoryObject.GetReserveStatus();

                    result = repository.RemoveReservation(reservedRepositoryObject.Id.Id, result.UserId);

                    if (result.Type == ObjectReserveResultType.ReservationRemoved)
                    {
                        numberOfUnreservedObjects++;

                        break;
                    }

                    result = repository.GetReserveStatus(reservedRepositoryObject.Id.Id);
                }
                while (result.Type == ObjectReserveResultType.Reserved && attempt <= 5);
            }

            message += string.Format("Number of reserved objects: " + reservedRepositoryObjects.Count);
            message += Environment.NewLine;
            message += string.Format("Number of unreserved objects: " + numberOfUnreservedObjects);

            args.Message = message;
        }

        #endregion
    }
}
