﻿using System;
using System.Collections.Generic;
using System.Linq;
using QCommon.Extensions;
using QCommon.Log;
using Qef.Common.Log;
using Qef.Common.Utils;
using Qis.Common;
using Qis.Common.AttributeValues;
using Qis.Common.Metamodel;
using Qis.Common.Qsql;
using Qis.Common.Qsql.Operations;
using Qis.Common.Qsql.Operations.Logical;
using Qis.Common.Qsql.SelectQuery;
using Qis.Web.Forms.Common.Scripting;

namespace Qis.Module.Scripts
{
    public static class Search
    {
        #region Nested Types

        public static class Handlers
        {
            #region Constants

            private const string c_relationSearchParamName = "RelationSearch";
            private const string c_relationTypesParamName = "RelationTypes";
            private const string c_templatesParamName = "Templates";

            #endregion

            #region Public Methods

            public static RepositoryObjectList SearchAdvanced(FilterScriptEventArgs args)
            {
                /*
                QueryResultView.aspx?File=Search.xml&
                    RepId=Demo&
                    ConfId=585fa76b-1511-43ca-ab88-b2ce023f77fe&
                    Templates=BusinessProcessNetwork;WorkFlowDiagram;BusinessProcessDiagram&
                    Description=Process flow&
                    Responsible=Kuno Brodersen&
                    Accountable=Usman Ali Munawar&
                    ApprovalState=Approved&
                    RelationTypes=Links;LinkedBy;Contains;ContainedIn&
                    RelationSearch={Name of object}
                */

                ILogWriter logWriter = args.QisServer.GetQefService<ILogService>()
                    .GetLogWriter(QisConst.QisId, Source.Parse("ObjectScripts"));

                try
                {
                    args.CanFilter = true;
                    args.CanSort = true;

                    #region Collect data from URL

                    // Attribute names to values map.
                    string[] predefinedParamNames = new[]
                    {
                        "RepId",
                        "ConfId",
                        "File",
                        "Customization",
                        c_relationTypesParamName,
                        c_relationSearchParamName,
                        c_templatesParamName
                    };
                    IList<Tuple<string, string>> attributeNameToValueMap = new List<Tuple<string, string>>();

                    foreach (string key in args.Request.Keys)
                    {
                        if (predefinedParamNames.Contains(key))
                        {
                            continue;
                        }

                        var value = args.Request[key].Trim();

                        if (value.IsBlank())
                        {
                            continue;
                        }

                        attributeNameToValueMap.Add(new Tuple<string, string>(key, value));
                    }

                    // Templates.
                    string templatesParam = args.Request[c_templatesParamName];
                    string[] templates = null;

                    if (!templatesParam.IsBlank())
                    {
                        templates = templatesParam.Split(';');
                        templates.DoForEach(item => item.Trim());
                    }

                    // Relation types to values.
                    string relationTypesParam = args.Request[c_relationTypesParamName];
                    string relationSearchParam = args.Request[c_relationSearchParamName];
                    Scripts.RelationType[] relationTypes = null;

                    if (!relationTypesParam.IsBlank())
                    {
                        relationTypes =
                            relationTypesParam
                                .Split(';')
                                .Select(
                                    item =>
                                    {
                                        Scripts.RelationType relationType;

                                        Enum.TryParse(item, out relationType);

                                        return relationType;
                                    })
                                .Distinct()
                                .ToArray();
                    }

                    string message =
                        string.Format(
                            "Templates: {0}{1}",
                            templates != null ? string.Join(", ", templates) : "all templates",
                            Environment.NewLine);
                    message +=
                        string.Format(
                            "Attributes: {0}{1}",
                            attributeNameToValueMap != null && attributeNameToValueMap.Count > 0
                                ? string.Join(", ", attributeNameToValueMap.Select(item => item.Item1))
                                : "all attributes",
                            Environment.NewLine);
                    message +=
                        string.Format(
                            "Values: {0}{1}",
                            attributeNameToValueMap != null && attributeNameToValueMap.Count > 0
                                ? string.Join(", ", attributeNameToValueMap.Select(item => item.Item2))
                                : "all attributes",
                            Environment.NewLine);
                    message +=
                        string.Format(
                            "Relation types: {0}{1}",
                            relationTypes != null
                                ? string.Join(", ", relationTypes.Select(item => item.ToString()))
                                : "all types",
                            Environment.NewLine);
                    message +=
                        string.Format(
                            "Relation value: {0}{1}",
                            !relationSearchParam.IsBlank() ? relationSearchParam : "empty",
                            Environment.NewLine);

                    logWriter.Info(message);

                    #endregion

                    IConfiguration configuration = args.Configuration;
                    IRepository repository = configuration.GetRepository();

                    var repositoryObjects = Scripts.SearchObjectsAdvanced(
                        repository,
                        configuration,
                        templates,
                        attributeNameToValueMap,
                        relationTypes,
                        relationSearchParam,
                        true,
                        args.UIFilterExpression,
                        args.UISortExpression);

                    return repositoryObjects;
                }
                catch (Exception ex)
                {
                    logWriter.Error(ex.ToString());
                }

                return new RepositoryObjectList();
            }

            public static RepositoryObjectList SearchSimple(FilterScriptEventArgs args)
            {
                /*
                QueryResultView.aspx?File=Search.xml&
                    RepId=Demo&
                    ConfId=585fa76b-1511-43ca-ab88-b2ce023f77fe&
                    Templates=BusinessProcessNetwork;WorkFlowDiagram;BusinessProcessDiagram&
                    RelationTypes=Links;LinkedBy;Contains;ContainedBy&
                    RelationSearch={Name of object}
                */

                ILogWriter logWriter = args.QisServer.GetQefService<ILogService>()
                    .GetLogWriter(QisConst.QisId, Source.Parse("ObjectScripts"));

                try
                {
                    args.CanFilter = true;
                    args.CanSort = true;

                    #region Collect data from URL

                    // Templates.
                    string templatesParam = args.Request[c_templatesParamName];
                    string[] templates = null;

                    if (!templatesParam.IsBlank())
                    {
                        templates = templatesParam.Split(';');
                        templates.DoForEach(item => item.Trim());
                    }

                    // Relation types to values.
                    string relationTypesParam = args.Request[c_relationTypesParamName];
                    string relationSearchParam = args.Request[c_relationSearchParamName];
                    Scripts.RelationType[] relationTypes = null;

                    if (!relationTypesParam.IsBlank())
                    {
                        relationTypes =
                            relationTypesParam
                                .Split(';')
                                .Select(
                                    item =>
                                    {
                                        Scripts.RelationType relationType;

                                        Enum.TryParse(item, out relationType);

                                        return relationType;
                                    })
                                .ToArray();
                    }
                    else
                    {
                        relationTypes = EnumHelper.GetAllValues<Scripts.RelationType>().ToArray();
                    }

                    // Attributes.

                    string[] attributes = Metamodel.GetGenericSearchAttributes();

                    string message =
                        string.Format(
                            "Templates: {0}{1}",
                            templates != null ? string.Join(", ", templates) : "all templates",
                            Environment.NewLine);
                    message +=
                        string.Format(
                            "Attributes: {0}{1}",
                            attributes != null ? string.Join(", ", attributes) : "all attributes",
                            Environment.NewLine);
                    message +=
                        string.Format(
                            "Relation types: {0}{1}",
                            relationTypes != null
                                ? string.Join(", ", relationTypes.Select(item => item.ToString()))
                                : "all types",
                            Environment.NewLine);
                    message +=
                        string.Format(
                            "Relation value: {0}{1}",
                            !relationSearchParam.IsBlank() ? relationSearchParam : "empty",
                            Environment.NewLine);

                    logWriter.Info(message);

                    #endregion

                    IConfiguration configuration = args.Configuration;
                    IRepository repository = configuration.GetRepository();

                    var repositoryObjects = Scripts.SearchObjectsSimple(
                        repository,
                        configuration,
                        templates,
                        attributes,
                        relationTypes,
                        relationSearchParam,
                        true,
                        args.UIFilterExpression,
                        args.UISortExpression);

                    return repositoryObjects;
                }
                catch (Exception ex)
                {
                    logWriter.Error(ex.ToString());
                }

                return new RepositoryObjectList();
            }

            #endregion
        }

        public static class Scripts
        {
            #region Nested Types

            public enum RelationType
            {
                Links,
                LinkedBy,
                Contains,
                ContainedIn
            }

            /// <summary>
            ///     Types of search.
            /// </summary>
            public enum SearchType
            {
                /// <summary>
                ///     Uses logical AND to search objects by attribute values.
                /// </summary>
                And,

                /// <summary>
                ///     Uses logical OR to search objects by attribute values.
                /// </summary>
                Or
            }

            public class SearchValue
            {
                #region Constructors and Destructors

                public SearchValue()
                    : this(new List<string>())
                {
                    // Nothing to do
                }

                public SearchValue(List<string> values)
                    : this(values, SearchType.Or)
                {
                    // Nothing to do
                }

                public SearchValue(List<string> values, SearchType searchType)
                {
                    #region Argument Check

                    if (values == null)
                    {
                        throw new ArgumentNullException("values");
                    }

                    #endregion

                    Values = values;
                    SearchType = searchType;
                }

                #endregion

                #region Public Properties

                public SearchType SearchType
                {
                    get;
                    set;
                }

                public List<string> Values
                {
                    get;
                    private set;
                }

                #endregion
            }

            #endregion

            #region Constants

            private const string c_emptyFilterValue = "__IsNull__";

            #endregion

            #region Fields

            /// <summary>
            ///     Lists GUID-based predefined attributes
            /// </summary>
            private static readonly HashSet<string> s_uniqueIdentifierNames =
                new HashSet<string>(
                    new[]
                    {
                        PredefinedObjectAttribute.ObjectId.Name,
                        PredefinedObjectAttribute.RevisionId.Name,
                        PredefinedObjectAttribute.ConfigurationId.Name,
                        PredefinedObjectAttribute.BaseConfigurationId.Name
                    });

            #endregion

            #region Private Methods

            private static Dictionary<string, SearchValue> ConvertAttributeMap(
                IList<Tuple<string, string>> attributeNameToValueMap,
                SearchType searchType)
            {
                if (attributeNameToValueMap == null)
                {
                    return new Dictionary<string, SearchValue>();
                }

                Dictionary<string, SearchValue> result = attributeNameToValueMap
                    .Select(item => item.Item1)
                    .Distinct()
                    .ToDictionary(
                        item => item,
                        item => new SearchValue
                        {
                            SearchType = searchType
                        });
                attributeNameToValueMap.DoForEach(item => result[item.Item1].Values.Add(item.Item2));

                return result;
            }

            private static ILogicalExpression CreateAndOperationExpression(List<ILogicalExpression> expressions)
            {
                if (expressions.Count <= 1)
                {
                    return expressions.SingleOrDefault();
                }

                return MergeExpressions(
                    expressions,
                    (firstOperand, secondOperant) => new AndOperationExpression(firstOperand, secondOperant))
                    .SingleOrDefault();
            }

            private static ILogicalExpression CreateLogicalExpression(
                IMetamodel metamodel,
                IEnumerable<string> templates,
                string attributeName,
                SearchValue searchValue,
                bool detectMetamodelFormat,
                IWhere uiFilterExpression = null)
            {
                var expressions = new List<ILogicalExpression>();

                if (uiFilterExpression != null)
                {
                    expressions.Add(uiFilterExpression.Clause);
                }

                foreach (var attributeValue in searchValue.Values)
                {
                    string value = attributeValue;

                    if (value.IsBlank())
                    {
                        continue;
                    }

                    if (value == c_emptyFilterValue)
                    {
                        value = string.Empty;
                    }

                    if (!value.IsBlank() && detectMetamodelFormat)
                    {
                        IAttributeDefinition attributeDefinition =
                            (templates == null || !templates.Any()
                                ? metamodel.Templates
                                    .Where(item => item.Attributes.Contains(attributeName))
                                : metamodel
                                    .Templates
                                    .Where(
                                        item =>
                                            templates != null && templates.Contains(item.Name)
                                                && item.Attributes.Contains(attributeName)))
                                .Select(item => item.Attributes[attributeName])
                                .FirstOrDefault();

                        if (attributeDefinition != null)
                        {
                            switch (attributeDefinition.Format)
                            {
                                case AttributeFormat.SingleLink:
                                case AttributeFormat.MultiLink:
                                case AttributeFormat.SingleFile:
                                case AttributeFormat.MultiFile:

                                    attributeName +=
                                        AttributeValueOperand.AttributeChainDelimiter
                                            + PredefinedObjectAttribute.Name.Name;

                                    break;
                            }
                        }
                    }

                    Func<IScalarExpression, IScalarExpression, ILogicalExpression> operation =
                        (first, second) => new LikeOperationExpression(first, second);
                    var processedValue = !attributeValue.IsBlank()
                        ? string.Format("%{0}%", attributeValue)
                        : string.Empty;

                    #region Special processing for GUID-based predefined attributes

                    if (!attributeValue.IsBlank() && s_uniqueIdentifierNames.Contains(attributeName))
                    {
                        Guid guidValue;
                        if (Guid.TryParse(attributeValue, out guidValue))
                        { //search value contains valid GUID, compare directly 
                            operation = (first, second) => new EqualOperationExpression(first, second);
                            processedValue = guidValue.ToString();
                        }
                        else
                        { //no GUID in search value, assume it is some part of GUID.
                            //Convert to upper case to survive CI/CS collations.
                            processedValue = processedValue.ToUpper();
                        }
                    }

                    #endregion

                    expressions.Add(
                        operation(new AttributeValueOperand(attributeName), new ConstantOperand(processedValue)));
                }

                var createBinaryOperationExpression = GetCreateBinaryOperationExpression(searchValue.SearchType);

                return createBinaryOperationExpression(expressions);
            }

            private static ILogicalExpression CreateOrOperationExpression(List<ILogicalExpression> expressions)
            {
                if (expressions.Count <= 1)
                {
                    return expressions.SingleOrDefault();
                }

                return MergeExpressions(
                    expressions,
                    (firstOperand, secondOperant) => new OrOperationExpression(firstOperand, secondOperant))
                    .SingleOrDefault();
            }

            private static IWhere CreateWhereExpression(
                IMetamodel metamodel,
                IEnumerable<string> templates,
                Dictionary<string, SearchValue> request,
                SearchType searchType,
                bool detectMetamodelFormat,
                IWhere uiFilterExpression = null)
            {
                var expressions = new List<ILogicalExpression>();

                if (uiFilterExpression != null)
                {
                    expressions.Add(uiFilterExpression.Clause);
                }

                foreach (var requetstItem in request)
                {
                    string attributeName = requetstItem.Key;
                    SearchValue searchValue = requetstItem.Value;

                    ILogicalExpression expression = CreateLogicalExpression(
                        metamodel,
                        templates,
                        attributeName,
                        searchValue,
                        detectMetamodelFormat,
                        uiFilterExpression);

                    if (expression == null)
                    {
                        continue;
                    }

                    expressions.Add(expression);
                }

                var createBinaryOperationExpression = GetCreateBinaryOperationExpression(searchType);

                var binaryOperationExpression = createBinaryOperationExpression(expressions);

                if (binaryOperationExpression == null)
                {
                    return uiFilterExpression;
                }

                return new WhereExpression(
                    uiFilterExpression != null
                        ? new AndOperationExpression(
                            binaryOperationExpression,
                            uiFilterExpression.Clause)
                        : binaryOperationExpression);
            }

            /// <summary>
            ///     Creates and instance of the <see cref="WhereExpression" /> class for QSQL query.
            ///     As opposed to <see cref="CreateWhereExpression" />, the current method is optimized
            ///     for LinksTo relations.
            /// </summary>
            private static IWhere CreateWhereExpressionForLinks(
                string[] attributes,
                IEnumerable<ObjectId> ids,
                Func<List<ILogicalExpression>, ILogicalExpression> createBinaryOperationExpression,
                IWhere uiFilterExpression = null)
            {
                var expressions = new List<ILogicalExpression>();

                if (uiFilterExpression != null)
                {
                    expressions.Add(uiFilterExpression.Clause);
                }

                foreach (string attribute in attributes)
                {
                    foreach (var objectId in ids)
                    {
                        expressions.Add(
                            new LikeOperationExpression(
                                new AttributeValueOperand(attribute),
                                new ConstantOperand(string.Format("%{0}%", objectId))));
                    }
                }

                var binaryOperationExpression = createBinaryOperationExpression(expressions);

                if (binaryOperationExpression == null)
                {
                    return uiFilterExpression;
                }

                return new WhereExpression(
                    uiFilterExpression != null
                        ? new AndOperationExpression(
                            binaryOperationExpression,
                            uiFilterExpression.Clause)
                        : binaryOperationExpression);
            }

            private static RepositoryObjectList DoFilterObjects(
                IRepository repository,
                IConfiguration configuration,
                IEnumerable<Oid> oids,
                Dictionary<string, SearchValue> attributeNameToValueMap,
                SearchType searchType,
                QueryOptions options,
                IWhere uiFilterExpression = null,
                IOrder uiSortExpression = null,
                int top = 0,
                int skip = 0)
            {
                #region Argument Check

                if (repository == null)
                {
                    throw new ArgumentNullException("templates");
                }

                if (configuration == null)
                {
                    throw new ArgumentNullException("configuration");
                }

                if (options == null)
                {
                    throw new ArgumentNullException("options");
                }

                #endregion

                ITableSource table = oids.All(oid => oid.IsDefaultRevision)
                    ? new ObjectListTable(oids.Select(oid => oid.Id))
                    : new ObjectListTable(oids.Select(oid => oid.Revision));

                IWhere whereExpression = CreateWhereExpression(
                    repository.Metamodel,
                    new string[0],
                    attributeNameToValueMap,
                    searchType,
                    false,
                    uiFilterExpression);

                SelectExpression query = new SelectExpression(
                    new ColumnExpressions(new[] {new ObjectIdExpression()}),
                    table,
                    whereExpression,
                    uiSortExpression,
                    top,
                    skip);

                options = new QueryOptions
                {
                    Comparison = SqlComparison.DefaultCaseInsensitive,
                    CurrentLanguage = options.CurrentLanguage,
                    IncludeDeleted = options.IncludeDeleted,
                    Scope = options.Scope
                };

                return repository.RunQsqlExpression(query, configuration.Id, options);
            }

            private static RepositoryObjectList DoFindObjects(
                IRepository repository,
                IConfiguration configuration,
                string[] templates,
                Dictionary<string, SearchValue> attributeNameToValueMap,
                SearchType searchType,
                bool detectMetamodelFormat,
                QueryOptions options,
                IWhere uiFilterExpression = null,
                IOrder uiSortExpression = null,
                int top = 0,
                int skip = 0)
            {
                #region Argument Check

                if (repository == null)
                {
                    throw new ArgumentNullException("templates");
                }

                if (configuration == null)
                {
                    throw new ArgumentNullException("configuration");
                }

                if (options == null)
                {
                    throw new ArgumentNullException("options");
                }

                #endregion

                ITableSource table = templates == null || templates.Count() <= 0
                    ? (ITableSource)new AllTemplatesTable()
                    : new TemplateSetTable(templates);

                IWhere whereExpression = CreateWhereExpression(
                    repository.Metamodel,
                    templates,
                    attributeNameToValueMap,
                    searchType,
                    detectMetamodelFormat,
                    uiFilterExpression);

                SelectExpression query = new SelectExpression(
                    new ColumnExpressions(new[] {new ObjectIdExpression()}),
                    table,
                    whereExpression,
                    uiSortExpression,
                    top,
                    skip);

                options = new QueryOptions
                {
                    Comparison = SqlComparison.DefaultCaseInsensitive,
                    CurrentLanguage = options.CurrentLanguage,
                    IncludeDeleted = options.IncludeDeleted,
                    Scope = options.Scope
                };

                return repository.RunQsqlExpression(query, configuration.Id, options);
            }

            private static RepositoryObjectList FindObjectsByLinkedBy(
                IRepository repository,
                IConfiguration configuration,
                string[] templates,
                string searchBy,
                SearchType searchType,
                bool detectMetamodelFormat,
                IWhere uiFilterExpression = null,
                IOrder uiSortExpression = null)
            {
                #region Argument Check

                if (repository == null)
                {
                    throw new ArgumentNullException("templates");
                }

                if (configuration == null)
                {
                    throw new ArgumentNullException("configuration");
                }

                #endregion

                if (searchBy.IsBlank())
                {
                    return new RepositoryObjectList();
                }

                ITableSource table = templates == null || templates.Count() <= 0
                    ? (ITableSource)new AllTemplatesTable()
                    : new TemplateSetTable(templates);

                Dictionary<string, SearchValue> attributeNameToValueMap = new Dictionary<string, SearchValue>
                {
                    {
                        PredefinedObjectAttribute.Name.Name, new SearchValue(
                            new List<string>
                            {
                                searchBy
                            })
                    }
                };

                IWhere whereExpression = CreateWhereExpression(
                    repository.Metamodel,
                    templates,
                    attributeNameToValueMap,
                    searchType,
                    detectMetamodelFormat,
                    uiFilterExpression);

                SelectExpression query = new SelectExpression(
                    new ColumnExpressions(new[] {new ObjectIdExpression()}),
                    table,
                    whereExpression,
                    uiSortExpression);

                QueryOptions options = new QueryOptions
                {
                    Comparison = SqlComparison.DefaultCaseInsensitive
                };

                var repositoryObjects = repository.RunQsqlExpression(query, configuration.Id, options);

                List<QHyperLink> linksHyperLinkList = new List<QHyperLink>();

                repositoryObjects.DoForEach(
                    item =>
                        item
                            .Attributes
                            .DoForEach(
                                item2 => linksHyperLinkList.AddRange(item2.Value.GetHyperLinks())));

                return configuration.GetObjectSet(new QHyperLinkList(linksHyperLinkList.Distinct()));
            }

            private static RepositoryObjectList FindObjectsByLinks(
                IRepository repository,
                IConfiguration configuration,
                string[] templates,
                string searchBy,
                SearchType searchType,
                bool detectMetamodelFormat,
                IWhere uiFilterExpression = null,
                IOrder uiSortExpression = null)
            {
                #region Argument Check

                if (repository == null)
                {
                    throw new ArgumentNullException("templates");
                }

                if (configuration == null)
                {
                    throw new ArgumentNullException("configuration");
                }

                #endregion

                if (searchBy.IsBlank())
                {
                    return new RepositoryObjectList();
                }

                ITableSource table = new AllTemplatesTable();

                Dictionary<string, SearchValue> attributeNameToValueMap = new Dictionary<string, SearchValue>
                {
                    {
                        PredefinedObjectAttribute.Name.Name, new SearchValue(
                            new List<string>
                            {
                                searchBy
                            })
                    }
                };

                IWhere whereExpression = CreateWhereExpression(
                    repository.Metamodel,
                    templates,
                    attributeNameToValueMap,
                    searchType,
                    detectMetamodelFormat,
                    uiFilterExpression);

                SelectExpression query = new SelectExpression(
                    new ColumnExpressions(new[] {new ObjectIdExpression()}),
                    table,
                    whereExpression,
                    uiSortExpression);

                QueryOptions options = new QueryOptions
                {
                    Comparison = SqlComparison.DefaultCaseInsensitive
                };

                var repositoryObjects = repository.RunQsqlExpression(query, configuration.Id, options);

                if (repositoryObjects.Count <= 0)
                {
                    return new RepositoryObjectList();
                }

                table = templates == null || templates.Count() <= 0
                    ? (ITableSource)new AllTemplatesTable()
                    : new TemplateSetTable(templates);

                string[] attributes = Metamodel.GetGenericSearchLinks();

                whereExpression = CreateWhereExpressionForLinks(
                    attributes,
                    repositoryObjects.Select(item => item.Id.Id),
                    CreateOrOperationExpression,
                    uiFilterExpression);

                query = new SelectExpression(
                    new ColumnExpressions(new[] {new ObjectIdExpression()}),
                    table,
                    whereExpression,
                    uiSortExpression);

                return repository.RunQsqlExpression(query, configuration.Id, options);
            }

            private static Func<List<ILogicalExpression>, ILogicalExpression> GetCreateBinaryOperationExpression(
                SearchType searchType)
            {
                Func<List<ILogicalExpression>, ILogicalExpression> createBinaryOperationExpression = null;

                switch (searchType)
                {
                    case SearchType.And:
                        createBinaryOperationExpression = CreateAndOperationExpression;

                        break;

                    case SearchType.Or:
                        createBinaryOperationExpression = CreateOrOperationExpression;

                        break;
                }

                return createBinaryOperationExpression;
            }

            private static List<ILogicalExpression> MergeExpressions(
                List<ILogicalExpression> expressions,
                Func<ILogicalExpression, ILogicalExpression, ILogicalExpression> createBinaryOperationExpression)
            {
                if (expressions.Count <= 1)
                {
                    return expressions;
                }

                var firstOperand = expressions[0];
                var secondOperant = expressions[1];

                var result = new List<ILogicalExpression>(expressions.Skip(2))
                {
                    createBinaryOperationExpression(firstOperand, secondOperant)
                };

                return MergeExpressions(result, createBinaryOperationExpression);
            }

            #endregion

            #region Public Methods

            /// <summary>
            ///     Finds objects of the specified templates with the specified attributes. Equality of attributes are combined, using
            ///     AND operation.
            /// </summary>
            /// <param name="repository">
            ///     Repository, in which objects are searched.
            /// </param>
            /// <param name="configuration">
            ///     Configuration, in which objects are searched.
            /// </param>
            /// <param name="oids">
            ///     Object identifiers to filter. If at least one identifier is of specific revision then search by specific revisions
            ///     is used.
            /// </param>
            /// <param name="attributeNameToValueMap">
            ///     Map of attribute names to their values. The map is used to find matching objects. Single and multi link attributes
            ///     are evaluated as link display text.
            /// </param>
            /// <param name="searchType">
            ///     Type of search.
            /// </param>
            /// <param name="uiFilterExpression">
            ///     Filter expression applied in UI by user.
            /// </param>
            /// <param name="uiSortExpression">
            ///     Order expression applied in UI by user.
            /// </param>
            /// <param name="top">
            ///     How many top records to retrieve. 0 - means all and is value by default.
            /// </param>
            /// <param name="skip">
            ///     How many records to skip before retrieving. 0 - means none and is value by default.
            /// </param>
            /// <returns>
            ///     Objects of the specified templates with the specified attributes.
            /// </returns>
            public static RepositoryObjectList FilterObjects(
                IRepository repository,
                IConfiguration configuration,
                IEnumerable<Oid> oids,
                Dictionary<string, SearchValue> attributeNameToValueMap,
                SearchType searchType,
                IWhere uiFilterExpression = null,
                IOrder uiSortExpression = null,
                int top = 0,
                int skip = 0)
            {
                QueryOptions options = new QueryOptions
                {
                    Comparison = SqlComparison.DefaultCaseInsensitive
                };

                return FilterObjects(
                    repository,
                    configuration,
                    oids,
                    attributeNameToValueMap,
                    searchType,
                    options,
                    uiFilterExpression,
                    uiSortExpression,
                    top,
                    skip);
            }

            /// <summary>
            ///     Finds objects of the specified templates with the specified attributes. Equality of attributes are combined, using
            ///     AND operation.
            /// </summary>
            /// <param name="repository">
            ///     Repository, in which objects are searched.
            /// </param>
            /// <param name="configuration">
            ///     Configuration, in which objects are searched.
            /// </param>
            /// <param name="oids">
            ///     Object identifiers to filter. If at least one identifier is of specific revision then search by specific revisions
            ///     is used.
            /// </param>
            /// <param name="attributeNameToValueMap">
            ///     Map of attribute names to their values. The map is used to find matching objects. Single and multi link attributes
            ///     are evaluated as link display text.
            /// </param>
            /// <param name="searchType">
            ///     Type of search.
            /// </param>
            /// <param name="options">
            ///     QSQL query options.
            /// </param>
            /// <param name="uiFilterExpression">
            ///     Filter expression applied in UI by user.
            /// </param>
            /// <param name="uiSortExpression">
            ///     Order expression applied in UI by user.
            /// </param>
            /// <param name="top">
            ///     How many top records to retrieve. 0 - means all and is value by default.
            /// </param>
            /// <param name="skip">
            ///     How many records to skip before retrieving. 0 - means none and is value by default.
            /// </param>
            /// <returns>
            ///     Objects of the specified templates with the specified attributes.
            /// </returns>
            public static RepositoryObjectList FilterObjects(
                IRepository repository,
                IConfiguration configuration,
                IEnumerable<Oid> oids,
                Dictionary<string, SearchValue> attributeNameToValueMap,
                SearchType searchType,
                QueryOptions options,
                IWhere uiFilterExpression = null,
                IOrder uiSortExpression = null,
                int top = 0,
                int skip = 0)
            {
                #region Argument Check

                if (repository == null)
                {
                    throw new ArgumentNullException("repository");
                }

                if (configuration == null)
                {
                    throw new ArgumentNullException("configuration");
                }

                if (options == null)
                {
                    throw new ArgumentNullException("options");
                }

                #endregion

                return DoFilterObjects(
                    repository,
                    configuration,
                    oids,
                    attributeNameToValueMap,
                    searchType,
                    options,
                    uiFilterExpression,
                    uiSortExpression,
                    top,
                    skip);
            }

            /// <summary>
            ///     Finds objects of the specified templates with the specified attributes. Equality of attributes are combined, using
            ///     AND operation.
            /// </summary>
            /// <param name="repository">
            ///     Repository, in which objects are searched.
            /// </param>
            /// <param name="configuration">
            ///     Configuration, in which objects are searched.
            /// </param>
            /// <param name="templates">
            ///     Templates, objects of which are searched. If null or empty, all templates are searched.
            /// </param>
            /// <param name="uiFilterExpression">
            ///     Filter expression applied in UI by user.
            /// </param>
            /// <param name="uiSortExpression">
            ///     Order expression applied in UI by user.
            /// </param>
            /// <param name="top">
            ///     How many top records to retrieve. 0 - means all and is value by default.
            /// </param>
            /// <param name="skip">
            ///     How many records to skip before retrieving. 0 - means none and is value by default.
            /// </param>
            /// <returns>
            ///     Objects of the specified templates with the specified attributes.
            /// </returns>
            public static RepositoryObjectList FindObjects(
                IRepository repository,
                IConfiguration configuration,
                string[] templates,
                IWhere uiFilterExpression = null,
                IOrder uiSortExpression = null,
                int top = 0,
                int skip = 0)
            {
                QueryOptions options = new QueryOptions
                {
                    Comparison = SqlComparison.DefaultCaseInsensitive
                };

                return DoFindObjects(
                    repository,
                    configuration,
                    templates,
                    null,
                    SearchType.Or,
                    false,
                    options,
                    uiFilterExpression,
                    uiSortExpression,
                    top,
                    skip);
            }

            /// <summary>
            ///     Finds objects of the specified templates with the specified attributes. Equality of attributes are combined, using
            ///     AND operation.
            /// </summary>
            /// <param name="repository">
            ///     Repository, in which objects are searched.
            /// </param>
            /// <param name="configuration">
            ///     Configuration, in which objects are searched.
            /// </param>
            /// <param name="templates">
            ///     Templates, objects of which are searched. If null or empty, all templates are searched.
            /// </param>
            /// <param name="attributeNameToValueMap">
            ///     Map of attribute names to their values. The map is used to find matching objects. Single and multi link attributes
            ///     are evaluated as link display text.
            /// </param>
            /// <param name="searchType">
            ///     Type of search.
            /// </param>
            /// <param name="detectMetamodelFormat">
            ///     <b>true</b> to detect metamodel format; <b>false</b> otherwise. Use <b>true</b> to treat link attributes as plain
            ///     text.
            /// </param>
            /// <param name="uiFilterExpression">
            ///     Filter expression applied in UI by user.
            /// </param>
            /// <param name="uiSortExpression">
            ///     Order expression applied in UI by user.
            /// </param>
            /// <param name="top">
            ///     How many top records to retrieve. 0 - means all and is value by default.
            /// </param>
            /// <param name="skip">
            ///     How many records to skip before retrieving. 0 - means none and is value by default.
            /// </param>
            /// <returns>
            ///     Objects of the specified templates with the specified attributes.
            /// </returns>
            public static RepositoryObjectList FindObjects(
                IRepository repository,
                IConfiguration configuration,
                string[] templates,
                Dictionary<string, SearchValue> attributeNameToValueMap,
                SearchType searchType,
                bool detectMetamodelFormat,
                IWhere uiFilterExpression = null,
                IOrder uiSortExpression = null,
                int top = 0,
                int skip = 0)
            {
                QueryOptions options = new QueryOptions
                {
                    Comparison = SqlComparison.DefaultCaseInsensitive
                };

                return FindObjects(
                    repository,
                    configuration,
                    templates,
                    attributeNameToValueMap,
                    searchType,
                    detectMetamodelFormat,
                    options,
                    uiFilterExpression,
                    uiSortExpression,
                    top,
                    skip);
            }

            /// <summary>
            ///     Finds objects of the specified templates with the specified attributes. Equality of attributes are combined, using
            ///     AND operation.
            /// </summary>
            /// <param name="repository">
            ///     Repository, in which objects are searched.
            /// </param>
            /// <param name="configuration">
            ///     Configuration, in which objects are searched.
            /// </param>
            /// <param name="templates">
            ///     Templates, objects of which are searched. If null or empty, all templates are searched.
            /// </param>
            /// <param name="attributeNameToValueMap">
            ///     Map of attribute names to their values. The map is used to find matching objects. Single and multi link attributes
            ///     are evaluated as link display text.
            /// </param>
            /// <param name="searchType">
            ///     Type of search.
            /// </param>
            /// <param name="detectMetamodelFormat">
            ///     <b>true</b> to detect metamodel format; <b>false</b> otherwise. Use <b>true</b> to treat link attributes as plain
            ///     text.
            /// </param>
            /// <param name="options">
            ///     QSQL query options.
            /// </param>
            /// <param name="uiFilterExpression">
            ///     Filter expression applied in UI by user.
            /// </param>
            /// <param name="uiSortExpression">
            ///     Order expression applied in UI by user.
            /// </param>
            /// <param name="top">
            ///     How many top records to retrieve. 0 - means all and is value by default.
            /// </param>
            /// <param name="skip">
            ///     How many records to skip before retrieving. 0 - means none and is value by default.
            /// </param>
            /// <returns>
            ///     Objects of the specified templates with the specified attributes.
            /// </returns>
            public static RepositoryObjectList FindObjects(
                IRepository repository,
                IConfiguration configuration,
                string[] templates,
                Dictionary<string, SearchValue> attributeNameToValueMap,
                SearchType searchType,
                bool detectMetamodelFormat,
                QueryOptions options,
                IWhere uiFilterExpression = null,
                IOrder uiSortExpression = null,
                int top = 0,
                int skip = 0)
            {
                #region Argument Check

                if (repository == null)
                {
                    throw new ArgumentNullException("templates");
                }

                if (configuration == null)
                {
                    throw new ArgumentNullException("configuration");
                }

                if (options == null)
                {
                    throw new ArgumentNullException("options");
                }

                #endregion

                return DoFindObjects(
                    repository,
                    configuration,
                    templates,
                    attributeNameToValueMap,
                    searchType,
                    detectMetamodelFormat,
                    options,
                    uiFilterExpression,
                    uiSortExpression,
                    top,
                    skip);
            }

            public static RepositoryObjectList SearchObjectsAdvanced(
                IRepository repository,
                IConfiguration configuration,
                string[] templates,
                IList<Tuple<string, string>> attributeNameToValueMap,
                RelationType[] relationTypes,
                string searchBy,
                bool detectMetamodelFormat,
                IWhere uiFilterExpression = null,
                IOrder uiSortExpression = null,
                int top = 0,
                int skip = 0)
            {
                #region Argument Check

                if (repository == null)
                {
                    throw new ArgumentNullException("repository");
                }

                if (configuration == null)
                {
                    throw new ArgumentNullException("configuration");
                }

                #endregion

                RepositoryObjectList result = new RepositoryObjectList();

                ITableSource table = templates == null || templates.Count() <= 0
                    ? (ITableSource)new AllTemplatesTable()
                    : new TemplateSetTable(templates);

                IWhere whereExpression = CreateWhereExpression(
                    repository.Metamodel,
                    templates,
                    ConvertAttributeMap(attributeNameToValueMap, SearchType.And),
                    SearchType.And,
                    detectMetamodelFormat,
                    uiFilterExpression);

                SelectExpression query = new SelectExpression(
                    new ColumnExpressions(new[] {new ObjectIdExpression()}),
                    table,
                    whereExpression,
                    uiSortExpression,
                    top,
                    skip);

                QueryOptions options = new QueryOptions
                {
                    Comparison = SqlComparison.DefaultCaseInsensitive
                };

                result.AddRange(repository.RunQsqlExpression(query, configuration.Id, options));

                if (relationTypes != null)
                {
                    foreach (var relationType in relationTypes)
                    {
                        switch (relationType)
                        {
                            case RelationType.Links:
                            case RelationType.Contains:

                                result.AddRange(
                                    FindObjectsByLinks(
                                        repository,
                                        configuration,
                                        templates,
                                        searchBy,
                                        SearchType.And,
                                        detectMetamodelFormat,
                                        uiFilterExpression,
                                        uiSortExpression));

                                break;

                            case RelationType.LinkedBy:
                            case RelationType.ContainedIn:

                                result.AddRange(
                                    FindObjectsByLinkedBy(
                                        repository,
                                        configuration,
                                        templates,
                                        searchBy,
                                        SearchType.And,
                                        detectMetamodelFormat,
                                        uiFilterExpression,
                                        uiSortExpression));

                                break;
                        }
                    }
                }

                return result;
            }

            public static RepositoryObjectList SearchObjectsSimple(
                IRepository repository,
                IConfiguration configuration,
                string[] templates,
                string[] attributes,
                RelationType[] relationTypes,
                string searchBy,
                bool detectMetamodelFormat,
                IWhere uiFilterExpression = null,
                IOrder uiSortExpression = null)
            {
                #region Argument Check

                if (repository == null)
                {
                    throw new ArgumentNullException("templates");
                }

                if (configuration == null)
                {
                    throw new ArgumentNullException("configuration");
                }

                #endregion

                IList<Tuple<string, string>> attributeNameToValueMap = attributes
                    .Select(item => new Tuple<string, string>(item, searchBy))
                    .ToList();

                RepositoryObjectList result = new RepositoryObjectList();

                if (relationTypes != null)
                {
                    foreach (var relationType in relationTypes)
                    {
                        switch (relationType)
                        {
                            case RelationType.Links:
                            case RelationType.Contains:

                                ITableSource table = templates == null || templates.Count() <= 0
                                    ? (ITableSource)new AllTemplatesTable()
                                    : new TemplateSetTable(templates);

                                IWhere whereExpression = CreateWhereExpression(
                                    repository.Metamodel,
                                    templates,
                                    ConvertAttributeMap(attributeNameToValueMap, SearchType.Or),
                                    SearchType.Or,
                                    detectMetamodelFormat,
                                    uiFilterExpression);

                                SelectExpression query = new SelectExpression(
                                    new ColumnExpressions(new[] {new ObjectIdExpression()}),
                                    table,
                                    whereExpression,
                                    uiSortExpression);

                                QueryOptions options = new QueryOptions
                                {
                                    Comparison = SqlComparison.DefaultCaseInsensitive
                                };

                                result.AddRange(repository.RunQsqlExpression(query, configuration.Id, options));

                                break;

                            case RelationType.LinkedBy:
                            case RelationType.ContainedIn:

                                result.AddRange(
                                    FindObjectsByLinkedBy(
                                        repository,
                                        configuration,
                                        templates,
                                        searchBy,
                                        SearchType.Or,
                                        detectMetamodelFormat,
                                        uiFilterExpression,
                                        uiSortExpression));

                                break;
                        }
                    }
                }

                return result;
            }

            #endregion
        }

        #endregion
    }
}
