﻿using System;
using System.Collections.Generic;
using System.Linq;
using QCommon.Extensions;
using Qdc.Common;
using Qef.Common;
using Qef.Common.Modules;
using Qef.Common.Security;
using Qis.Common;
using Qis.Common.AttributeValues;
using Qis.Web.Forms.Common;
using Qis.Web.Forms.Common.ObjectDialog.ControlStates;
using IRepository = Qis.Common.IRepository;

namespace Qis.Module.Scripts
{
    public static partial class ScriptExtensions
    {
        #region Public Methods

        public static bool CheckConnected(this IExternalDocument externalDocument, out string message)
        {
            message = null;

            if (externalDocument == null
                || !externalDocument.IsConnectionInitialized
                || !externalDocument.IsLoggedIn)
            {
                message = "Either not connected to the SharePoint server or is not logged in.";

                return false;
            }

            return true;
        }

        public static void Clear(this IWebControlState controlState)
        {
            controlState.SetControlValue(new PlainText());
        }

        public static void Clear(this IComboBoxState comboBox)
        {
            comboBox.Items.Clear();
            comboBox.SetControlValue(new PlainText());
        }

        public static bool GetChecked(this IWebControlState controlState)
        {
            return controlState.GetControlValue().AsPlainText().ToBoolean();
        }

        public static ExternalDocumentLink GetExternalDocumentLink(this IAttributeValue attributeValue)
        {
            string attributeValueAsPlainText = attributeValue.AsPlainText(null);

            ExternalDocumentLink link;

            if (ExternalDocumentLink.TryParse(attributeValueAsPlainText, out link))
            {
                return link;
            }

            return null;
        }

        public static string GetExternalDocumentPath(this IAttributeValue attributeValue)
        {
            ExternalDocumentLink link = attributeValue.GetExternalDocumentLink();

            if (link == null)
            {
                return null;
            }

            return link.Url;
        }

        public static IRepository GetRepository(this IQisServer qis, RepId repId)
        {
            var rep = qis.FindRepository(repId);
            if (rep == null)
            {
                throw new ArgumentException(string.Format("Repository with id \"{0}\" does not exists.", repId));
            }

            return rep;
        }

        public static string GetSelectedText(this IComboBoxState comboBox)
        {
            return comboBox.GetControlValue().AsPlainText();
        }

        public static IDictionary<string, ServerInfo> GetSharePointServerInfos(
            this IRuntimeDiscoveryService runtimeDiscoveryService,
            SecureToken secureToken)
        {
            IQdcServer qdcModule = null;

            try
            {
                qdcModule = QdcServerLocator.GetServer(runtimeDiscoveryService, secureToken);
            }
            catch (QModuleNotFoundException)
            {
                // Nothing to do
            }
            catch (QNotAuthorizedException)
            {
                // Nothing to do -- missing license
            }

            if (qdcModule == null)
            {
                return new Dictionary<string, ServerInfo>();
            }

            return qdcModule.Settings.Servers.ToDictionary(item => item.Name, item => item);
        }

        #endregion
    }
}
