﻿using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using QCommon.Extensions;
using Qef.Common.Log;
using Qef.Common.Users;
using Qis.Common;
using Qis.Common.AttributeValues;
using Qis.Common.Metamodel;
using Qis.Common.Metamodel.DialogDescriptions;
using Qis.Common.Scripting.Events.EventHandlerArguments;
using Qis.Common.Scripting.Events.EventHandlerAttributes;

namespace Qis.Module.Scripts
{
    public class QisScriptCallbackMockUp : MarshalByRefObject, IQisScriptCallback
    {
        #region Fields

        private readonly List<string> m_messages;

        #endregion

        #region Constructors and Destructors

        public QisScriptCallbackMockUp()
        {
            m_messages = new List<string>();
        }

        #endregion

        #region Public Properties

        public List<string> Messages
        {
            get
            {
                return m_messages;
            }
        }

        #endregion

        #region Public Methods

        public override object InitializeLifetimeService()
        {
            return null;
        }

        #endregion

        #region IQisScriptCallback Members

        public void LogMessage(string msg)
        {
            m_messages.Add(msg);
        }

        #endregion
    }

    public static class QualiWareSetup
    {
        #region Private Methods

        private static void ExecuteSetupsAdHoc(ConfigurationConnectedEventArgs args, RepositoryObjectList setups)
        {
            StringCollection output;
            CompilerErrorCollection errors;

            var program = string.Format(
                @"
                using System;
                using System.Collections.Generic;
                using System.Linq;
                using System.Text;
                using Qef.Common;
                using Qef.Common.Log;
                using Qis.Common;
                using Qis.Common.Metamodel;
                using Qis.Common.Metamodel.DialogDescriptions;
                using Qis.Common.Scripting;
                using Qis.Module.Scripts;

                namespace AdHocScript
                {{
                    public partial class Program
                    {{

                        private IRepository m_repository;
                        public IRepository Repository
                        {{
                            get {{ return m_repository; }}
                        }}

                        public IMetamodel Metamodel
                        {{
                            get {{ return m_repository.Metamodel; }}
                        }}

                        public int Run()
                        {{
                            try
                            {{
                                var repository = CallContext.QisServer.FindRepository(RepId.Parse(""{0}""));
                                if (repository == null)
                                {{
                                    return 0;
                                }}

                                m_repository = repository;

                                {1};
                            
                                return 0;
                            }}
                            catch(Exception ex)
                            {{
                                CallContext
                                    .QisServer
                                    .GetQefService<ILogService>()
                                    .GetLogWriter(QisConst.QisId, Source.Parse(""QualiWareSetup""))
                                    .Debug(ex.ToString());
                                throw ex;
                                    
                            }}
                        }}
                    }}
                }}
                ",
                args.RepositoryId,
                setups.Select(s => s.Attributes[Metamodel.Attributes.SetupAction].Value.AsPlainText()).Join(";"));

            var logWriter = args.Qis.GetQefService<ILogService>()
                .GetLogWriter(QisConst.QisId, Source.Parse("QualiWareSetup"));
            logWriter.Info(program);

            if (!args
                .Repository
                .CSEngine
                .Execute(
                    program.AsArray(),
                    new QisScriptCallbackMockUp(),
                    out output,
                    out errors))
            {
                logWriter.Debug("Unknown error. Seems script has not been executed successfully.");
            }

            if (errors == null)
            {
                return;
            }

            string errs = string.Empty;
            foreach (CompilerError error in errors)
            {
                if (!error.IsWarning)
                {
                    errs += string.Format(
                        "Error: {0} at {1}, line {2}\r\n",
                        error.ErrorText,
                        error.FileName,
                        error.Line);
                }
            }
            foreach (CompilerError error in errors)
            {
                if (error.IsWarning)
                {
                    errs += string.Format(
                        "Warning: {0} at {1}, line {2}\r\n",
                        error.ErrorText,
                        error.FileName,
                        error.Line);
                }
            }

            if (!string.IsNullOrEmpty(errs))
            {
                logWriter.Debug(errs);
            }
        }

        private static RepositoryObjectList FindSetupsToExecute(ConfigurationConnectedEventArgs args)
        {
            var setups = args.Configuration.FindObjects(Metamodel.Templates.QualiWareSetup);
            setups.InstantiationLevel = ObjectInstantiationLevel.FullData;

            var userStorage = args.Qis.GetQefService<IUserStorageService>();

            var setupsToExecute = new RepositoryObjectList();
            foreach (var setup in setups)
            {
                if (!setup.Attributes.Contains(Metamodel.Attributes.ServerSide)
                    || setup.Attributes[Metamodel.Attributes.ServerSide].Value.IsEmpty
                    || setup.Attributes[Metamodel.Attributes.ServerSide].Value.AsPlainText() != "1"
                    || !setup.Attributes.Contains(Metamodel.Attributes.SetupAction)
                    || setup.Attributes[Metamodel.Attributes.SetupAction].Value.IsEmpty)
                {
                    continue;
                }

                IAttributeValue userAttrVal, groupAttrVal, roleAttrVal;
                var user =
                    setup.Attributes.TryGetValue(Metamodel.Attributes.UserName, out userAttrVal)
                        ? userAttrVal.AsPlainText()
                        : string.Empty;
                var group =
                    setup.Attributes.TryGetValue(Metamodel.Attributes.Group, out groupAttrVal)
                        ? groupAttrVal.AsPlainText()
                        : string.Empty;
                var role =
                    setup.Attributes.TryGetValue(Metamodel.Attributes.Role, out roleAttrVal)
                        ? roleAttrVal.AsPlainText()
                        : string.Empty;

                var anyUser = user.IsBlank() || user.Trim() == "*";
                var anyGroup = @group.IsBlank() || @group.Trim() == "*";
                var anyRole = role.IsBlank() || role.Trim() == "*";

                if (!anyUser && userStorage.FindUsers(user).IndexOf(args.UserId) < 0)
                {
                    continue;
                }

                if (!anyGroup)
                {
                    if (!userStorage
                        .GetAllRoleHolderGroupIds(args.UserId)
                        .Intersect(userStorage.FindGroups(@group).Select(g => g.Id))
                        .Any())
                    {
                        continue;
                    }
                }

                if (!anyRole && role != args.RoleId.ToString())
                {
                    continue;
                }

                setupsToExecute.Add(setup);
            }

            return setupsToExecute;
        }

        #endregion

        #region Public Methods

        /// <summary>
        ///     Extends target template with parent's attributes.
        ///     This also affects default dialog of the template.
        /// </summary>
        /// <param name="metamodel">
        ///     Instance of <see cref="IMetamodel" /> to make changes through.
        /// </param>
        /// <param name="templateName">
        ///     Name of template to extend.
        /// </param>
        /// <param name="parentName">
        ///     Name of parent template to take attributes from.
        /// </param>
        [Obsolete("Use DynamicMetamodel.AddTemplateParent method instead.")]
        public static void AddTemplateParent(
            IMetamodel metamodel,
            string templateName,
            string parentName,
            IEnumerable<TabDisplayName> displayNames = null)
        {
            DynamicMetamodel.AddTemplateParent(metamodel, templateName, parentName, displayNames);
        }

        [ConfigurationConnected]
        public static void ExecuteQualiWareSetups(ConfigurationConnectedEventArgs args)
        {
            var setupsToExecute = FindSetupsToExecute(args);
            ExecuteSetupsAdHoc(args, setupsToExecute);
        }

        /// <summary>
        ///     Gets all kind of layouts in target dialog.
        /// </summary>
        /// <param name="metamodel">
        ///     Instance of <see cref="IMetamodel" /> to make changes through.
        /// </param>
        /// <param name="dialogName">
        ///     Full name of the dialog including role and/or customization (e.g. role/customization/name.xdlg).
        /// </param>
        /// <returns>
        ///     List of dialog layouts; otherwise empty list.
        /// </returns>
        [Obsolete("Use DynamicMetamodel.GetDialogLayouts method instead.")]
        public static IEnumerable<LayoutTemplate> GetDialogLayouts(
            IMetamodel metamodel,
            string dialogName)
        {
            return DynamicMetamodel.GetDialogLayouts(metamodel, dialogName);
        }

        /// <summary>
        ///     Modifies (if found) or creates new attribute definition in target template.
        /// </summary>
        /// <param name="metamodel">
        ///     Instance of <see cref="IMetamodel" /> to make changes through.
        /// </param>
        /// <param name="templateName">
        ///     Name of template to change.
        /// </param>
        /// <param name="attribute">
        ///     New definition of the attribute.
        /// </param>
        [Obsolete("Use DynamicMetamodel.ModifyAttribute method instead.")]
        public static void ModifyAttribute(
            IMetamodel metamodel,
            string templateName,
            AttributeDefinitionBase attribute)
        {
            DynamicMetamodel.ModifyAttribute(metamodel, templateName, attribute);
        }

        /// <summary>
        ///     Modifies (if found) or creates new dialog definition.
        /// </summary>
        /// <param name="metamodel">
        ///     Instance of <see cref="IMetamodel" /> to make changes through.
        /// </param>
        /// <param name="dialogName">
        ///     Full name of the dialog including role and/or customization (e.g. role/customization/name.xdlg).
        /// </param>
        /// <param name="parentNames">
        ///     List of parent dialogs to inherit from (including role and/or customization).
        /// </param>
        /// <param name="displayNames">
        ///     List of <see cref="TabDisplayName" />. Used to rename tabs (usually inherited) in dialog. May be null.
        /// </param>
        /// <param name="tabs">
        ///     List of explicit tab definitions including layout (may be extended by inheritance).
        /// </param>
        /// <param name="layouts">
        ///     List of dialog layouts.
        /// </param>
        [Obsolete("Use DynamicMetamodel.ModifyDialog method instead.")]
        public static void ModifyDialog(
            IMetamodel metamodel,
            string dialogName,
            IEnumerable<string> parentNames,
            IEnumerable<TabDisplayName> displayNames,
            IEnumerable<DialogTab> tabs,
            IEnumerable<LayoutTemplate> layouts)
        {
            DynamicMetamodel.ModifyDialog(metamodel, dialogName, parentNames, displayNames, tabs, layouts);
        }

        /// <summary>
        ///     Modifies tab definition in target dialog.
        /// </summary>
        /// <param name="metamodel">
        ///     Instance of <see cref="IMetamodel" /> to make changes through.
        /// </param>
        /// <param name="dialogName">
        ///     Full name of the dialog including role and/or customization (e.g. role/customization/name.xdlg).
        /// </param>
        /// <param name="tab">
        ///     New definition of tab including all kinds of layout.
        /// </param>
        [Obsolete("Use DynamicMetamodel.ModifyTab method instead.")]
        public static void ModifyTab(
            IMetamodel metamodel,
            string dialogName,
            DialogTab tab)
        {
            DynamicMetamodel.ModifyTab(metamodel, dialogName, tab);
        }

        /// <summary>
        ///     Modifies (if found) or creates new template definition.
        /// </summary>
        /// <param name="metamodel">
        ///     Instance of <see cref="IMetamodel" /> to make changes through.
        /// </param>
        /// <param name="templateName">
        ///     Name of template to modify or create.
        /// </param>
        /// <param name="parentNames">
        ///     List of parent templates to inherit from.
        /// </param>
        /// <param name="hidden">
        ///     <b>true</b> to hide template in UI; otherwise <b>false</b>
        /// </param>
        /// <param name="attributes">
        ///     List of explicit attributes (may be extended by inheritance).
        /// </param>
        [Obsolete("Use DynamicMetamodel.ModifyTemplate method instead.")]
        public static void ModifyTemplate(
            IMetamodel metamodel,
            string templateName,
            IEnumerable<string> parentNames,
            bool hidden,
            IEnumerable<AttributeDefinitionBase> attributes)
        {
            DynamicMetamodel.ModifyTemplate(metamodel, templateName, parentNames, hidden, attributes);
        }

        /// <summary>
        ///     Removes tabs in template's default dialog which were introduced by parent template default dialog.
        ///     This doesn't affect attribute definitions in template, but only default dialog of the template.
        /// </summary>
        /// <param name="metamodel">
        ///     Instance of <see cref="IMetamodel" /> to make changes through.
        /// </param>
        /// <param name="templateName">
        ///     Name of template to process.
        /// </param>
        /// <param name="parentName">
        ///     Name of parent template to remove.
        /// </param>
        [Obsolete("Use DynamicMetamodel.RemoveTemplateParent method instead.")]
        public static void RemoveTemplateParent(
            IMetamodel metamodel,
            string templateName,
            string parentName)
        {
            DynamicMetamodel.RemoveTemplateParent(metamodel, templateName, parentName);
        }

        /// <summary>
        ///     Reorders tabs in <paramref name="dialogName" /> according to given order in <paramref name="tabOrder" />.
        /// </summary>
        /// <param name="metamodel">
        ///     Instance of <see cref="IMetamodel" /> to make changes through.
        /// </param>
        /// <param name="dialogName">
        ///     Full name of the dialog including role and/or customization (e.g. role/customization/name.xdlg).
        /// </param>
        /// <param name="tabOrder">
        ///     Desired order of tabs. Can be list of tab names or display names.
        /// </param>
        [Obsolete("Use DynamicMetamodel.ReorderTabs method instead.")]
        public static void ReorderTabs(
            IMetamodel metamodel,
            string dialogName,
            IEnumerable<string> tabOrder)
        {
            DynamicMetamodel.ReorderTabs(metamodel, dialogName, tabOrder);
        }

        /// <summary>
        ///     Sets visibility of target template.
        /// </summary>
        /// <param name="metamodel">
        ///     Instance of <see cref="IMetamodel" /> to make changes through.
        /// </param>
        /// <param name="templateName">
        ///     Name of template to change visibility for.
        /// </param>
        /// <param name="hidden">
        ///     <b>true</b> to hide template in UI; otherwise <b>false</b>
        /// </param>
        [Obsolete("Use DynamicMetamodel.SetTemplateVisibility method instead.")]
        public static void SetTemplateVisibility(
            IMetamodel metamodel,
            string templateName,
            bool hidden)
        {
            DynamicMetamodel.SetTemplateVisibility(metamodel, templateName, hidden);
        }

        #endregion
    }
}
