﻿using System;
using System.Collections.Generic;
using System.Linq;
using QCommon.Extensions;
using Qdc.Common;
using Qef.Common;
using Qef.Common.Users;
using Qef.Web.Common;
using Qis.Common;
using Qis.Common.AttributeValues;
using Qis.Web.Forms.Common;
using Qis.Web.Forms.Common.LinkConverters;
using Qis.Web.Forms.Common.Scripting;
using IRepository = Qis.Common.IRepository;
using IUser = Qef.Common.Users.IUser;

namespace Qis.Module.Scripts
{
    /// <summary>
    ///     Provides common script methods.
    /// </summary>
    public static class Common
    {
        #region Nested Types

        public delegate void ObjectChangedDelegate(
            IRepository repository,
            IConfiguration configuration,
            RepositoryObject obj,
            string path,
            IDictionary<string, ServerInfo> sharePointServerInfos);

        #endregion

        #region Fields

        public static readonly string[] FalseStrings = new[] {0.ToString(), Boolean.FalseString};
        public static readonly string[] TrueStrings = new[] {1.ToString(), Boolean.TrueString};

        #endregion

        #region Private Methods

        private static string ConvertValueToString(IAttributeValue value)
        {
            if (value == null)
            {
                return String.Empty;
            }

            IText text = value as IText;
            return text == null ? value.GetContent() : text.Text;
        }

        #endregion

        #region Public Methods

        /// <summary>
        ///     Converts object attribute to XHTML format.
        /// </summary>
        /// <param name="repositoryObject">
        ///     Repository object, attribute of which is to convert.
        /// </param>
        /// <param name="attributeName">
        ///     Name of attribute, value of which is to convert to XHTML format.
        /// </param>
        /// <param name="baseUrl">
        ///     Base URL for converted hyperlinks.
        /// </param>
        /// <param name="linkTarget">
        ///     Target for converted hyperlinks.
        /// </param>
        /// <param name="webContext">
        ///     Web-specific context.
        /// </param>
        /// <param name="linkConverterContext">
        ///     Link converter context.
        /// </param>
        /// <returns>
        ///     Converted object attribute to XHTML format.
        /// </returns>
        public static string ConvertQis2Html(
            RepositoryObject repositoryObject,
            string attributeName,
            string baseUrl,
            string linkTarget,
            WebContext webContext,
            QisLinkConverterContext linkConverterContext)
        {
            #region Argument Check

            if (repositoryObject == null)
            {
                throw new ArgumentNullException("repositoryObject");
            }

            if (webContext == null)
            {
                throw new ArgumentNullException("webContext");
            }

            if (attributeName.IsBlank())
            {
                throw new ArgumentException("attributeName");
            }

            #endregion

            string value = QisHtmlTextualLinkConverter.ConvertQis2Html(
                ConvertValueToString(repositoryObject.Attributes[attributeName].Value),
                linkConverterContext,
                baseUrl,
                linkTarget,
                webContext);

            value.Replace(Environment.NewLine, String.Empty).ToJSString();

            return value;
        }

        /// <summary>
        ///     Converts object attribute to XHTML format.
        /// </summary>
        /// <param name="repositoryObject">
        ///     Repository object, attribute of which is to convert.
        /// </param>
        /// <param name="attributeName">
        ///     Name of attribute, value of which is to convert to XHTML format.
        /// </param>
        /// <param name="webContext">
        ///     Web-specific context.
        /// </param>
        /// <param name="linkConverterContext">
        ///     Link converter context.
        /// </param>
        /// <returns>
        ///     Converted object attribute to XHTML format.
        /// </returns>
        public static string ConvertQis2Html(
            RepositoryObject repositoryObject,
            string attributeName,
            WebContext webContext,
            QisLinkConverterContext linkConverterContext)
        {
            return ConvertQis2Html(repositoryObject, attributeName, null, null, webContext, linkConverterContext);
        }

        /// <summary>
        ///     Provides generic implementation of filling C# script columns.
        ///     Main idea is in execution all scripts at once per one remoting/WCF call.
        /// </summary>
        /// <remarks>
        ///     Do not delete, rename or move to another namespace the method, because it is a predefined script and
        ///     is used to fill C# script columns in RE, QRV, MultiLink.
        /// </remarks>
        /// <param name="args">
        ///     Arguments provided by list view control (RE/QRV/MultiLink) for filling its cells with values.
        /// </param>
        public static void FillScriptedColumns(FillListViewPageEventArgs args)
        {
            //var totalStopwatch = Stopwatch.StartNew();
            for (int idx = 0; idx < args.PageObjects.Count; idx++)
            {
                foreach (var column in args.Columns.Where(c => c.CustomScript != null))
                {
                    //var cellStopwatch = Stopwatch.StartNew();
                    args.SetCellValue(idx, column.Name, args.GetAttributeValueFromScript(args.PageObjects[idx], column));
                    //args.SetCellValue(idx, column.Name, new PlainText(string.Format("ListView batch fill: {0}ms", cellStopwatch.ElapsedMilliseconds)));
                }
            }

            //var logService = (ILogService)args.QisServer.GetQefService(ServiceId.Log);
            //var logWriter = logService.GetLogWriter(WebConst.WebFormsId, Source.Parse("PERFORMANCE (ListView)"));
            //logWriter.Info(string.Format("ListView batch fill: {0}ms", totalStopwatch.ElapsedMilliseconds));
        }

        /// <summary>
        ///     Finds user full name.
        /// </summary>
        /// <param name="qisServer">
        ///     QEF service to retreive data about users and groups.
        /// </param>
        /// <param name="userId">
        ///     Identifier of user to search.
        /// </param>
        /// <returns>
        ///     User full name.
        /// </returns>
        public static string FindUserFullName(IQisServer qisServer, Guid userId)
        {
            IUserStorageService userStorageService = (IUserStorageService)qisServer.GetQefService(ServiceId.UserStorage);

            IUser user = userStorageService.FindUser(userId);

            return user == null ? String.Empty : user.FullName;
        }

        /// <summary>
        ///     Gets a message about object lock status. The format of the message:
        ///     <paramref name="actionMessage" />: message about object lock status.
        /// </summary>
        /// <param name="actionMessage">
        ///     Message about action taken.
        /// </param>
        /// <param name="objectName">
        ///     Object name.
        /// </param>
        /// <param name="resultType">
        ///     Object lock status.
        /// </param>
        /// <returns>
        ///     Message about object lock status.
        /// </returns>
        public static string GetLockResultMessage(
            string actionMessage,
            string objectName,
            ObjectLockResultType resultType)
        {
            string statusMessage = String.Empty;

            switch (resultType)
            {
                case ObjectLockResultType.Unlocked:
                    // No message.

                    break;
                case ObjectLockResultType.NotLockOwner:
                    statusMessage = Messages.CheckedOutByOtherUserMessage;

                    break;
                case ObjectLockResultType.AlreadyLocked:
                    statusMessage = Messages.InUseByOtherUserMessage;

                    break;
                case ObjectLockResultType.AlreadyReserved:
                    statusMessage = Messages.CheckedOutByOtherUserMessage;

                    break;
                case ObjectLockResultType.NotLocked:
                    statusMessage = Messages.NotCheckedOutMessage;

                    break;
                default:
                    statusMessage = Messages.ReasonUnknownMessage;

                    break;
            }

            statusMessage = String.Format("{0} \"{1}\": {2}.", actionMessage, objectName, statusMessage);

            return statusMessage;
        }

        /// <summary>
        ///     Gets a message about object reservation status. The format of the message:
        ///     <paramref name="actionMessage" />: message about object reservation status.
        /// </summary>
        /// <param name="actionMessage">
        ///     Message about action taken.
        /// </param>
        /// <param name="objectName">
        ///     Object name.
        /// </param>
        /// <param name="resultType">
        ///     Object reservation status.
        /// </param>
        /// <returns>
        ///     Message about object reservation status.
        /// </returns>
        public static string GetReserveResultMessage(
            string actionMessage,
            string objectName,
            ObjectReserveResultType resultType)
        {
            string statusMessage = String.Empty;

            switch (resultType)
            {
                case ObjectReserveResultType.ReservationRemoved:
                    // No message.

                    break;
                case ObjectReserveResultType.NotReservationOwner:
                    statusMessage = Messages.CheckedOutByOtherUserMessage;

                    break;
                case ObjectReserveResultType.ReservationDuplication:
                    statusMessage = Messages.AlreadyCheckedOutMessage;

                    break;
                case ObjectReserveResultType.AlreadyLocked:
                    statusMessage = Messages.InUseByOtherUserMessage;

                    break;
                case ObjectReserveResultType.AlreadyReserved:
                    statusMessage = Messages.CheckedOutByOtherUserMessage;

                    break;
                case ObjectReserveResultType.NotReserved:
                    statusMessage = Messages.NotCheckedOutMessage;

                    break;
                default:
                    statusMessage = Messages.ReasonUnknownMessage;

                    break;
            }

            statusMessage = String.Format("{0} \"{1}\": {2}.", actionMessage, objectName, statusMessage);

            return statusMessage;
        }

        #endregion
    }
}
