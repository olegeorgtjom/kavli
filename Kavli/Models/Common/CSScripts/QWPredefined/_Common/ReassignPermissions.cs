﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;
using QCommon.Extensions;
using QCommon.Log;
using Qef.Common;
using Qef.Common.Database;
using Qef.Common.Licensing;
using Qef.Common.Log;
using Qef.Common.Security;
using Qef.Common.Users;
using Qis.Common;
using Qis.Common.Scripting;
using Qis.Common.Security;
using Qis.Web.Forms.Common.Scripting;

namespace Qis.Module.Scripts
{
    public class ReassignPermissions
    {
        #region Nested Types

        private class DoubleLogWriter : ILogWriter
        {
            #region Fields

            private readonly string m_logPath;
            private readonly ILogWriter m_logWriter;

            #endregion

            #region Constructors and Destructors

            public DoubleLogWriter(ILogWriter logWriter, string logPath)
            {
                #region Argument check

                if (logWriter == null)
                {
                    throw new ArgumentNullException("logWriter");
                }

                if (logPath.IsBlank())
                {
                    throw new ArgumentException("Path to log file can't be empty.", "logPath");
                }

                #endregion

                m_logWriter = logWriter;
                m_logPath = logPath;
                try
                {
                    File.WriteAllText(m_logPath, "Logging started: " + DateTime.Now.ToString("u"));
                }
                catch (Exception ex)
                {
                    throw new InvalidOperationException(
                        string.Format("Log file cannot be written ({0})", m_logPath),
                        ex);
                }
            }

            #endregion

            #region Private Methods

            private void AppendLog(LogLevel level, string message)
            {
                try
                {
                    File.AppendAllText(
                        m_logPath,
                        string.Format("{0,20}{1,10}  {2}\n", DateTime.Now.ToString("u"), level, message),
                        Encoding.UTF8);
                }
                catch (Exception)
                {
                    //suppress all exceptions - attempt to write message in QEF log should not be skipped
                }
            }

            #endregion

            #region ILogWriter Members

            public bool IsEnabledFor(LogLevel level)
            {
                return m_logWriter.IsEnabledFor(level);
            }

            public void Log(LogLevel level, string message)
            {
                AppendLog(level, message);
                m_logWriter.Log(level, message);
            }

            public void Log(LogLevel level, string message, Exception exception)
            {
                AppendLog(level, message + " Exception: " + exception.Message);
                m_logWriter.Log(level, message, exception);
            }

            public void Verbose(string message)
            {
                AppendLog(LogLevel.Verbose, message);
                m_logWriter.Verbose(message);
            }

            public void Debug(string message)
            {
                AppendLog(LogLevel.Debug, message);
                m_logWriter.Debug(message);
            }

            public void Info(string message)
            {
                AppendLog(LogLevel.Info, message);
                m_logWriter.Info(message);
            }

            public void Warning(string message)
            {
                AppendLog(LogLevel.Warning, message);
                m_logWriter.Warning(message);
            }

            public void Error(string message)
            {
                AppendLog(LogLevel.Error, message);
                m_logWriter.Error(message);
            }

            public void Fatal(string message)
            {
                AppendLog(LogLevel.Fatal, message);
                m_logWriter.Fatal(message);
            }

            public LogLevel LogLevel
            {
                get
                {
                    return m_logWriter.LogLevel;
                }
                set
                {
                    m_logWriter.LogLevel = value;
                }
            }

            #endregion
        }

        /// <summary>
        ///     Contains QEF assignments of AD users and groups:<br />
        ///     - internal and external identifiers;<br />
        ///     - role assignments for AD users and groups;<br />
        ///     - local QEF groups membership of AD users and groups;<br />
        ///     - assignments of licenses.
        /// </summary>
        [Serializable]
        public sealed class QefAssignments : IXmlSerializable
        {
            #region Constants

            private const string c_externalGroupIdAttributeName = "ExternalGroupId";
            private const string c_externalUserIdAttributeName = "ExternalUserId";
            private const string c_groupGroupsMembership = "GroupGroupsMembership";
            private const string c_groupIdAttributeName = "GroupId";
            private const string c_groupIdsMapAttributeName = "GroupIdsMap";
            private const string c_groupsAttributeName = "Groups";
            private const string c_groupUsersMembership = "GroupUsersMembership";
            private const string c_internalGroupIdAttributeName = "InternalGroupId";
            private const string c_internalUserIdAttributeName = "InternalUserId";
            private const string c_licenseAttributeName = "License";
            private const string c_licensesMapAttributeName = "LicensesMap";
            private const string c_mapItemAttributeName = "MapItem";
            private const string c_roleGroupsMapAttributeName = "RoleGroupsMap";
            private const string c_roleIdAttributeName = "RoleId";
            private const string c_roleUsersMapAttributeName = "RoleUsersMap";
            private const string c_userIdsMapAttributeName = "UserIdsMap";
            private const string c_usersAttributeName = "Users";

            #endregion

            #region Constructors and Destructors

            /// <summary>
            ///     Initializes a new empty instance of the <see cref="QefAssignments" /> class.
            /// </summary>
            private QefAssignments()
            {
                UserIds = new Dictionary<Guid, string>();
                GroupIds = new Dictionary<Guid, string>();
                RoleUsers = new Dictionary<Rid, List<Guid>>();
                RoleGroups = new Dictionary<Rid, List<Guid>>();
                GroupUsersMembership = new Dictionary<Guid, List<Guid>>();
                GroupGroupsMembership = new Dictionary<Guid, List<Guid>>();
                Licenses = new Dictionary<SerialNumber, List<Guid>>();
            }

            /// <summary>
            ///     Initializes a new instance of the <see cref="QefAssignments" /> class.
            /// </summary>
            /// <param name="userIds">
            ///     Internal identifier to external identifier of AD users map.
            /// </param>
            /// <param name="groupIds">
            ///     Internal identifier to external identifier of AD groups map.
            /// </param>
            /// <param name="userRoles">
            ///     QEF role identifier to list of internal identifiers of AD users map.
            /// </param>
            /// <param name="groupRoles">
            ///     QEF role identifier to list of internal identifiers of AD groups map.
            /// </param>
            /// <param name="groupUsersMembership">
            ///     QEF local group identifier to list of internal identifiers of AD users map.
            /// </param>
            /// <param name="groupGroupsMembership">
            ///     QEF local group identifier to list of internal identifiers of AD groups map.
            /// </param>
            /// <param name="licenses">
            ///     QEF license serial to list of internal identifiers of AD users map.
            /// </param>
            public QefAssignments(
                Dictionary<Guid, string> userIds,
                Dictionary<Guid, string> groupIds,
                Dictionary<Rid, List<Guid>> userRoles,
                Dictionary<Rid, List<Guid>> groupRoles,
                Dictionary<Guid, List<Guid>> groupUsersMembership,
                Dictionary<Guid, List<Guid>> groupGroupsMembership,
                Dictionary<SerialNumber, List<Guid>> licenses)
            {
                #region Argument Check

                if (userIds == null)
                {
                    throw new ArgumentNullException("userIds");
                }
                if (userIds.Values.Any(item => string.IsNullOrWhiteSpace(item)))
                {
                    throw new ArgumentException("Map cannot have null or empty external id.", "userIds");
                }

                if (groupIds == null)
                {
                    throw new ArgumentNullException("groupIds");
                }
                if (groupIds.Values.Any(item => string.IsNullOrWhiteSpace(item)))
                {
                    throw new ArgumentException("Map cannot have null or empty external id.", "groupIds");
                }

                if (userRoles == null)
                {
                    throw new ArgumentNullException("userRoles");
                }
                if (userRoles.Values.Any(item => item == null || item.Count == 0))
                {
                    throw new ArgumentException("Map cannot have null or empty user id list.", "userRoles");
                }

                if (groupRoles == null)
                {
                    throw new ArgumentNullException("groupRoles");
                }
                if (groupRoles.Values.Any(item => item == null || item.Count == 0))
                {
                    throw new ArgumentException("Map cannot have null or empty group id list.", "groupRoles");
                }

                if (groupUsersMembership == null)
                {
                    throw new ArgumentNullException("groupUsersMembership");
                }
                if (groupUsersMembership.Values.Any(item => item == null || item.Count == 0))
                {
                    throw new ArgumentException("Map cannot have null or empty user id list.", "groupUsersMembership");
                }

                if (groupGroupsMembership == null)
                {
                    throw new ArgumentNullException("groupGroupsMembership");
                }
                if (groupGroupsMembership.Values.Any(item => item == null || item.Count == 0))
                {
                    throw new ArgumentException("Map cannot have null or empty group id list.", "groupGroupsMembership");
                }

                if (licenses == null)
                {
                    throw new ArgumentNullException("licenses");
                }
                if (licenses.Values.Any(item => item == null || item.Count == 0))
                {
                    throw new ArgumentException("Map cannot have null or empty user id list.", "licenses");
                }

                #endregion

                UserIds = userIds;
                GroupIds = groupIds;
                RoleUsers = userRoles;
                RoleGroups = groupRoles;
                GroupUsersMembership = groupUsersMembership;
                GroupGroupsMembership = groupGroupsMembership;
                Licenses = licenses;
            }

            #endregion

            #region Private Methods

            private static QException CreateUnexpectedNodeException(string nodeName)
            {
                return new QException(string.Format("Invalid XML. Unexpected node name: {0}.", nodeName));
            }

            private static QException CreateUnexpectedNodeException(string nodeName, string expectedNodeName)
            {
                return new QException(
                    string.Format(
                        "Invalid XML. Unexpected node name. Expected {0} but was {1}.",
                        expectedNodeName,
                        nodeName));
            }

            private void ReadGroupGroups(XmlReader reader)
            {
                if (reader.IsEmptyElement)
                {
                    return;
                }
                reader.Read();
                while (!(reader.Name == c_groupGroupsMembership && reader.NodeType == XmlNodeType.EndElement))
                {
                    if (reader.Name != c_mapItemAttributeName)
                    {
                        throw CreateUnexpectedNodeException(reader.Name, c_mapItemAttributeName);
                    }

                    reader.Read();
                    if (reader.Name != c_groupIdAttributeName)
                    {
                        throw CreateUnexpectedNodeException(reader.Name, c_groupIdAttributeName);
                    }
                    var groupId = new Guid(reader.ReadString());

                    reader.Read();
                    if (reader.Name != c_groupsAttributeName)
                    {
                        throw CreateUnexpectedNodeException(reader.Name, c_groupsAttributeName);
                    }

                    var groups = new List<Guid>();
                    reader.Read();
                    while (!(reader.Name == c_groupsAttributeName && reader.NodeType == XmlNodeType.EndElement))
                    {
                        if (reader.Name != c_internalGroupIdAttributeName)
                        {
                            throw CreateUnexpectedNodeException(reader.Name, c_internalGroupIdAttributeName);
                        }
                        groups.Add(new Guid(reader.ReadString()));
                        reader.Read();
                    }
                    GroupGroupsMembership.Add(groupId, groups);

                    reader.Read();
                    reader.Read();
                }
            }

            private void ReadGroupIds(XmlReader reader)
            {
                if (reader.IsEmptyElement)
                {
                    return;
                }
                reader.Read();
                while (!(reader.Name == c_groupIdsMapAttributeName && reader.NodeType == XmlNodeType.EndElement))
                {
                    if (reader.Name != c_mapItemAttributeName)
                    {
                        throw CreateUnexpectedNodeException(reader.Name, c_mapItemAttributeName);
                    }

                    reader.Read();
                    if (reader.Name != c_internalGroupIdAttributeName)
                    {
                        throw CreateUnexpectedNodeException(reader.Name, c_internalGroupIdAttributeName);
                    }
                    var internalGroupId = new Guid(reader.ReadString());

                    reader.Read();
                    if (reader.Name != c_externalGroupIdAttributeName)
                    {
                        throw CreateUnexpectedNodeException(reader.Name, c_externalGroupIdAttributeName);
                    }
                    var externalGroupId = reader.ReadString();

                    GroupIds.Add(internalGroupId, externalGroupId);
                    reader.Read();
                    reader.Read();
                }
            }

            private void ReadGroupUsers(XmlReader reader)
            {
                if (reader.IsEmptyElement)
                {
                    return;
                }
                reader.Read();
                while (!(reader.Name == c_groupUsersMembership && reader.NodeType == XmlNodeType.EndElement))
                {
                    if (reader.Name != c_mapItemAttributeName)
                    {
                        throw CreateUnexpectedNodeException(reader.Name, c_mapItemAttributeName);
                    }

                    reader.Read();
                    if (reader.Name != c_groupIdAttributeName)
                    {
                        throw CreateUnexpectedNodeException(reader.Name, c_groupIdAttributeName);
                    }
                    var groupId = new Guid(reader.ReadString());

                    reader.Read();
                    if (reader.Name != c_usersAttributeName)
                    {
                        throw CreateUnexpectedNodeException(reader.Name, c_usersAttributeName);
                    }

                    var users = new List<Guid>();
                    reader.Read();
                    while (!(reader.Name == c_usersAttributeName && reader.NodeType == XmlNodeType.EndElement))
                    {
                        if (reader.Name != c_internalUserIdAttributeName)
                        {
                            throw CreateUnexpectedNodeException(reader.Name, c_internalUserIdAttributeName);
                        }
                        users.Add(new Guid(reader.ReadString()));
                        reader.Read();
                    }
                    GroupUsersMembership.Add(groupId, users);

                    reader.Read();
                    reader.Read();
                }
            }

            private void ReadLicenses(XmlReader reader)
            {
                if (reader.IsEmptyElement)
                {
                    return;
                }
                reader.Read();
                while (!(reader.Name == c_licensesMapAttributeName && reader.NodeType == XmlNodeType.EndElement))
                {
                    if (reader.Name != c_mapItemAttributeName)
                    {
                        throw CreateUnexpectedNodeException(reader.Name, c_mapItemAttributeName);
                    }

                    reader.Read();
                    if (reader.Name != c_licenseAttributeName)
                    {
                        throw CreateUnexpectedNodeException(reader.Name, c_licenseAttributeName);
                    }
                    var license = SerialNumber.Parse(reader.ReadString());

                    reader.Read();
                    if (reader.Name != c_usersAttributeName)
                    {
                        throw CreateUnexpectedNodeException(reader.Name, c_usersAttributeName);
                    }

                    var users = new List<Guid>();
                    reader.Read();
                    while (!(reader.Name == c_usersAttributeName && reader.NodeType == XmlNodeType.EndElement))
                    {
                        if (reader.Name != c_internalUserIdAttributeName)
                        {
                            throw CreateUnexpectedNodeException(reader.Name, c_internalUserIdAttributeName);
                        }
                        users.Add(new Guid(reader.ReadString()));
                        reader.Read();
                    }
                    Licenses.Add(license, users);

                    reader.Read();
                    reader.Read();
                }
            }

            private void ReadRoleGroups(XmlReader reader)
            {
                if (reader.IsEmptyElement)
                {
                    return;
                }
                reader.Read();
                while (!(reader.Name == c_roleGroupsMapAttributeName && reader.NodeType == XmlNodeType.EndElement))
                {
                    if (reader.Name != c_mapItemAttributeName)
                    {
                        throw CreateUnexpectedNodeException(reader.Name, c_mapItemAttributeName);
                    }

                    reader.Read();
                    if (reader.Name != c_roleIdAttributeName)
                    {
                        throw CreateUnexpectedNodeException(reader.Name, c_roleIdAttributeName);
                    }
                    var roleId = new Rid(reader.ReadString());

                    reader.Read();
                    if (reader.Name != c_groupsAttributeName)
                    {
                        throw CreateUnexpectedNodeException(reader.Name, c_groupsAttributeName);
                    }

                    var groups = new List<Guid>();
                    reader.Read();
                    while (!(reader.Name == c_groupsAttributeName && reader.NodeType == XmlNodeType.EndElement))
                    {
                        if (reader.Name != c_internalGroupIdAttributeName)
                        {
                            throw CreateUnexpectedNodeException(reader.Name, c_internalGroupIdAttributeName);
                        }
                        groups.Add(new Guid(reader.ReadString()));
                        reader.Read();
                    }
                    RoleGroups.Add(roleId, groups);

                    reader.Read();
                    reader.Read();
                }
            }

            private void ReadRoleUsers(XmlReader reader)
            {
                if (reader.IsEmptyElement)
                {
                    return;
                }
                reader.Read();
                while (!(reader.Name == c_roleUsersMapAttributeName && reader.NodeType == XmlNodeType.EndElement))
                {
                    if (reader.Name != c_mapItemAttributeName)
                    {
                        throw CreateUnexpectedNodeException(reader.Name, c_mapItemAttributeName);
                    }

                    reader.Read();
                    if (reader.Name != c_roleIdAttributeName)
                    {
                        throw CreateUnexpectedNodeException(reader.Name, c_roleIdAttributeName);
                    }
                    var roleId = new Rid(reader.ReadString());

                    reader.Read();
                    if (reader.Name != c_usersAttributeName)
                    {
                        throw CreateUnexpectedNodeException(reader.Name, c_usersAttributeName);
                    }

                    var users = new List<Guid>();
                    reader.Read();
                    while (!(reader.Name == c_usersAttributeName && reader.NodeType == XmlNodeType.EndElement))
                    {
                        if (reader.Name != c_internalUserIdAttributeName)
                        {
                            throw CreateUnexpectedNodeException(reader.Name, c_internalUserIdAttributeName);
                        }
                        users.Add(new Guid(reader.ReadString()));
                        reader.Read();
                    }
                    RoleUsers.Add(roleId, users);

                    reader.Read();
                    reader.Read();
                }
            }

            private void ReadUserIds(XmlReader reader)
            {
                if (reader.IsEmptyElement)
                {
                    return;
                }
                reader.Read();
                while (!(reader.Name == c_userIdsMapAttributeName && reader.NodeType == XmlNodeType.EndElement))
                {
                    if (reader.Name != c_mapItemAttributeName)
                    {
                        throw CreateUnexpectedNodeException(reader.Name, c_mapItemAttributeName);
                    }

                    reader.Read();
                    if (reader.Name != c_internalUserIdAttributeName)
                    {
                        throw CreateUnexpectedNodeException(reader.Name, c_internalUserIdAttributeName);
                    }
                    var internalUserId = new Guid(reader.ReadString());

                    reader.Read();
                    if (reader.Name != c_externalUserIdAttributeName)
                    {
                        throw CreateUnexpectedNodeException(reader.Name, c_externalUserIdAttributeName);
                    }
                    var externalUserId = reader.ReadString();
                    UserIds.Add(internalUserId, externalUserId);

                    reader.Read();
                    reader.Read();
                }
            }

            #endregion

            #region Public Properties

            /// <summary>
            ///     Gets map of QEF local group identifier to list of internal identifiers of AD groups.
            /// </summary>
            [XmlIgnore]
            public Dictionary<Guid, List<Guid>> GroupGroupsMembership
            {
                get;
                private set;
            }

            /// <summary>
            ///     Gets map of internal identifier to external identifier of AD groups.
            /// </summary>
            [XmlIgnore]
            public Dictionary<Guid, string> GroupIds
            {
                get;
                private set;
            }

            /// <summary>
            ///     Gets map of QEF local group identifier to list of internal identifiers of AD users.
            /// </summary>
            [XmlIgnore]
            public Dictionary<Guid, List<Guid>> GroupUsersMembership
            {
                get;
                private set;
            }

            /// <summary>
            ///     Gets map of QEF license serial to list of internal identifiers of AD users.
            /// </summary>
            [XmlIgnore]
            public Dictionary<SerialNumber, List<Guid>> Licenses
            {
                get;
                private set;
            }

            /// <summary>
            ///     Gets map of QEF role identifier to list of internal identifiers of AD groups.
            /// </summary>
            [XmlIgnore]
            public Dictionary<Rid, List<Guid>> RoleGroups
            {
                get;
                private set;
            }

            /// <summary>
            ///     Gets map of QEF role identifier to list of internal identifiers of AD users.
            /// </summary>
            [XmlIgnore]
            public Dictionary<Rid, List<Guid>> RoleUsers
            {
                get;
                private set;
            }

            /// <summary>
            ///     Gets map of internal identifier to external identifier of AD users.
            /// </summary>
            [XmlIgnore]
            public Dictionary<Guid, string> UserIds
            {
                get;
                private set;
            }

            #endregion

            #region Public Methods

            /// <summary>
            ///     Deserializes an instane of the <see cref="QefAssignments" /> from XML file.
            /// </summary>
            /// <param name="filePath">
            ///     File name to read and deserialize from.
            /// </param>
            /// <returns>
            ///     Deserialized instance of the <see cref="QefAssignments" />.
            /// </returns>
            public static QefAssignments Deserialize(string filePath)
            {
                using (var fileStream = File.OpenRead(filePath))
                {
                    var serializer = new XmlSerializer(typeof(QefAssignments));
                    return (QefAssignments)serializer.Deserialize(fileStream);
                }
            }

            /// <summary>
            ///     Serializes given instane of the <see cref="QefAssignments" /> to XML file.
            /// </summary>
            /// <param name="qefAssignments">
            ///     Instance of the <see cref="QefAssignments" /> to serialize.
            /// </param>
            /// <param name="filePath">
            ///     File name to write serialization output to.
            /// </param>
            public static void Serialize(QefAssignments qefAssignments, string filePath)
            {
                using (var fileStream = File.Open(filePath, FileMode.Create, FileAccess.Write, FileShare.Read))
                {
                    using (var writer = XmlWriter.Create(
                        fileStream,
                        new XmlWriterSettings
                        {
                            Encoding = Encoding.UTF8,
                            Indent = true
                        }))
                    {
                        new XmlSerializer(typeof(QefAssignments)).Serialize(writer, qefAssignments);
                    }
                }
            }

            #endregion

            #region IXmlSerializable Members

            public XmlSchema GetSchema()
            {
                return null;
            }

            public void ReadXml(XmlReader reader)
            {
                string rootNodeName = GetType().Name;
                if (reader.Name != rootNodeName)
                {
                    throw CreateUnexpectedNodeException(reader.Name);
                }

                while (reader.Read())
                {
                    switch (reader.Name)
                    {
                        case c_userIdsMapAttributeName:
                            ReadUserIds(reader);
                            break;
                        case c_groupIdsMapAttributeName:
                            ReadGroupIds(reader);
                            break;
                        case c_roleUsersMapAttributeName:
                            ReadRoleUsers(reader);
                            break;
                        case c_roleGroupsMapAttributeName:
                            ReadRoleGroups(reader);
                            break;
                        case c_groupUsersMembership:
                            ReadGroupUsers(reader);
                            break;
                        case c_groupGroupsMembership:
                            ReadGroupGroups(reader);
                            break;
                        case c_licensesMapAttributeName:
                            ReadLicenses(reader);
                            break;
                        default:
                            if (reader.Name != rootNodeName || reader.NodeType != XmlNodeType.EndElement)
                            {
                                throw CreateUnexpectedNodeException(reader.Name);
                            }
                            break;
                    }
                }
            }

            public void WriteXml(XmlWriter writer)
            {
                //UserIds
                writer.WriteStartElement(c_userIdsMapAttributeName);
                foreach (var userId in UserIds)
                {
                    writer.WriteStartElement(c_mapItemAttributeName);
                    writer.WriteElementString(c_internalUserIdAttributeName, userId.Key.ToString());
                    writer.WriteElementString(c_externalUserIdAttributeName, userId.Value);
                    writer.WriteEndElement();
                }
                writer.WriteEndElement();

                //GroupIds
                writer.WriteStartElement(c_groupIdsMapAttributeName);
                foreach (var groupId in GroupIds)
                {
                    writer.WriteStartElement(c_mapItemAttributeName);
                    writer.WriteElementString(c_internalGroupIdAttributeName, groupId.Key.ToString());
                    writer.WriteElementString(c_externalGroupIdAttributeName, groupId.Value);
                    writer.WriteEndElement();
                }
                writer.WriteEndElement();

                //RoleUsers
                writer.WriteStartElement(c_roleUsersMapAttributeName);
                foreach (var userRole in RoleUsers)
                {
                    writer.WriteStartElement(c_mapItemAttributeName);
                    writer.WriteElementString(c_roleIdAttributeName, userRole.Key.ToString());
                    writer.WriteStartElement(c_usersAttributeName);
                    foreach (var id in userRole.Value)
                    {
                        writer.WriteElementString(c_internalUserIdAttributeName, id.ToString());
                    }
                    writer.WriteEndElement();
                    writer.WriteEndElement();
                }
                writer.WriteEndElement();

                //RoleGroups
                writer.WriteStartElement(c_roleGroupsMapAttributeName);
                foreach (var groupRole in RoleGroups)
                {
                    writer.WriteStartElement(c_mapItemAttributeName);
                    writer.WriteElementString(c_roleIdAttributeName, groupRole.Key.ToString());
                    writer.WriteStartElement(c_groupsAttributeName);
                    foreach (var id in groupRole.Value)
                    {
                        writer.WriteElementString(c_internalGroupIdAttributeName, id.ToString());
                    }
                    writer.WriteEndElement();
                    writer.WriteEndElement();
                }
                writer.WriteEndElement();

                //UsersMembership
                writer.WriteStartElement(c_groupUsersMembership);
                foreach (var membership in GroupUsersMembership)
                {
                    writer.WriteStartElement(c_mapItemAttributeName);
                    writer.WriteElementString(c_groupIdAttributeName, membership.Key.ToString());
                    writer.WriteStartElement(c_usersAttributeName);
                    foreach (var id in membership.Value)
                    {
                        writer.WriteElementString(c_internalUserIdAttributeName, id.ToString());
                    }
                    writer.WriteEndElement();
                    writer.WriteEndElement();
                }
                writer.WriteEndElement();

                //GroupsMembership
                writer.WriteStartElement(c_groupGroupsMembership);
                foreach (var membership in GroupGroupsMembership)
                {
                    writer.WriteStartElement(c_mapItemAttributeName);
                    writer.WriteElementString(c_groupIdAttributeName, membership.Key.ToString());
                    writer.WriteStartElement(c_groupsAttributeName);
                    foreach (var id in membership.Value)
                    {
                        writer.WriteElementString(c_internalGroupIdAttributeName, id.ToString());
                    }
                    writer.WriteEndElement();
                    writer.WriteEndElement();
                }
                writer.WriteEndElement();

                //Licenses
                writer.WriteStartElement(c_licensesMapAttributeName);
                foreach (var licenseMembership in Licenses)
                {
                    writer.WriteStartElement(c_mapItemAttributeName);
                    writer.WriteElementString(c_licenseAttributeName, licenseMembership.Key.Value);
                    writer.WriteStartElement(c_usersAttributeName);
                    foreach (var id in licenseMembership.Value)
                    {
                        writer.WriteElementString(c_internalUserIdAttributeName, id.ToString());
                    }
                    writer.WriteEndElement();
                    writer.WriteEndElement();
                }
                writer.WriteEndElement();
            }

            #endregion
        }

        #endregion

        #region Constants

        private const int c_sqlCommandTimeout = 7200;

        #endregion

        #region Fields

        private Dictionary<Guid, IGroup> m_cachedQefGroups;
        private Dictionary<SerialNumber, ILicense> m_cachedQefLicenses;
        private Dictionary<Guid, IRoleHolder> m_cachedQefRoleHolders;
        private Dictionary<Rid, IRole> m_cachedQefRoles;
        private Dictionary<Guid, IUser> m_cachedQefUsers;

        // After the constant is updated, run UnitTest to ensure that database structure, 
        // which is made direct calls to, is compatible with the script.
        private static readonly DataStorageVersion c_requiredDbVersion = DataStorageVersion.Create("QIS", 6, 5);

        private readonly string m_filePath;
        private readonly ILicenseManagementService m_licenseManagement;
        private readonly ILicenseStorageService m_licenseStorageService;
        private readonly ILogWriter m_log;
        private readonly IQisServer m_qisServer;
        private readonly IUserStorageService m_userStorageService;

        #endregion

        #region Constructors and Destructors

        private ReassignPermissions(IQisServer qisServer, string filePath, string logPath)
        {
            m_qisServer = qisServer;
            m_filePath = filePath;
            m_userStorageService = qisServer.GetQefService<IUserStorageService>();
            m_licenseManagement = m_qisServer.GetQefService<ILicenseManagementService>();
            m_licenseStorageService = qisServer.GetQefService<ILicenseStorageService>();
            ILogWriter log = qisServer.GetQefService<ILogService>().GetLogWriter(
                QisConst.QisId,
                Source.Parse(GetType().FullName));
            m_log = logPath.IsBlank() ? log : new DoubleLogWriter(log, logPath);
        }

        #endregion

        #region Private Properties

        private Dictionary<Guid, IGroup> CachedQefGroups
        {
            get
            {
                return m_cachedQefGroups
                    ?? (m_cachedQefGroups = m_userStorageService.GetAllGroups().ToDictionary(key => key.Id));
            }
        }

        private Dictionary<SerialNumber, ILicense> CachedQefLicenses
        {
            get
            {
                return m_cachedQefLicenses
                    ?? (m_cachedQefLicenses = m_licenseStorageService
                        .GetLicenses()
                        .ToDictionary(key => key.SerialNumber));
            }
        }

        private Dictionary<Guid, IRoleHolder> CachedQefRoleHolders
        {
            get
            {
                return m_cachedQefRoleHolders
                    ?? (m_cachedQefRoleHolders = CachedQefUsers
                        .Select(p => new KeyValuePair<Guid, IRoleHolder>(p.Value.Id, p.Value))
                        .Concat(CachedQefGroups.Select(p => new KeyValuePair<Guid, IRoleHolder>(p.Value.Id, p.Value)))
                        .ToDefaultDictionary());
            }
        }

        private Dictionary<Rid, IRole> CachedQefRoles
        {
            get
            {
                return m_cachedQefRoles
                    ?? (m_cachedQefRoles = m_userStorageService.GetAllRoles().ToDictionary(key => key.Id));
            }
        }

        private Dictionary<Guid, IUser> CachedQefUsers
        {
            get
            {
                return m_cachedQefUsers
                    ?? (m_cachedQefUsers = m_userStorageService.GetAllUsers().ToDictionary(key => key.Id));
            }
        }

        #endregion

        #region Private Methods

        private static QefAssignments LoadQefAssignments(string filePath)
        {
            return QefAssignments.Deserialize(filePath);
        }

        private static void SaveQefAssignments(QefAssignments qefAssignments, string filePath)
        {
            QefAssignments.Serialize(qefAssignments, filePath);
        }

        private void ClearCaches()
        {
            m_cachedQefUsers = null;
            m_cachedQefGroups = null;
            m_cachedQefRoleHolders = null;
            m_cachedQefRoles = null;
            m_cachedQefLicenses = null;
        }

        private IEnumerable<IRepository> GetAllRepositoriesAndMetamodels()
        {
            return m_qisServer
                .FindAllRepositories()
                .Concat(m_qisServer.FindAllMetamodelDatabaseRepositories());
        }

        private QefAssignments GetQefAssignments()
        {
            var adUsers = m_userStorageService
                .GetAllUsers()
                .Where(u => !u.IsLocal)
                .ToList();
            var allGroups = m_userStorageService
                .GetAllGroups()
                .ToList();
            var adGroups = allGroups.Where(group => !group.IsLocal).ToList();
            var localGroups = allGroups.Where(group => group.IsLocal).ToList();

            // Ids
            var userIdsMap = adUsers.ToDictionary(key => key.Id, val => val.ExternalId);
            var groupIdsMap = adGroups.ToDictionary(key => key.Id, val => val.ExternalId);

            // Roles
            var userRolesMap = new Dictionary<Rid, List<Guid>>();
            var groupRolesMap = new Dictionary<Rid, List<Guid>>();
            m_userStorageService
                .GetAllRoles()
                .DoForEach(
                    role =>
                    {
                        var roleUsers = role.Users.Where(user => !user.IsLocal).Select(user => user.Id).ToList();
                        var roleGroups = role.Groups.Where(group => !group.IsLocal).Select(group => group.Id).ToList();
                        if (roleUsers.Any())
                        {
                            userRolesMap.Add(role.Id, roleUsers);
                        }
                        if (roleGroups.Any())
                        {
                            groupRolesMap.Add(role.Id, roleGroups);
                        }
                    });

            // Group membership
            var groupUsersMembershipMap = new Dictionary<Guid, List<Guid>>();
            var groupGroupsMembershipMap = new Dictionary<Guid, List<Guid>>();
            localGroups.DoForEach(
                group =>
                {
                    //Skip AllUsers group
                    if (group.Id == QefConst.AllUsersGroupId)
                    {
                        return;
                    }

                    var groupUsers = group.Users.Where(user => !user.IsLocal).Select(user => user.Id).ToList();
                    var groupGroups = group.Children.Where(child => !child.IsLocal).Select(child => child.Id).ToList();
                    if (groupUsers.Any())
                    {
                        groupUsersMembershipMap.Add(group.Id, groupUsers);
                    }
                    if (groupGroups.Any())
                    {
                        groupGroupsMembershipMap.Add(group.Id, groupGroups);
                    }
                });

            // Licenses assignments
            var licencesMap = new Dictionary<SerialNumber, List<Guid>>();
            m_licenseStorageService
                .GetLicenses()
                .Where(license => license.BindingType == LicenseBindingType.PerUser)
                .DoForEach(
                    license =>
                    {
                        var users = license.UserData.Select(userId => userId.Value).Intersect(userIdsMap.Keys).ToList();
                        if (users.Any())
                        {
                            licencesMap.Add(license.SerialNumber, users);
                        }
                    }
                );

            return new QefAssignments(
                userIdsMap,
                groupIdsMap,
                userRolesMap,
                groupRolesMap,
                groupUsersMembershipMap,
                groupGroupsMembershipMap,
                licencesMap);
        }

        private void RestoreAssignments(QefAssignments qefAssignments)
        {
            ClearCaches();
            var oldInternalToExternalId = qefAssignments.UserIds.Concat(qefAssignments.GroupIds).ToDefaultDictionary();

            m_log.Info("Stage3.2.1 restoring user settings. Started.");
            var oldInternalToNewInternalId = ScanNewUsersAndReassignQisPermissions(oldInternalToExternalId);
            m_log.Info("Stage3.2.1 restoring user settings. Completed.");

            m_log.Info("Stage3.2.2 restoring role assignments. Started.");
            RestoreQefRoleAssignments(qefAssignments, oldInternalToNewInternalId);
            m_log.Info("Stage3.2.2 restoring role assignments. Completed.");

            m_log.Info("Stage3.2.3 restoring local groups membership. Started.");
            RestoreQefGroupsMembership(qefAssignments, oldInternalToNewInternalId);
            m_log.Info("Stage3.2.3 restoring local groups membership. Completed.");

            m_log.Info("Stage3.2.4 restoring licenses assignments. Started.");
            RestoreQefLicenseAssignments(qefAssignments, oldInternalToNewInternalId);
            m_log.Info("Stage3.2.4 restoring licenses assignments. Completed.");
        }

        private void RestoreQefGroupsMembership(
            QefAssignments qefAssignments,
            Dictionary<Guid, Guid> oldInternalToNewInternalId)
        {
            var oldGroupIds = qefAssignments.GroupUsersMembership.Keys.Union(qefAssignments.GroupGroupsMembership.Keys);
            foreach (var groupId in oldGroupIds)
            {
                IGroup localGroup;
                if (!CachedQefGroups.TryGetValue(groupId, out localGroup) || !localGroup.IsLocal)
                {
                    continue;
                }

                List<Guid> users;
                if (qefAssignments.GroupUsersMembership.TryGetValue(groupId, out users))
                {
                    var newUserIds = new List<Guid>();
                    foreach (var user in users)
                    {
                        Guid newUser;
                        if (oldInternalToNewInternalId.TryGetValue(user, out newUser)
                            && CachedQefUsers.ContainsKey(newUser))
                        {
                            newUserIds.Add(newUser);
                        }
                    }

                    if (newUserIds.Any())
                    {
                        localGroup.Users.Replace(
                            localGroup.Users.Select(user => user.Id).Union(newUserIds).ToArray());
                    }
                }

                List<Guid> groups;
                if (qefAssignments.GroupGroupsMembership.TryGetValue(groupId, out groups))
                {
                    var newGroupIds = new List<Guid>();
                    foreach (var group in groups)
                    {
                        Guid newGroup;
                        if (oldInternalToNewInternalId.TryGetValue(group, out newGroup)
                            && CachedQefGroups.ContainsKey(newGroup))
                        {
                            newGroupIds.Add(newGroup);
                        }
                    }

                    if (newGroupIds.Any())
                    {
                        localGroup.Children.Replace(
                            localGroup.Children.Select(group => group.Id).Union(newGroupIds).ToArray());
                    }
                }
            }
        }

        private void RestoreQefLicenseAssignments(
            QefAssignments qefAssignments,
            Dictionary<Guid, Guid> oldInternalToNewInternalId)
        {
            foreach (var licenseSerial in qefAssignments.Licenses.Keys)
            {
                ILicense license;
                if (!CachedQefLicenses.TryGetValue(licenseSerial, out license) || !license.IsValid)
                {
                    continue;
                }

                List<Guid> users;
                if (!qefAssignments.Licenses.TryGetValue(licenseSerial, out users))
                {
                    continue;
                }

                //var allowedToAssign = license.OwnerLimit - license.UserIds.Count;
                //var assigned = 0;
                var newUserIds = new List<Guid>();
                foreach (var user in users)
                {
                    Guid newUser;
                    if (!oldInternalToNewInternalId.TryGetValue(user, out newUser)
                        || !CachedQefUsers.ContainsKey(newUser))
                    {
                        continue;
                    }

                    newUserIds.Add(newUser);
                    //var unbind = license.UserIds.Contains(new QefUserId(user));
                    //if (unbind)
                    //{
                    //    licenseManagement.UnbindLicenseFromUser(licenseSerial, new QefUserId(user));
                    //}

                    //if (!unbind && allowedToAssign <= assigned)
                    //{
                    //    continue;
                    //}

                    //licenseManagement.BindLicenseToUser(licenseSerial, new QefUserId(newUser));
                    //assigned++;
                }

                if (!newUserIds.Any())
                {
                    continue;
                }

                var usersToAssign = license
                    .UserData
                    .Union(newUserIds.Select(user => QefUserId.Parse(user)))
                    .ToList();
                m_licenseManagement.ChangeLicenseAssignment(licenseSerial, usersToAssign);
            }
        }

        private void RestoreQefRoleAssignments(
            QefAssignments qefAssignments,
            Dictionary<Guid, Guid> oldInternalToNewInternalId)
        {
            foreach (var roleId in qefAssignments.RoleUsers.Keys.Union(qefAssignments.RoleGroups.Keys))
            {
                IRole role;
                if (!CachedQefRoles.TryGetValue(roleId, out role))
                {
                    continue;
                }

                List<Guid> users;
                if (qefAssignments.RoleUsers.TryGetValue(roleId, out users))
                {
                    var newUserIds = new List<Guid>();
                    foreach (var user in users)
                    {
                        Guid newUser;
                        if (oldInternalToNewInternalId.TryGetValue(user, out newUser)
                            && CachedQefUsers.ContainsKey(newUser))
                        {
                            newUserIds.Add(newUser);
                        }
                    }

                    if (newUserIds.Any())
                    {
                        role.Users.Replace(
                            role.Users.Select(user => user.Id).Union(newUserIds).ToArray());
                    }
                }

                List<Guid> groups;
                if (qefAssignments.RoleGroups.TryGetValue(roleId, out groups))
                {
                    var newGroupIds = new List<Guid>();
                    foreach (var group in groups)
                    {
                        Guid newGroup;
                        if (oldInternalToNewInternalId.TryGetValue(group, out newGroup)
                            && CachedQefGroups.ContainsKey(newGroup))
                        {
                            newGroupIds.Add(newGroup);
                        }
                    }

                    if (newGroupIds.Any())
                    {
                        role.Groups.Replace(
                            role.Groups.Select(group => group.Id).Union(newGroupIds).ToArray());
                    }
                }
            }
        }

        private void ResyncWithActiveDirectory()
        {
            if (m_userStorageService.IsADSynchronizationStarted)
            {
                m_log.Info("Stopping AD synchronization.");
                m_userStorageService.StopADSynchronization();
            }
            m_log.Info("Removing AD users.");
            m_userStorageService.DeleteADUsersAndGroups();
            m_userStorageService.ClearCaches();
            m_log.Info("Starting AD synchronization.");
            m_userStorageService.StartADSynchronization();
        }

        private Dictionary<Guid, Guid> ScanNewUsersAndReassignQisPermissions(
            Dictionary<Guid, string> oldInternalToExternalId)
        {
            m_log.Info("Waiting until AD synchronization completed.");
            while (m_userStorageService.ADSynchronizationStatus != LdapSynchronizationStatus.ProcessingNotification
                && m_userStorageService.ADSynchronizationStatus != LdapSynchronizationStatus.Sleeping)
            {
                Thread.Sleep(200);
                if (m_userStorageService.ADSynchronizationStatus == LdapSynchronizationStatus.Disabled)
                {
                    break;
                }
            }

            m_log.Info("Reading user/group data from QEF.");

            HashSet<Guid> localIds = new HashSet<Guid>(
                CachedQefUsers
                    .Values
                    .Where(u => u.IsLocal)
                    .Select(u => u.Id)
                    .Concat(
                        CachedQefGroups
                            .Values
                            .Where(g => g.IsLocal)
                            .Select(g => g.Id)));

            HashSet<Guid> adIds = new HashSet<Guid>(
                CachedQefUsers
                    .Values
                    .Where(u => !u.IsLocal)
                    .Select(u => u.Id)
                    .Concat(
                        CachedQefGroups
                            .Values
                            .Where(g => !g.IsLocal)
                            .Select(g => g.Id)));

            var newExternalToInternalId = CachedQefUsers
                .Values
                .Where(u => !u.IsLocal)
                .Select(u => new KeyValuePair<string, Guid>(u.ExternalId, u.Id))
                .Concat(
                    CachedQefGroups
                        .Values
                        .Where(g => !g.IsLocal)
                        .Select(g => new KeyValuePair<string, Guid>(g.ExternalId, g.Id)))
                .ToDefaultDictionary();

            m_log.Info("Creating old to new user Id map.");
            var oldInternalToNewInternalId = oldInternalToExternalId
                .ToDictionary(
                    p => p.Key,
                    p =>
                    {
                        if (!newExternalToInternalId.ContainsKey(p.Value))
                        {
                            m_log.Warning(
                                string.Format(
                                    "Can't find new Id for pair (old Id: {0}  ExternalId: {1}). Old Id used.",
                                    p.Key,
                                    p.Value));
                            return p.Key;
                        }
                        return newExternalToInternalId[p.Value];
                    });

            m_log.Info("Reading all repositories.");
            var repositoryList = GetAllRepositoriesAndMetamodels();

            m_log.Info("Saving data storage references.");
            var repIdToDataStorageReference = repositoryList
                .Where(r => r.DataStorageExists)
                .ToDictionary(r => r.Id, r => r.GetData().DataStorageReference);

            HashSet<RepId> onlineRepositories = new HashSet<RepId>(
                repositoryList
                    .Where(r => r.GetData().Status != RepositoryStatus.Offline)
                    .Select(r => r.Id));

            try
            {
                m_log.Info("Disconnecting all users and taking repositories offline.");
                repositoryList.DoForEach(
                    r =>
                    {
                        r.DisconnectAllUsers();
                        if (onlineRepositories.Contains(r.Id))
                        {
                            r.TakeOffline();
                        }
                    });

                //Set all DataStorageReferences to null
                m_log.Info("Cleaning data storage references.");
                repositoryList.DoForEach(
                    r =>
                    {
                        var data = r.GetData();
                        data.DataStorageReference = null;
                        r.SetData(data);
                    });

                #region Fix default role settings via public API

                m_log.Info("Processing default role settings.");
                foreach (IRepositoryRole role in m_qisServer.RepositoryRoles)
                {
                    OwnerInfo oldInfo = role.DefaultUserGroup;
                    if (oldInfo != null && oldInfo != OwnerInfo.Empty)
                    {
                        Guid newUserOrGroupId;
                        if (!oldInternalToNewInternalId.TryGetValue(oldInfo.Id, out newUserOrGroupId))
                        {
                            if (!localIds.Contains(oldInfo.Id) && !adIds.Contains(oldInfo.Id))
                            {
                                m_log.Warning(
                                    string.Format(
                                        "Can't resolve Id {0} to new Id. Old Id used. DefaultUserGroup in role: {1}",
                                        oldInfo.Id,
                                        role.Name));
                            }
                        }
                        else
                        {
                            if (oldInfo.Id != newUserOrGroupId)
                            {
                                role.DefaultUserGroup = new OwnerInfo(newUserOrGroupId, oldInfo.Name);
                            }
                        }
                    }

                    INewObjectAcl acl = role.NewObjectAcl;
                    var oldAclEntries = acl.ToList();
                    bool newAclHasChanges = false;
                    foreach (IObjectAclEntry oldAclEntry in oldAclEntries)
                    {
                        Guid newUserOrGroupId;
                        if (!oldInternalToNewInternalId.TryGetValue(oldAclEntry.UserOrGroupId, out newUserOrGroupId))
                        {
                            if (!localIds.Contains(oldAclEntry.UserOrGroupId)
                                && !adIds.Contains(oldAclEntry.UserOrGroupId))
                            {
                                m_log.Warning(
                                    string.Format(
                                        "Can't resolve Id {0} to new Id. New ACL entry skipped. Permission: {1}",
                                        oldAclEntry.UserOrGroupId,
                                        ObjectPermissions.FindDescription(oldAclEntry.PermissionId).Name));
                            }
                            continue;
                        }
                        if (oldAclEntry.UserOrGroupId != newUserOrGroupId)
                        {
                            acl.Remove(oldAclEntry.UserOrGroupId, oldAclEntry.PermissionId);
                            acl.Add(newUserOrGroupId, oldAclEntry.PermissionId, oldAclEntry.Granted);
                            newAclHasChanges = true;
                        }
                    }
                    if (newAclHasChanges)
                    {
                        role.Save();
                    }
                }

                #endregion

                //Fix QisRepositoryDefinitionDb via public API
                m_log.Info("Processing repository permissions and role maps.");
                foreach (IRepository repository in repositoryList)
                {
                    #region Repository permissions

                    IRepositoryAcl acl = repository.Acl;
                    var oldAclEntries = acl.ToList();
                    bool repositoryAclHasChanges = false;
                    foreach (IRepositoryAclEntry oldAclEntry in oldAclEntries)
                    {
                        Guid newUserOrGroupId;
                        if (!oldInternalToNewInternalId.TryGetValue(oldAclEntry.UserOrGroupId, out newUserOrGroupId))
                        {
                            if (!localIds.Contains(oldAclEntry.UserOrGroupId)
                                && !adIds.Contains(oldAclEntry.UserOrGroupId))
                            {
                                m_log.Warning(
                                    string.Format(
                                        "Can't resolve Id {0} to new Id. Repository ACL entry skipped. Permission: {1}",
                                        oldAclEntry.UserOrGroupId,
                                        RepositoryPermissions.FindDescription(oldAclEntry.PermissionId).Name));
                            }
                            continue;
                        }
                        if (oldAclEntry.UserOrGroupId != newUserOrGroupId)
                        {
                            acl.Remove(oldAclEntry.UserOrGroupId, oldAclEntry.PermissionId);
                            acl.Add(newUserOrGroupId, oldAclEntry.PermissionId, oldAclEntry.Granted);
                            repositoryAclHasChanges = true;
                        }
                    }
                    if (repositoryAclHasChanges)
                    {
                        acl.Save();
                    }

                    #endregion

                    #region Role map

                    IRepositoryRoleMap roleMap = repository.RepositoryRoleMap;
                    var oldRoleEntries = roleMap.ToList();
                    bool roleMapHasChanges = false;
                    foreach (IRepositoryRoleMapEntry oldRoleEntry in oldRoleEntries)
                    {
                        Guid newUserOrGroupId;
                        if (!oldInternalToNewInternalId.TryGetValue(oldRoleEntry.UserOrGroupId, out newUserOrGroupId))
                        {
                            if (!localIds.Contains(oldRoleEntry.UserOrGroupId)
                                && !adIds.Contains(oldRoleEntry.UserOrGroupId))
                            {
                                m_log.Warning(
                                    string.Format(
                                        "Can't resolve Id {0} to new Id. Role entry skipped. Role: {1}",
                                        oldRoleEntry.UserOrGroupId,
                                        oldRoleEntry.RoleId));
                            }
                            continue;
                        }
                        if (oldRoleEntry.UserOrGroupId != newUserOrGroupId)
                        {
                            roleMap.Remove(oldRoleEntry.UserOrGroupId, oldRoleEntry.RoleId);
                            roleMap.Create(
                                newUserOrGroupId,
                                oldRoleEntry.RoleId,
                                oldRoleEntry.ConfigurationAccess);
                            roleMapHasChanges = true;
                        }
                    }
                    if (roleMapHasChanges)
                    {
                        roleMap.Save();
                    }

                    #endregion
                }

                m_log.Info("Processing object permissions and owners for all repositories.");
                //For each entry from repIdToDataStorageReference fix 
                //    a) AclEntries table RoleHolderId field
                //    b) Objects table Owner, OwnerGroup field (sel distinct, update Ids)
                foreach (KeyValuePair<RepId, DataStorageReference> pair in repIdToDataStorageReference
                    .Where(p => p.Value != null))
                {
                    m_log.Info(string.Format("Processing started for repository '{0}'", pair.Key));

                    using (SqlConnection connection = new SqlConnection(pair.Value.ConnectionString))
                    {
                        connection.Open();
                        using (SqlTransaction transaction = connection.BeginTransaction())
                        {
                            #region Fix AclEntries table

                            #region Select all existing holders

                            List<Guid> existingRoleHolders = new List<Guid>();
                            using (SqlCommand command = connection.CreateCommand())
                            {
                                command.Transaction = transaction;
                                command.CommandTimeout = c_sqlCommandTimeout;
                                command.CommandText = string.Format(
                                    "select distinct [RoleHolderId] from [{0}].[AclEntries]",
                                    pair.Value.SchemaName);
                                using (IDataReader reader = command.ExecuteReader())
                                {
                                    while (reader.Read())
                                    {
                                        existingRoleHolders.Add(reader.GetGuid(0));
                                    }
                                }
                            }

                            #endregion

                            #region Create update query for holders who needs update

                            StringBuilder updateRoleHolderQuery = new StringBuilder();
                            foreach (Guid existingRoleHolder in existingRoleHolders)
                            {
                                Guid newUserOrGroupId;
                                if (!oldInternalToNewInternalId.TryGetValue(existingRoleHolder, out newUserOrGroupId))
                                {
                                    if (!localIds.Contains(existingRoleHolder) && !adIds.Contains(existingRoleHolder))
                                    {
                                        m_log.Warning(
                                            string.Format(
                                                "Can't resolve Id {0} to new Id. Record skipped. "
                                                    + "RoleHolderId in repository: {1}",
                                                existingRoleHolder,
                                                pair.Key));
                                    }
                                    continue;
                                }
                                if (existingRoleHolder == newUserOrGroupId)
                                {
                                    continue;
                                }
                                updateRoleHolderQuery.AppendLine();
                                updateRoleHolderQuery.AppendFormat("UPDATE [{0}].[AclEntries]", pair.Value.SchemaName);
                                updateRoleHolderQuery.AppendLine();
                                updateRoleHolderQuery.AppendFormat(
                                    "SET [RoleHolderId] = N'{0}'",
                                    newUserOrGroupId.ToString("D"));
                                updateRoleHolderQuery.AppendLine();
                                updateRoleHolderQuery.AppendFormat(
                                    "WHERE ([RoleHolderId] = N'{0}')",
                                    existingRoleHolder.ToString("D"));
                            }

                            #endregion

                            #region Execute update query

                            if (updateRoleHolderQuery.Length > 0)
                            {
                                using (SqlCommand command = connection.CreateCommand())
                                {
                                    command.Transaction = transaction;
                                    command.CommandTimeout = c_sqlCommandTimeout;
                                    command.CommandText = updateRoleHolderQuery.ToString();
                                    command.ExecuteScalar();
                                }
                            }

                            #endregion

                            #endregion

                            #region Fix Objects table

                            //    b) Objects table Owner, OwnerGroup field (sel distinct, update Ids)
                            RepId currentRepId = pair.Key;
                            string currentSchemaName = pair.Value.SchemaName;

                            Action<string> updateField =
                                fieldName =>
                                {
                                    #region Select all existing Owners

                                    List<string> existingOwners = new List<string>();
                                    using (SqlCommand command = connection.CreateCommand())
                                    {
                                        command.Transaction = transaction;
                                        command.CommandTimeout = c_sqlCommandTimeout;
                                        command.CommandText = string.Format(
                                            "select distinct [{1}] from [{0}].[Objects]",
                                            currentSchemaName,
                                            fieldName);
                                        using (IDataReader reader = command.ExecuteReader())
                                        {
                                            while (reader.Read())
                                            {
                                                existingOwners.Add(reader.GetString(0));
                                            }
                                        }
                                    }

                                    #endregion

                                    #region Create update query for holders who needs update

                                    StringBuilder updateOwnerQuery = new StringBuilder();
                                    foreach (string existingOwnerString in existingOwners)
                                    {
                                        OwnerInfo existingOwner = OwnerInfo.Deserialize(existingOwnerString);
                                        Guid newUserOrGroupId;
                                        if (!oldInternalToNewInternalId.TryGetValue(
                                            existingOwner.Id,
                                            out newUserOrGroupId))
                                        {
                                            if (adIds.Contains(existingOwner.Id))
                                            {
                                                newUserOrGroupId = existingOwner.Id;
                                            }
                                            else
                                            {
                                                if (!localIds.Contains(existingOwner.Id))
                                                {
                                                    m_log.Warning(
                                                        string.Format(
                                                            "Can't resolve Id {0} to new Id. Record skipped. "
                                                                + "{2} in repository: {1}",
                                                            OwnerInfo.Serialize(existingOwner),
                                                            currentRepId,
                                                            fieldName));
                                                }
                                                continue;
                                            }
                                        }
                                        string newOwnerLogin = existingOwner.Name;
                                        IRoleHolder newHolder;
                                        if (CachedQefRoleHolders.TryGetValue(newUserOrGroupId, out newHolder))
                                        {
                                            newOwnerLogin = newHolder.GetName();
                                        }
                                        string newOwnerString = OwnerInfo.Serialize(
                                            new OwnerInfo(newUserOrGroupId, newOwnerLogin));
                                        if (existingOwnerString == newOwnerString)
                                        {
                                            continue;
                                        }
                                        updateOwnerQuery.AppendLine();
                                        updateOwnerQuery.AppendFormat("UPDATE [{0}].[Objects]", currentSchemaName);
                                        updateOwnerQuery.AppendLine();
                                        updateOwnerQuery.AppendFormat(
                                            "SET [{1}] = N'{0}'",
                                            newOwnerString,
                                            fieldName);
                                        updateOwnerQuery.AppendLine();
                                        updateOwnerQuery.AppendFormat(
                                            "WHERE ([{1}] = N'{0}')",
                                            existingOwnerString,
                                            fieldName);
                                    }

                                    #endregion

                                    #region Execute update query

                                    if (updateOwnerQuery.Length > 0)
                                    {
                                        using (SqlCommand command = connection.CreateCommand())
                                        {
                                            command.Transaction = transaction;
                                            command.CommandTimeout = c_sqlCommandTimeout;
                                            command.CommandText = updateOwnerQuery.ToString();
                                            command.ExecuteScalar();
                                        }
                                    }

                                    #endregion
                                };

                            updateField("Owner");

                            updateField("OwnerGroup");

                            #endregion

                            transaction.Commit();
                        }
                    }

                    m_log.Info(string.Format("Processing completed for repository '{0}'", pair.Key));
                }
            }
            finally
            {
                //Restore all
                m_log.Info("Restoring data storage references and bringing repositories online.");
                repositoryList.DoForEach(
                    r =>
                    {
                        DataStorageReference storage;
                        if (repIdToDataStorageReference.TryGetValue(r.Id, out storage))
                        {
                            var data = r.GetData();
                            data.DataStorageReference = storage;
                            r.SetData(data);
                        }
                        if (onlineRepositories.Contains(r.Id))
                        {
                            r.BringOnline();
                        }
                    });
            }

            return oldInternalToNewInternalId;
        }

        private void VerifyEnvironment(int start, int stop)
        {
            m_log.Info("Verifying environment.");
            IUser currentUser = m_userStorageService.FindUser(m_qisServer.CurrentSession.UserId);

            Func<int, bool> inRange = value => start <= value && value <= stop;

            if (inRange(3))
            {
                var currentQisMsSqlVersion = DBProviderFactory
                    .Create(
                        new DataStorageReference(
                            QisConst.QisMSSqlProviderName,
                            DBTransaction.InitialCatalog + "=someDB",
                            "qds_dbo"),
                        -1)
                        .RequiredDataStorageVersion;

                //QisConst.DBver == c_requiredDbVersion
                if (currentQisMsSqlVersion != c_requiredDbVersion)
                {
                    string message = string.Format(
                        "Database version should be {0}. Current version is {1}",
                        c_requiredDbVersion,
                        currentQisMsSqlVersion);
                    m_log.Error(message);
                    throw new InvalidOperationException(message);
                }
            }

            if (inRange(2))
            {
                //current user - local
                if (!currentUser.IsLocal)
                {
                    string message = string.Format(
                        "Current user '{0}' is not local. Only local users are allowed for this operation. "
                            + "Use predefined user 'Admin' or similar.",
                        currentUser.Login);
                    m_log.Error(message);
                    throw new InvalidOperationException(message);
                }
            }

            if (inRange(3))
            {
                //current user - QIS admin, UserAndGroupAdmin, LicenseAdmin
                string message = null;
                var effectiveRoles = m_userStorageService.GetEffectiveRoles(m_qisServer.CurrentSession.UserId);
                if (!effectiveRoles.Any(r => r.Id == QisConst.QisAdminRoleId))
                {
                    message = string.Format(
                        "QIS Administrator role should be assigned to current user '{0}'.",
                        currentUser.Login);
                }
                if (!effectiveRoles.Any(r => r.Id == QefRoles.UserAndGroupAdministrator))
                {
                    if (!string.IsNullOrEmpty(message))
                    {
                        message += "\n";
                    }

                    message += string.Format(
                        "QEF User And Group Administrator role should be assigned to current user '{0}'.",
                        currentUser.Login);
                }
                if (!effectiveRoles.Any(r => r.Id == QefRoles.LicenseAdministrator))
                {
                    if (!string.IsNullOrEmpty(message))
                    {
                        message += "\n";
                    }

                    message += string.Format(
                        "QEF License Administrator role should be assigned to current user '{0}'.",
                        currentUser.Login);
                }

                if (!string.IsNullOrEmpty(message))
                {
                    m_log.Error(message);
                    throw new InvalidOperationException(message);
                }
            }

            if (inRange(2))
            {
                //current user - QEF user admin
                if (!m_userStorageService
                    .GetEffectiveRoles(m_qisServer.CurrentSession.UserId)
                    .Any(r => r.Id == QefRoles.UserAndGroupAdministrator))
                {
                    string message = string.Format(
                        "QEF UserAndGroupAdministrator role should be assigned to current user '{0}'.",
                        currentUser.Login);
                    m_log.Error(message);
                    throw new InvalidOperationException(message);
                }
            }

            if (inRange(3))
            {
                //No users connected to repositories
                //In case of connected remote repositories, the number of connected users is the same as
                //the number of connected remote repositories
                var connectedUsers = m_qisServer.GetConnectedUsers();
                if (connectedUsers
                    .Where(
                        item =>
                            item.UserId != m_qisServer.CurrentSession.UserId &&
                                item.SessionId != m_qisServer.CurrentSession.Id)
                    .Any())
                {
                    StringBuilder message = new StringBuilder();
                    message.AppendFormat(
                        "No users should be connected to QIS except current. {0} connections present now.",
                        connectedUsers.Count);
                    message.Append(Environment.NewLine);
                    message.Append("Connected users: ");
                    foreach (var connectedUser in connectedUsers)
                    {
                        IUser user = m_userStorageService.FindUser(connectedUser.UserId);

                        message.Append(Environment.NewLine);
                        message.AppendFormat(
                            "ID: {0}, Login={1}, FullName={2}, SessionId={3}, RepId={4}, Configurations={5}, Language={6}, RoleId={7}",
                            user.Id,
                            user.Login,
                            user.FullName,
                            connectedUser.SessionId,
                            connectedUser.RepositoryId,
                            connectedUser.Language,
                            connectedUser.RepositoryRoleId,
                            string.Join(";", connectedUser.Configurations.Select(item => item.Value)));
                    }

                    m_log.Error(message.ToString());
                    throw new InvalidOperationException(message.ToString());
                }
            }

            if (inRange(2))
            {
                //AD sync started
                if (!m_userStorageService.IsADSynchronizationStarted)
                {
                    string message = "AD synchronization must be active.";
                    m_log.Error(message);
                    throw new InvalidOperationException(message);
                }
            }

            //AD user/groups exists
            if (!m_userStorageService.HasADUsersOrGroups)
            {
                string message = "No AD users found.";
                m_log.Error(message);
                throw new InvalidOperationException(message);
            }

            if (inRange(3))
            {
                var repositoryList = GetAllRepositoriesAndMetamodels();
                var invalidVersions = repositoryList
                    .Where(r => r.GetDataStorageState().Status != DataStorageStatus.UpToDate);
                if (invalidVersions.Any())
                {
                    invalidVersions.DoForEach(
                        r => m_log.Error(
                            string.Format(
                                "Invalid data storage state. RepId: '{1}', {2}",
                                r.Id,
                                r.GetDataStorageState())));
                    string message = "Some of repositories have unsupported DB version.";
                    m_log.Error(message);
                    throw new InvalidOperationException(message + " See QEF log for details.");
                }
            }

            if (inRange(3) && !inRange(1))
            {
                //File with old map should be present
                if (!File.Exists(m_filePath))
                {
                    string message = string.Format(
                        "File with old user/group Ids and QEF assignments does not exist. ({0})",
                        m_filePath);
                    m_log.Error(message);
                    throw new InvalidOperationException(message);
                }
            }
        }

        #endregion

        #region Public Methods

        public static void RunAll(IQisServer qisServer, string filePath, string logPath)
        {
            ReassignPermissions rp = new ReassignPermissions(qisServer, filePath, logPath);
            rp.Run(1, 3);
        }

        public static void RunSingleStage(IQisServer qisServer, string filePath, int stage, string logPath)
        {
            ReassignPermissions rp = new ReassignPermissions(qisServer, filePath, logPath);
            rp.Run(stage, stage);
        }

        public void Run(int start, int stop)
        {
            #region Argument Check

            if (1 > start || start > 3)
            {
                throw new ArgumentOutOfRangeException("start", start, "Value must be in range: [1..3]");
            }

            if (1 > stop || stop > 3)
            {
                throw new ArgumentOutOfRangeException("stop", stop, "Value must be in range: [1..3]");
            }

            if (start > stop)
            {
                throw new ArgumentException("Stop value must not be less than start value.", "stop");
            }

            #endregion

            Func<int, bool> inRange = value => start <= value && value <= stop;

            m_log.Info("Reassigning QIS permissions and QEF assignments. Started.");
            VerifyEnvironment(start, stop);

            QefAssignments oldQefAssignments = null;

            if (inRange(1))
            {
                m_log.Info("Stage 1.1: reading existing assignments and user data from QEF. Started.");
                oldQefAssignments = GetQefAssignments();
                m_log.Info("Stage 1.1: reading existing assignments and user data from QEF. Completed");

                m_log.Info(
                    string.Format(
                        "Stage 1.2: writing existing assignments and user data to file '{0}'. Started",
                        m_filePath));
                SaveQefAssignments(oldQefAssignments, m_filePath);
                m_log.Info(
                    string.Format(
                        "Stage 1.2: writing existing assignments and user data to file '{0}'. Completed",
                        m_filePath));
            }
            if (inRange(2))
            {
                m_log.Info("Stage 2: AD resynchronization. Started.");
                ResyncWithActiveDirectory();
                m_log.Info("Stage 2: AD resynchronization. Completed.");
            }
            if (inRange(3))
            {
                m_log.Info(
                    "Stage 3.1: reading old QEF assignments (roles, membership, licenses) and user settings. Started.");
                if (oldQefAssignments == null)
                {
                    m_log.Info(
                        string.Format(
                            "Reading old QEF assignments (roles, membership, licenses) and user settings from file '{0}'.",
                            m_filePath));
                    oldQefAssignments = LoadQefAssignments(m_filePath);
                }
                m_log.Info(
                    "Stage 3.1: reading old QEF assignments (roles, membership, licenses) and user settings. Completed.");

                m_log.Info(
                    "Stage 3.2: restoring QEF assignments (roles, membership, licenses) and user settings. Started.");
                RestoreAssignments(oldQefAssignments);
                m_log.Info(
                    "Stage 3.2: restoring QEF assignments (roles, membership, licenses) and user settings. Completed.");
            }
            m_log.Info("Reassigning QIS permissions and QEF assignments. Completed.");
        }

        #endregion
    }

    public static class ReassignPermissionsHandlers
    {
        #region Private Methods

        private static string GetFilePathFromDialog(ObjectDialogEventArgs args)
        {
            return args.Dialog.AttributeControls["FilePath"].Value.ToString();
        }

        private static string GetLogPathFromDialog(ObjectDialogEventArgs args)
        {
            return args.Dialog.AttributeControls["LogPath"].Value.ToString();
        }

        #endregion

        #region Public Methods

        public static void RunAllDialogHandler(ObjectDialogEventArgs args)
        {
            var configuration = args.Object.GetConfiguration();
            RepId repId = configuration.GetRepository().Id;
            Cid cid = configuration.Id;
            RepRoleId roleId = args.RoleId;

            ReassignPermissions.RunAll(args.QisServer, GetFilePathFromDialog(args), GetLogPathFromDialog(args));

            IRepository rep = args.QisServer.FindRepository(repId);
            rep.Connect(roleId);
            rep.FindConfiguration(cid).Connect();
        }

        public static void RunAllGeneralHandler(GeneralScriptArgs args)
        {
            RepId repId = args.Configuration.GetRepository().Id;
            Cid cid = args.Configuration.Id;
            RepRoleId roleId = args.RoleId;

            ReassignPermissions.RunAll(args.Qis, args.Parameters[0], args.Parameters[1]);

            IRepository rep = args.Qis.FindRepository(repId);
            rep.Connect(roleId);
            rep.FindConfiguration(cid).Connect();
        }

        public static void RunSingleStageDialogHandler(ObjectDialogEventArgs args)
        {
            var configuration = args.Object.GetConfiguration();
            RepId repId = configuration.GetRepository().Id;
            Cid cid = configuration.Id;
            RepRoleId roleId = args.RoleId;

            ReassignPermissions.RunSingleStage(
                args.QisServer,
                GetFilePathFromDialog(args),
                int.Parse(args.SenderId.Replace("stage", string.Empty)),
                GetLogPathFromDialog(args));

            IRepository rep = args.QisServer.FindRepository(repId);
            rep.Connect(roleId);
            rep.FindConfiguration(cid).Connect();
        }

        public static void RunSingleStageGeneralHandler(GeneralScriptArgs args)
        {
            RepId repId = args.Configuration.GetRepository().Id;
            Cid cid = args.Configuration.Id;
            RepRoleId roleId = args.RoleId;

            ReassignPermissions.RunSingleStage(
                args.Qis,
                args.Parameters[0],
                int.Parse(args.Parameters[1]),
                args.Parameters[2]);

            IRepository rep = args.Qis.FindRepository(repId);
            rep.Connect(roleId);
            rep.FindConfiguration(cid).Connect();
        }

        #endregion
    }
}
