﻿using System;
using System.Collections.Generic;
using System.Linq;
using QCommon.Extensions;
using Qis.Common;
using Qis.Common.AttributeValues;
using Qis.Common.Qsql;
using Qis.Common.Qsql.Operations;
using Qis.Common.Qsql.Operations.Logical;
using Qis.Common.Qsql.SelectQuery;
using Qis.Web.Forms.Common.ObjectDialog;
using Qis.Web.Forms.Common.Scripting;

namespace Qis.Module.Scripts
{
    /// <summary>
    ///     Provides methods relevant to Document Management System (DMS) functionality.
    /// </summary>
    public static class DMS
    {
        #region Private Methods

        private static void ClearDocumentApproveAttributes(RepositoryObject obj)
        {
            ClearDocumentApproveAttributes(obj, CMS.c_draftApprovalState);
        }

        private static void ClearDocumentApproveAttributes(RepositoryObject obj, string approvalState)
        {
            ObjectScripts.SetAttributeValue(obj, Metamodel.Attributes.ApprovalState, new PlainText(approvalState));
            ObjectScripts.ClearAttributeValue(obj, Metamodel.Attributes.ApprovalDate);
            ObjectScripts.ClearAttributeValue(obj, Metamodel.Attributes.ApprovalChangedBy);
        }

        private static void DoCheckInDocument(
            RepositoryObject obj,
            ControlMap controlStates)
        {
            #region Argument Check

            if (obj == null)
            {
                throw new ArgumentNullException("obj");
            }

            #endregion

            ObjectReserveResult result = obj.RemoveReservation();

            if (result.Type != ObjectReserveResultType.ReservationRemoved)
            {
                throw new QInvalidScriptOperationException(
                    Common.GetReserveResultMessage(
                        Messages.CannotCheckInMessage,
                        obj.Name,
                        result.Type));
            }

            if (controlStates != null && controlStates.Count() > 0)
            {
                ObjectScripts.UpdateAttributeValues(obj, controlStates);

                obj.SaveData();
            }

            ObjectScripts.FreezeObject(obj);
        }

        private static RepositoryObjectList FindSharedDocumentsLinksToTags(
            IConfiguration configuration,
            ITableSource sourceTable)
        {
            #region Argument Check

            if (configuration == null)
            {
                throw new ArgumentNullException("configuration");
            }

            #endregion

            IQuery query = new SelectExpression(
                new ColumnExpressions(new[] {new ObjectIdExpression()}),
                sourceTable,
                new WhereExpression(
                    new InOperationExpression(
                        new AttributeValueOperand(Metamodel.Attributes.SharedDocument),
                        new CommaOperationExpression(
                            Common.TrueStrings.Select(item => new ConstantOperand(item)).ToArray()))),
                null);

            return configuration.GetRepository().RunQsqlExpression(query, configuration.Id);
        }

        #endregion

        #region Public Methods

        /// <summary>
        ///     Checks in the specified document.
        /// </summary>
        /// <param name="obj">
        ///     Document to check in.
        /// </param>
        /// <exception cref="ArgumentNullException">
        ///     T<paramref name="obj" /> is null.
        /// </exception>
        /// <exception cref="QInvalidScriptOperationException">
        ///     Reservation of the document cannot be removed.
        /// </exception>
        public static void CheckInDocument(RepositoryObject obj)
        {
            DoCheckInDocument(obj, null);
        }

        /// <summary>
        ///     Checks in the specified document.
        /// </summary>
        /// <param name="obj">
        ///     Document to check in.
        /// </param>
        /// <param name="controlStates">
        ///     Controls with new values for the attributes.
        /// </param>
        /// <exception cref="ArgumentNullException">
        ///     T<paramref name="obj" /> is null.
        /// </exception>
        /// <exception cref="QInvalidScriptOperationException">
        ///     Reservation of the document cannot be removed.
        /// </exception>
        public static void CheckInDocument(RepositoryObject obj, ControlMap controlStates)
        {
            #region Argument Check

            if (controlStates == null)
            {
                throw new ArgumentNullException("controlStates");
            }

            #endregion

            DoCheckInDocument(obj, controlStates);
        }

        /// <summary>
        ///     Checks out the specified document.
        /// </summary>
        /// <param name="obj">
        ///     Document to check out.
        /// </param>
        /// <returns>
        ///     Checked out document.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        ///     <paramref name="obj" /> is null.
        /// </exception>
        /// <exception cref="QInvalidScriptOperationException">
        ///     Document already reserved or locked.<br />
        ///     -or-<br />
        ///     Document cannot be reserved.
        /// </exception>
        public static RepositoryObject CheckOutDocument(RepositoryObject obj)
        {
            return CheckOutDocument(obj, CMS.c_draftApprovalState);
        }

        /// <summary>
        ///     Checks out the specified document.
        /// </summary>
        /// <param name="obj">
        ///     Document to check out.
        /// </param>
        /// <param name="approvalState">
        ///     Value for the <see cref="Metamodel.Attributes.ApprovalState" /> attribute.
        /// </param>
        /// <returns>
        ///     Checked out document.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        ///     <paramref name="obj" /> is null.
        /// </exception>
        /// <exception cref="QInvalidScriptOperationException">
        ///     Document already reserved or locked.<br />
        ///     -or-<br />
        ///     Document cannot be reserved.
        /// </exception>
        public static RepositoryObject CheckOutDocument(RepositoryObject obj, string approvalState)
        {
            return CheckOutDocument(obj, null, approvalState);
        }

        /// <summary>
        ///     Checks out the specified document.
        /// </summary>
        /// <param name="obj">
        ///     Document to check out.
        /// </param>
        /// <param name="userId">
        ///     The identifier of a user to reserver the object for, or <b>null</b> if object is to be reserved for
        ///     the current user.
        /// </param>
        /// <returns>
        ///     Checked out document.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        ///     <paramref name="obj" /> is null.
        /// </exception>
        /// <exception cref="QInvalidScriptOperationException">
        ///     Document already reserved or locked.<br />
        ///     -or-<br />
        ///     Document cannot be reserved.
        /// </exception>
        public static RepositoryObject CheckOutDocument(RepositoryObject obj, Guid? userId)
        {
            return CheckOutDocument(obj, userId, CMS.c_draftApprovalState);
        }

        /// <summary>
        ///     Checks out the specified document.
        /// </summary>
        /// <param name="obj">
        ///     Document to check out.
        /// </param>
        /// <param name="userId">
        ///     The identifier of a user to reserver the object for, or <b>null</b> if object is to be reserved for
        ///     the current user.
        /// </param>
        /// <param name="approvalState">
        ///     Value for the <see cref="Metamodel.Attributes.ApprovalState" /> attribute.
        /// </param>
        /// <returns>
        ///     Checked out document.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        ///     <paramref name="obj" /> is null.
        /// </exception>
        /// <exception cref="QInvalidScriptOperationException">
        ///     Document already reserved or locked.<br />
        ///     -or-<br />
        ///     Document cannot be reserved.
        /// </exception>
        public static RepositoryObject CheckOutDocument(
            RepositoryObject obj,
            Guid? userId,
            string approvalState)
        {
            #region Argument Check

            if (obj == null)
            {
                throw new ArgumentNullException("obj");
            }

            #endregion

            ObjectReserveResult status = obj.GetReserveStatus();

            if (status.Type == ObjectReserveResultType.AlreadyReserved ||
                status.Type == ObjectReserveResultType.AlreadyLocked)
            {
                throw new QInvalidScriptOperationException(
                    Common.GetReserveResultMessage(
                        Messages.CannotCheckOutMessage,
                        obj.Name,
                        status.Type));
            }

            RepositoryObject objectRevision = obj;

            if (objectRevision.IsFrozen)
            {
                objectRevision = ObjectScripts.CreateRevision(objectRevision);

                // Clear Approve attributes.
                ClearDocumentApproveAttributes(objectRevision, approvalState);

                objectRevision.SaveData();

                status = objectRevision.GetReserveStatus();

                // Object may be reserved when created.
                if (status.Type == ObjectReserveResultType.Reserved)
                {
                    return objectRevision;
                }
            }
            else
            {
                // Clear Approve attributes.
                ClearDocumentApproveAttributes(objectRevision, approvalState);

                objectRevision.SaveData();
            }

            status = userId.HasValue
                ? objectRevision.ReserveObject(userId.Value)
                : objectRevision.ReserveObject();

            if (status.Type != ObjectReserveResultType.Reserved)
            {
                throw new QInvalidScriptOperationException(
                    Common.GetReserveResultMessage(
                        Messages.CannotCheckOutMessage,
                        objectRevision.Name,
                        status.Type));
            }

            return objectRevision;
        }

        /// <summary>
        ///     Finds checked out documents.
        /// </summary>
        /// <param name="configuration">
        ///     Configuration, in which objects are searched.
        /// </param>
        /// <returns>
        ///     List of checked out documents.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        ///     <paramref name="configuration" /> is null.
        /// </exception>
        public static RepositoryObjectList FindCheckedOutDocuments(IConfiguration configuration)
        {
            if (configuration == null)
            {
                throw new ArgumentNullException("configuration");
            }

            RepositoryObjectList objects = new RepositoryObjectList();

            foreach (RepositoryObject obj in
                configuration.FindObjects(Metamodel.Templates.ExternalDocument))
            {
                ObjectReserveResult result = obj.GetReserveStatus();

                if (result.Type == ObjectReserveResultType.Reserved)
                {
                    objects.Add(obj);
                }
            }

            return objects;
        }

        /// <summary>
        ///     Finds documents, which link to the specified tags, modified during the specified last days.
        /// </summary>
        /// <param name="configuration">
        ///     Configuration, in which objects are searched.
        /// </param>
        /// <param name="tags">
        ///     Tags, to which documents are linked.
        /// </param>
        /// <param name="lastDays">
        ///     Number of days, during which documents were modified.
        /// </param>
        /// <returns>
        ///     List of documents.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        ///     <paramref name="configuration" /> is null.<br />
        ///     -or-<br />
        ///     <paramref name="tags" /> is null.
        /// </exception>
        public static RepositoryObjectList FindLatestDocumentsLinksToTags(
            IConfiguration configuration,
            QHyperLinkList tags,
            int lastDays)
        {
            #region Argument Check

            if (configuration == null)
            {
                throw new ArgumentNullException("configuration");
            }

            if (tags == null)
            {
                throw new ArgumentNullException("tags");
            }

            #endregion

            if (tags.Count == 0)
            {
                return null;
            }

            RepositoryObjectList documents = new RepositoryObjectList();

            foreach (var tag in tags)
            {
                var list = ObjectScripts.GetBackAttributeRelations(
                    configuration,
                    tag.ObjectId,
                    Metamodel.Attributes.BelongsToCategory);

                documents.AddRange(list);
            }

            IQuery query = new SelectExpression(
                new ColumnExpressions(new[] {new ObjectIdExpression()}),
                new ObjectListTable(documents),
                new WhereExpression(
                    new GreaterOrEqualOperationExpression(
                        new AttributeValueOperand(PredefinedObjectAttribute.Modified.Name),
                        new ConstantOperand(QDateTime.ToSortableString(DateTime.UtcNow.AddDays(-lastDays))))),
                null);

            return configuration.GetRepository().RunQsqlExpression(query, configuration.Id);
        }

        /// <summary>
        ///     Finds all documents, which have <see cref="Metamodel.Attributes.SharedDocument" /> attribute set to
        ///     <b>true</b>.
        /// </summary>
        /// <param name="configuration">
        ///     Configuration, in which objects are searched.
        /// </param>
        /// <returns>
        ///     List of documents.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        ///     <paramref name="configuration" /> is null.
        /// </exception>
        public static RepositoryObjectList FindSharedDocumentsLinksToTags(IConfiguration configuration)
        {
            return FindSharedDocumentsLinksToTags(
                configuration,
                new TemplateTable(Metamodel.Templates.ExternalDocument));
        }

        /// <summary>
        ///     Finds documents, which have <see cref="Metamodel.Attributes.SharedDocument" /> attribute set to
        ///     <b>true</b> and link to one of the specified tags.
        /// </summary>
        /// <param name="configuration">
        ///     Configuration, in which objects are searched.
        /// </param>
        /// <param name="tags">
        ///     Tags, to one of which documents are linked.
        /// </param>
        /// <returns>
        ///     List of documents.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        ///     <paramref name="configuration" /> is null.<br />
        ///     -or-<br />
        ///     <paramref name="tags" /> is null.
        /// </exception>
        public static RepositoryObjectList FindSharedDocumentsLinksToTags(
            IConfiguration configuration,
            IEnumerable<QHyperLink> tags)
        {
            IEnumerable<Oid> tagIds = tags.Select(item => item.ObjectId);

            return FindSharedDocumentsLinksToTags(configuration, tagIds);
        }

        /// <summary>
        ///     Finds documents, which have <see cref="Metamodel.Attributes.SharedDocument" /> attribute set to
        ///     <b>true</b> and link to one of the specified tags.
        /// </summary>
        /// <param name="configuration">
        ///     Configuration, in which objects are searched.
        /// </param>
        /// <param name="tagIds">
        ///     Identifier of tags, to one of which documents are linked.
        /// </param>
        /// <returns>
        ///     List of documents.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        ///     <paramref name="configuration" /> is null.<br />
        ///     -or-<br />
        ///     <paramref name="tagIds" /> is null.
        /// </exception>
        public static RepositoryObjectList FindSharedDocumentsLinksToTags(
            IConfiguration configuration,
            IEnumerable<Oid> tagIds)
        {
            #region Argument Check

            if (configuration == null)
            {
                throw new ArgumentNullException("configuration");
            }

            if (tagIds == null)
            {
                throw new ArgumentNullException("tagIds");
            }

            #endregion

            if (tagIds.Count() == 0)
            {
                return null;
            }

            RepositoryObjectList documents = new RepositoryObjectList();

            foreach (var tagId in tagIds)
            {
                var list = ObjectScripts.GetBackAttributeRelations(
                    configuration,
                    tagId,
                    Metamodel.Attributes.BelongsToCategory);

                documents.AddRange(list.Where(item => item.Template == Metamodel.Templates.ExternalDocument));
            }

            return FindSharedDocumentsLinksToTags(
                configuration,
                new ObjectListTable(documents));
        }

        /// <summary>
        ///     Finds documents, which have <see cref="Metamodel.Attributes.SharedDocument" /> attribute set to
        ///     <b>true</b> and link to one of the specified tags.
        /// </summary>
        /// <param name="configuration">
        ///     Configuration, in which objects are searched.
        /// </param>
        /// <param name="tagNames">
        ///     Name of tags, to one of which documents are linked.
        /// </param>
        /// <returns>
        ///     List of documents.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        ///     <paramref name="configuration" /> is null.
        /// </exception>
        public static RepositoryObjectList FindSharedDocumentsLinksToTags(
            IConfiguration configuration,
            IEnumerable<string> tagNames)
        {
            #region Argument Check

            if (configuration == null)
            {
                throw new ArgumentNullException("configuration");
            }

            #endregion

            if (tagNames.Count() <= 0)
            {
                return null;
            }

            // Find tag objects.
            IQuery query = new SelectExpression(
                new ColumnExpressions(new[] {new ObjectIdExpression()}),
                new TemplateTable(Metamodel.Templates.Category),
                new WhereExpression(
                    new InOperationExpression(
                        new AttributeValueOperand(PredefinedObjectAttribute.Name.Name),
                        new CommaOperationExpression(
                            tagNames.Select(item => new ConstantOperand(item)).ToArray()))),
                null);

            RepositoryObjectList tagObjects = configuration.GetRepository().RunQsqlExpression(
                query,
                configuration.Id);

            // Get identifiers of tag objects.
            IEnumerable<Oid> tagIds = tagObjects.Select(item => item.Id);

            return FindSharedDocumentsLinksToTags(configuration, tagIds);
        }

        /// <summary>
        ///     Reverts check-in of the specified document event though it was checked out by other user.
        /// </summary>
        /// <param name="obj">
        ///     Document to revert check-in.
        /// </param>
        /// <exception cref="ArgumentNullException">
        ///     <paramref name="obj" /> is null.
        /// </exception>
        /// <exception cref="QInvalidScriptOperationException">
        ///     Reservation of the document cannot be removed.
        /// </exception>
        public static void ForceUndoCheckOutDocument(RepositoryObject obj)
        {
            if (obj == null)
            {
                throw new ArgumentNullException("obj");
            }

            IConfiguration configuration = obj.GetConfiguration();

            if (configuration == null)
            {
                throw new ArgumentException("Cannot get configuration.", "obj");
            }

            IRepository repository = configuration.GetRepository();

            if (repository == null)
            {
                throw new ArgumentException("Cannot get repository.", "obj");
            }

            ObjectReserveResult result = obj.GetReserveStatus();

            result = repository.RemoveReservation(obj.Id.Id, result.UserId);
            if (result.Type != ObjectReserveResultType.ReservationRemoved)
            {
                throw new QInvalidScriptOperationException(
                    Common.GetReserveResultMessage(
                        Messages.CannotUndoCheckOutMessage,
                        obj.Name,
                        result.Type));
            }
        }

        /// <summary>
        ///     Reverts check-in of the specified document only if it was checked out by the same user.
        /// </summary>
        /// <param name="obj">
        ///     Document to revert check-in.
        /// </param>
        /// <exception cref="ArgumentNullException">
        ///     <paramref name="obj" /> is null.
        /// </exception>
        /// <exception cref="QInvalidScriptOperationException">
        ///     Reservation of the document cannot be removed.
        /// </exception>
        public static void UndoCheckOutDocument(RepositoryObject obj)
        {
            if (obj == null)
            {
                throw new ArgumentNullException("obj");
            }

            ObjectReserveResult result = obj.RemoveReservation();

            if (result.Type != ObjectReserveResultType.ReservationRemoved)
            {
                throw new QInvalidScriptOperationException(
                    Common.GetReserveResultMessage(
                        Messages.CannotUndoCheckOutMessage,
                        obj.Name,
                        result.Type));
            }
        }

        #endregion
    }

    /// <summary>
    ///     Provides handlers relevant to Document Management System (DMS) functionality.
    /// </summary>
    public static class DMSHandlers
    {
        #region Private Methods

        private static RepositoryObjectList FindLatestDocumentsLinksToTags(FilterScriptEventArgs args, int lastDays)
        {
            const string c_idUrlParam = "Id";

            QHyperLinkList tags = new QHyperLinkList();

            args.Request[c_idUrlParam].Split(';').DoForEach(
                uriString
                    =>
                    tags.Add(new QHyperLink(new Uri(uriString), string.Empty)));

            return DMS.FindLatestDocumentsLinksToTags(args.Configuration, tags, lastDays);
        }

        private static void UpdateDocumentTitle(ObjectDialogEventArgs args)
        {
            RepositoryObject obj = args.Object;

            string title = obj.Name;

            IAttributeValue attributeValue = ObjectScripts.GetAttributeValue(obj, PredefinedObjectAttribute.LocalRevision.Name);

            if (attributeValue != null)
            {
                title += string.Format(" - Rev.: {0}", attributeValue.AsPlainText(null));
            }

            attributeValue = ObjectScripts.GetAttributeValue(obj, Metamodel.Attributes.ApprovalState);

            if (attributeValue != null)
            {
                title += string.Format(" - Status: {0}", attributeValue.AsPlainText(null));
            }

            var lockResult = obj.GetLockStatus();

            var result = obj.GetReserveStatus();

            if (args.Session.Id != lockResult.SessionId)
            {
                string userName = Common.FindUserFullName(args.QisServer, lockResult.UserId);

                if (!string.IsNullOrEmpty(userName))
                {
                    title += string.Format(" - I brug af: {0}", userName);
                }
            }
            else if (result.Type == ObjectReserveResultType.Reserved)
            {
                string userName = Common.FindUserFullName(args.QisServer, result.UserId);
                title += string.Format(" - Reserveret af: {0}", userName);
            }

            UIScripts.SetTitle(args.UIActions, title);
        }

        #endregion

        #region Public Methods

        /// <summary>
        ///     Checks is the specified document. After the document is checked in, the form is refreshed.
        /// </summary>
        /// <param name="args">
        ///     ObjectDialog event arguments.
        /// </param>
        public static void CheckInDocument(ObjectDialogEventArgs args)
        {
            try
            {
                DMS.CheckInDocument(args.Object, args.Dialog.Controls);

                UIScripts.RefreshForm(args.UIActions);
            }
            catch (QInvalidScriptOperationException invalidScriptOperationException)
            {
                args.Message = invalidScriptOperationException.Message;

                return;
            }
            catch (Exception exception)
            {
                args.Message = exception.Message + string.Format("\nStack Trace:\n{0}", exception.StackTrace);

                return;
            }
        }

        /// <summary>
        ///     Checks out the specified document. After the document is checked out, the form is refreshed.
        /// </summary>
        /// <param name="args">
        ///     ObjectDialog event arguments.
        /// </param>
        public static void CheckOutDocument(ObjectDialogEventArgs args)
        {
            try
            {
                //DMS.CheckOutDocument(args.Object, CMS.c_draftApprovalStateDK);
                DMS.CheckOutDocument(args.Object, args.CommandArgument);

                UIScripts.RefreshForm(args.UIActions);
            }
            catch (QInvalidScriptOperationException invalidScriptOperationException)
            {
                args.Message = invalidScriptOperationException.Message;

                return;
            }
            catch (Exception exception)
            {
                args.Message = exception.Message + string.Format("\nStack Trace:\n{0}", exception.StackTrace);

                return;
            }
        }

        /// <summary>
        ///     Finds checked out documents.
        /// </summary>
        /// <param name="args">
        ///     QRV event arguments.
        /// </param>
        /// <returns>
        ///     List of documents checked out objects.
        /// </returns>
        public static RepositoryObjectList FindCheckedOutDocuments(FilterScriptEventArgs args)
        {
            return DMS.FindCheckedOutDocuments(args.Configuration);
        }

        /// <summary>
        ///     Finds documents, which link to the specified tags, modified during the last day.
        /// </summary>
        /// <param name="args">
        ///     QRV event arguments.
        /// </param>
        public static RepositoryObjectList FindLatestDocumentsLinksToTags1(FilterScriptEventArgs args)
        {
            return FindLatestDocumentsLinksToTags(args, 1);
        }

        /// <summary>
        ///     Finds documents, which link to the specified tags, modified during 1000 last day.
        /// </summary>
        /// <param name="args">
        ///     QRV event arguments.
        /// </param>
        public static RepositoryObjectList FindLatestDocumentsLinksToTags1000(FilterScriptEventArgs args)
        {
            return FindLatestDocumentsLinksToTags(args, 1000);
        }

        /// <summary>
        ///     Finds documents, which link to the specified tags, modified during 7 last day.
        /// </summary>
        /// <param name="args">
        ///     QRV event arguments.
        /// </param>
        public static RepositoryObjectList FindLatestDocumentsLinksToTags7(FilterScriptEventArgs args)
        {
            return FindLatestDocumentsLinksToTags(args, 7);
        }

        /// <summary>
        ///     Reverts check-in of the specified document event though it was checked out by other user.
        ///     After check-in of the document is reverted, the form is refreshed.
        /// </summary>
        /// <param name="args">
        ///     ObjectDialog event arguments.
        /// </param>
        public static void ForceUndoCheckOutDocument(ObjectDialogEventArgs args)
        {
            try
            {
                DMS.ForceUndoCheckOutDocument(args.Object);

                UIScripts.RefreshForm(args.UIActions);
            }
            catch (QInvalidScriptOperationException invalidScriptOperationException)
            {
                args.Message = invalidScriptOperationException.Message;

                return;
            }
            catch (Exception exception)
            {
                args.Message = exception.Message + string.Format("\nStack Trace:\n{0}", exception.StackTrace);

                return;
            }
        }

        /// <summary>
        ///     On document open event handler.
        /// </summary>
        /// <param name="args">
        ///     ObjectDialog event arguments.
        /// </param>
        public static void OnDocumentOpen(ObjectDialogEventArgs args)
        {
            UpdateDocumentTitle(args);
        }

        /// <summary>
        ///     Reverts check-in of the specified document only if it was checked out by the same user.
        ///     After check-in of the document is reverted, the form is refreshed.
        /// </summary>
        /// <param name="args">
        ///     ObjectDialog event arguments.
        /// </param>
        public static void UndoCheckOutDocument(ObjectDialogEventArgs args)
        {
            try
            {
                DMS.UndoCheckOutDocument(args.Object);

                UIScripts.RefreshForm(args.UIActions);
            }
            catch (QInvalidScriptOperationException invalidScriptOperationException)
            {
                args.Message = invalidScriptOperationException.Message;

                return;
            }
            catch (Exception exception)
            {
                args.Message = exception.Message + string.Format("\nStack Trace:\n{0}", exception.StackTrace);

                return;
            }
        }

        #endregion
    }
}
