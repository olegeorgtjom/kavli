﻿using System.Linq;
using Qis.Common;

namespace Qis.Module.Scripts
{
    public static partial class Metamodel
    {
        #region Nested Types

        public static partial class Attributes
        {
            #region Constants

            public const string AlternateID = "AlternateID";
            public const string ApprovalChangedBy = "ApprovalChangedBy";
            public const string ApprovalDate = "ApprovalDate";
            public const string ApprovalState = "ApprovalState";
            public const string AssociatedWith = "AssociatedWith";
            public const string Author = "Author";
            public const string BelongsToCategory = "BelongsToCategory";
            public const string BreaksDownTo = "BreaksDownTo";
            public const string CanApprove = "CanApprove";
            public const string ChgStatus = "ChgStatus";
            public const string CirculateTo = "CirculateTo";
            public const string CirculationText = "CirculationText";
            public const string CommentTo = "CommentTo";
            public const string CountryCode = "CountryCode";
            public const string Created = "sys_Created";
            public const string CreatedBy = "sys_CreatedBy";
            public const string Description = "Description";
            public const string DocumentId = "DocumentID";
            public const string DocumentType = "DocumentType";
            public const string EMail = "EMail";
            public const string FileAttachment = "FileAttachment";
            public const string FileName = "FileName";
            public const string FocalPoint = "FocalPoint";
            public const string GMSDocumentType = "GMSDocumentType";
            public const string Group = "Group";
            public const string HasBeenAcknowledged = "HasBeenAcknowledged";
            public const string HasLeadAuditor = "HasLeadAuditor";
            public const string HasParticipant = "HasParticipant";
            public const string HasPositionHolder = "HasPositionHolder";
            public const string HasResponsible = "HasResponsible";
            public const string HasSubscriptionOwner = "HasSubscriptionOwner";
            public const string HistoryOfChanges = "HistoryOfChanges";
            public const string DescriptionOfChange = "DescriptionOfChange";
            public const string Initials = "Initials";
            public const string Keyword = "Keyword";
            public const string LegacyDocumentID = "LegacyDocumentID";
            public const string Login = "LOGIN";
            public const string Modified = "sys_Modified";
            public const string ModifiedBy = "sys_ModifiedBy";
            public const string Name = "Name";
            public const string OriginatedBy = "OriginatedBy";
            public const string OriginatedDate = "OriginatedDate";
            public const string ProcessOwner = "ProcessOwner";
            public const string ReadOnlyFileLink = "ReadOnlyFileLink";
            public const string RelatedDiscipline = "RelatedDiscipline";
            public const string RelatedFunction = "RelatedFunction";
            public const string RelatedGlobalProcess = "RelatedGlobalProcess";
            public const string RelevantToLocation = "RelevantToLocation";
            public const string RevisionLabel = "RevisionLabel";
            public const string RevisionValidFrom = "RevisionValidFrom";
            public const string RevisionValidTo = "RevisionValidTo";
            public const string Role = "Role";
            public const string SequenceID = "SequenceID";
            public const string ServerSide = "ServerSide";
            public const string SetupAction = "SetupAction";
            public const string SharedDocument = "SharedDocument";
            public const string Status = "Status";
            public const string SubscribesTo = "SubscribesTo";
            public const string Title = "Title";
            public const string ToBeAcknowledged = "ToBeAcknowledged";
            public const string UserName = "UserName";

            #endregion
        }

        public static partial class Templates
        {
            #region Constants

            public const string AcknowledgeList = "AcknowledgeList";
            public const string Activity = "Activity";
            public const string ApplicationArchitectureDiagram = "ApplicationArchitectureDiagram";
            public const string ArchitectureFrameWork = "ArchitectureFrameWork";
            public const string BusinessProcess = "BusinessProcess";
            public const string BusinessProcessDiagram = "BusinessProcessDiagram";
            public const string BusinessProcessNetwork = "BusinessProcessNetwork";
            public const string Category = "Category";
            public const string ChangeRequest = "ChangeRequest";
            public const string ClassDiagram = "ClassDiagram";
            public const string Comment = "Comment";
            public const string ComponentDiagram = "ComponentDiagram";
            public const string CorrectiveAction = "CorrectiveAction";
            public const string DataFlowDiagram = "DataFlowDiagram";
            public const string DataMappingDiagram = "DataMappingDiagram";
            public const string DataModelDiagram = "DataModelDiagram";
            public const string Document = "Document";
            public const string DocumentCategory = "DocumentCategory";
            public const string ExternalDocument = "ExternalDocument";
            public const string Feedback = "Feedback";
            public const string FreeHandDocument = "FreeHandDocument";
            public const string HealthCareCheck = "HealthCareCheck";
            public const string HealthCareDevice = "HealthCareDevice";
            public const string Incident = "Incident";
            public const string InformationSystem = "InformationSystem";
            public const string InfraStructureDiagram = "InfraStructureDiagram";
            public const string InterestGroup = "InterestGroup";
            public const string Location = "Location";
            public const string NonConformance = "NonConformance";
            public const string OrganizationDiagram = "OrganizationDiagram";
            public const string Person = "Person";
            public const string Position = "Position";
            public const string QualityAudit = "QualityAudit";
            public const string QualiWareSetup = "QualiWareSetup";
            public const string QualiWareStateDiagram = "QualiWareStateDiagram";
            public const string RegulationDiagram = "RegulationDiagram";
            public const string RepositoryDesktop = "RepositoryDesktop";
            public const string RequirementModel = "RequirementModel";
            public const string StrategicRoadmap = "StrategicRoadmap";
            public const string StrategyModel = "StrategyModel";
            public const string Subscription = "Subscription";
            public const string TreasureHunt = "TreasureHunt";
            public const string UseCaseDiagram = "UseCaseDiagram";
            public const string WorkFlowDiagram = "WorkFlowDiagram";
            public const string WorkModel = "WorkModel";

            #endregion
        }

        #endregion

        #region Public Methods

        public static string[] GetAllConnectionTemplates()
        {
            string[] connectiontemplates = new[]
            {
                "Abstraction",
                "ActivityPath",
                "Association",
                "BrowserBackRelation",
                "BrowserGraphicRelation",
                "BrowserRelation",
                "CauseEfect",
                "ComplexRelation",
                "ComponentRealization",
                "ConceptAggregation",
                "ConceptAssociation",
                "ConceptGeneralization",
                "ConceptRelation",
                "Connection",
                "Connector",
                "ControlCoverage",
                "ControlFlow",
                "Correspondence",
                "CriterionInfluence",
                "DataApply",
                "DataExtract",
                "DataFlow",
                "DataMapping",
                "DataRelation",
                "Dependency",
                "Deployment",
                "Derivation",
                "ElementImport",
                "ExceptionHandler",
                "Extend",
                "FirewallPolicy",
                "FunctionalityUsage",
                "Generalization",
                "GeneralizationSet",
                "Guide",
                "ImpactQuantity",
                "Include",
                "Index",
                "InferentialRelation",
                "Influence",
                "InformationFlow",
                "Inhabitance",
                "InheritanceConnection",
                "IntegrationFlow",
                "Interaction",
                "InterfaceRealization",
                "ITSupport",
                "LifelineChange",
                "LogisticalFlow",
                "Manifestation",
                "Message",
                "MessageFlow",
                "ModuleParameter",
                "NaryRelationship",
                "NetworkConnection",
                "NodeRelation",
                "ObjectAssociation",
                "ObjectDecomposition",
                "ObjectDependency",
                "ObjectFlow",
                "ObjectGeneralization",
                "Originator",
                "PackageContaintment",
                "PackageImport",
                "PackageMerge",
                "Performer",
                "PhysicalForeignKey",
                "PositionHolder",
                "ProfileApplication",
                "Realization",
                "Recycling",
                "RelationshipConstraint",
                "RelConstraint",
                "Resource",
                "Responsibility",
                "RoleBinding",
                "SequenceFlow",
                "StateTransition",
                "StrategicAlignment",
                "Substitution",
                "SystemDependency",
                "Transition",
                "TransportSystem",
                "TypeRelationship",
                "Usage",
                "ValueFlow",
                "Wire",
            };

            return connectiontemplates;
        }

        public static string[] GetAllHTMLTemplates()
        {
            string[] htmltemplates = new[]
            {
                "HTMLIcon",
                "HTMLMenuItem",
                "HTMLMenuLayout",
                "HTMLMenuSettings",
                "HTMLPopupItem",
                "HTMLPopupMenuLayout",
                "HTMLPopupStyle",
                "HTMLProfile",
                "HTMLPropertySheet",
                "HTMLScrollEntry",
                "HTMLScroller",
                "HTMLSearch",
                "HTMLSlideMenu",
                "HTMLStyleSheet",
                "HTMLTag",
                "QualiWareStateDiagram",
                "QualiWareState",
                "QualiWareStateTransition",
            };

            return htmltemplates;
        }

        public static string[] GetAllTemplates(IRepository repository)
        {
            return repository.Metamodel.Templates.Select(item => item.Name).ToArray();
        }

        public static string[] GetGenericSearchAttributes()
        {
            string[] genericattributes = new[]
            {
                "DocumentID",
                "LegacyDocumentID",
                "Name",
                "RelatedDiscipline",
                "RelatedFunction",
                "RelatedGlobalProcess",
                "GMSDocumentType",
                "ReadOnlyFileLink",
                "RelevantToLocation",
                "FocalPoint",
                "Author",
                "ApprovalChangedBy",
            };

            return genericattributes;
        }

        public static string[] GetGenericSearchLinks()
        {
            string[] genericattributes = {};

            return genericattributes;
        }

        #endregion
    }
}
