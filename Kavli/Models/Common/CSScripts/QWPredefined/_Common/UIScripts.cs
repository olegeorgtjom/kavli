﻿using System;
using Qef.Common.Security;
using Qis.Common;
using Qis.Web.Forms.Common.UIActions;
using Qis.Web.Forms.Common;

namespace Qis.Module.Scripts
{
    /// <summary>
    ///     Provides functionality relevant to user interface.
    /// </summary>
    public static class UIScripts
    {
        #region Public Methods

        /// <summary>
        ///     Opens a new form with in-memory object.
        /// </summary>
        /// <param name="repositoryId">
        ///     Repository identifier.
        /// </param>
        /// <param name="configurationId">
        ///     Configuration identifier.
        /// </param>
        /// <param name="template">
        ///     Object template.
        /// </param>
        /// <param name="customization">
        ///     Name of the customization to use.
        /// </param>
        /// <param name="windowsType">
        ///     Window type.
        /// </param>
        /// <param name="actions">
        ///     List of UI actions.
        /// </param>
        public static void OpenInMemoryObjectForEditing(
            RepId repositoryId,
            Cid configurationId,
            string template,
            string customization,
            WindowsType windowsType,
            UIActionList actions)
        {
            try
            {
                actions.Add(
                    new JavaScriptUIAction(
                        string.Format(
                            "openAspxLink(\"ObjectDialog.aspx?NewObjectTemplate={0}&RepId={1}&ConfId={2}{3}\", {4});",
                            template,
                            repositoryId,
                            configurationId,
                            !string.IsNullOrEmpty(customization)
                                ? string.Format("&Customization={0}", customization)
                                : string.Empty,
                            (int)windowsType),
                        false));
            }
            catch (Exception exception)
            {
                throw new Exception("Failed to open object for editing: {0}", exception);
            }
        }

        /// <summary>
        ///     Tries to open a new form with the specified object loaded.
        /// </summary>
        /// <param name="sessionId">
        ///     User session identifier.
        /// </param>
        /// <param name="obj">
        ///     Object to open.
        /// </param>
        /// <param name="customization">
        ///     Name of the customization to use.
        /// </param>
        /// <param name="webContext">
        ///     Web-specific context.
        /// </param>
        /// <param name="actions">
        ///     List of UI actions.
        /// </param>
        /// <exception cref="Exception">
        ///     Form failed to open.
        /// </exception>
        public static void OpenObjectForEditing(
            Sid sessionId,
            RepositoryObject obj,
            string customization,
            WebContext webContext,
            UIActionList actions)
        {
            try
            {
                JavaScriptUIAction action = JavaScriptUIAction.OpenObjectForEditing(
                    sessionId,
                    obj,
                    customization,
                    webContext);

                actions.Add(action);
            }
            catch (Exception exception)
            {
                throw new Exception("Failed to open object for editing: {0}", exception);
            }
        }

        /// <summary>
        ///     Tries to refresh the current form.
        /// </summary>
        /// <param name="actions">
        ///     List of UI actions.
        /// </param>
        /// <exception cref="Exception">
        ///     Form failed to refresh.
        /// </exception>
        public static void RefreshForm(UIActionList actions)
        {
            try
            {
                JavaScriptUIAction action = JavaScriptUIAction.RefreshForm();

                actions.Add(action);
            }
            catch (Exception exception)
            {
                throw new Exception("Failed to refresh the form.", exception);
            }
        }

        /// <summary>
        ///     Tries to refresh the current form with the specified object loaded.
        /// </summary>
        /// <param name="sessionId">
        ///     User session identifier.
        /// </param>
        /// <param name="obj">
        ///     Object to open.
        /// </param>
        /// <param name="customization">
        ///     Name of the customization to use.
        /// </param>
        /// <param name="actions">
        ///     List of UI actions.
        /// </param>
        /// <exception cref="Exception">
        ///     Form failed to refresh.
        /// </exception>
        public static void RefreshObjectForEditing(
            Sid sessionId,
            RepositoryObject obj,
            string customization,
            UIActionList actions)
        {
            try
            {
                JavaScriptUIAction action = JavaScriptUIAction.RefreshObjectForEditing(sessionId, obj, customization);

                actions.Add(action);
            }
            catch (Exception exception)
            {
                throw new Exception("Failed to refresh the form.", exception);
            }
        }

        /// <summary>
        ///     Tries to refresh the form in parent frame.
        /// </summary>
        /// <param name="actions">
        ///     List of UI actions.
        /// </param>
        /// <exception cref="Exception">
        ///     Form failed to refresh.
        /// </exception>
        public static void RefreshParentForm(UIActionList actions)
        {
            try
            {
                JavaScriptUIAction action = JavaScriptUIAction.RefreshParentForm();

                actions.Add(action);
            }
            catch (Exception exception)
            {
                throw new Exception("Failed to refresh the form.", exception);
            }
        }

        /// <summary>
        ///     Tries to refresh the form in top frame.
        /// </summary>
        /// <param name="actions">
        ///     List of UI actions.
        /// </param>
        /// <exception cref="Exception">
        ///     Form failed to refresh.
        /// </exception>
        public static void RefreshTopForm(UIActionList actions)
        {
            try
            {
                JavaScriptUIAction action = JavaScriptUIAction.RefreshTopForm();

                actions.Add(action);
            }
            catch (Exception exception)
            {
                throw new Exception("Failed to refresh the form.", exception);
            }
        }

        /// <summary>
        ///     Sets form title to the specified string.
        /// </summary>
        /// <param name="actions">
        ///     List of UI actions.
        /// </param>
        /// <param name="title">
        ///     New form title.
        /// </param>
        public static void SetTitle(UIActionList actions, string title)
        {
            string script = string.Format("document.title = '{0}';", title);

            actions.Add(new JavaScriptUIAction(script));
        }

        #endregion
    }
}
