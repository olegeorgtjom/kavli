﻿namespace Qis.Module.Scripts
{
    /// <summary>
    ///     Provides a collection of string messages.
    /// </summary>
    public static class Messages
    {
        #region Constants

        public const string AlreadyCheckedOutMessage = "Already checked out";
        public const string CannotApproveMessage = "Cannot approve object";
        public const string CannotCheckInMessage = "Cannot check in document";
        public const string CannotCheckOutMessage = "Cannot check out document";
        public const string CannotCirculateMessage = "Cannot circulate object";
        public const string CannotCreateRevisionMessage = "Cannot create object revision";
        public const string CannotFindUserMessage = "Cannot find user";
        public const string CannotFreezeMessage = "Cannot freeze object";
        public const string CannotGetConfigurationMessage = "Cannot get reference to configuration";
        public const string CannotGetObjectMessage = "Cannot get reference to object";
        public const string CannotSendMailMessageMessage = "Cannot send e-mail";
        public const string CannotUndoCheckOutMessage = "Cannot undo check out document";
        public const string CheckedOutByOtherUserMessage = "Checked out by other user";
        public const string InUseByOtherUserMessage = "In use by other user";
        public const string IsFrozenMessage = "Object is frozen";
        public const string MissingRecipientsMessage = "Recipients list should not be empty";
        public const string MissingSenderMessage = "Sender should not be empty";
        public const string NotAllowedToApproveContainsMessage = "Not allowed to approve contains object";
        public const string NotAllowedToApproveMessage = "Not allowed to approve object";
        public const string NotCheckedOutMessage = "Not checked out";
        public const string ReasonUnknownMessage = "Reason unknown";

        #endregion
    }
}
