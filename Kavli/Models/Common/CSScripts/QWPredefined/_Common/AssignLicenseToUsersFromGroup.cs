﻿using System;
using QCommon.Extensions;
using Qef.Common;
using Qef.Common.Licensing;
using Qef.Common.Security;
using Qef.Common.Users;
using Qis.Common;

namespace Qis.Module.Scripts
{
    public class AssignLicenseToUsersFromGroup
    {
        #region Public Methods

        /// <summary>
        ///     Assigns license to users included into the specified group.
        /// </summary>
        /// <param name="qis">
        ///     An instance of the QIS server.
        /// </param>
        /// <param name="authority">
        ///     Group authority.
        ///     Must be non empty for AD group and <see cref="QefAuthority.Local" /> for local group.
        /// </param>
        /// <param name="groupName">
        ///     Name of group, users from which should be assigned to the specified license. Domain groups must
        ///     include domain name.
        /// </param>
        /// <param name="licenseSerialNumber">
        ///     Serial number of license, which is to assign users to. Only <see cref="LicenseBindingType.PerUser" />
        ///     licenses are supported. License serial number is found in license file. License must be installed.
        /// </param>
        /// <param name="message">
        ///     Output error message if error occurs.
        /// </param>
        /// <returns>
        ///     <b>true</b> if license is successfully assigned; <b>false</b> otherwise.
        /// </returns>
        public static bool AssignLicense(
            IQisServer qis,
            QefAuthority authority,
            string groupName,
            string licenseSerialNumber,
            out string message)
        {
            #region Argument Check

            if (qis == null)
            {
                throw new ArgumentNullException("qis");
            }

            if (groupName.IsBlank())
            {
                throw new ArgumentException("Name of group cannot be null or empty.", "groupName");
            }

            if (licenseSerialNumber.IsBlank())
            {
                throw new ArgumentException("Serial number of license cannot be null or empty.", "licenseSerialNumber");
            }

            #endregion

            message = null;

            IUserStorageService userStorage = (IUserStorageService)qis.GetQefService(ServiceId.UserStorage);
            IGroup group = userStorage.FindGroup(authority, groupName);
            if (group == null)
            {
                message = string.Format(
                    "Cannot find group: Authority: {0}, Login: {1}.",
                    authority,
                    groupName);

                return false;
            }

            ILicenseStorageService licenseStorage =
                (ILicenseStorageService)qis.GetQefService(ServiceId.LicenseStorage);
            ILicenseManagementService licenseManagement =
                (ILicenseManagementService)qis.GetQefService(ServiceId.LicenseManagement);

            SerialNumber serialNumber = SerialNumber.Parse(licenseSerialNumber);
            ILicense license = licenseStorage.FindLicense(serialNumber);
            if (license == null)
            {
                message = string.Format("Cannot find license \"{0}\".", licenseSerialNumber);

                return false;
            }

            if (license.BindingType == LicenseBindingType.PerUser)
            {
                message = string.Format(
                    "Cannot assign a {0} license \"{1}\" to users. A {2} license is required.",
                    license.BindingType,
                    licenseSerialNumber,
                    LicenseBindingType.PerUser);

                return false;
            }

            foreach (IUser user in group.Users)
            {
                QefUserId userId = QefUserId.Parse(user.Id);
                try
                {
                    licenseManagement.BindLicenseToUser(serialNumber, userId);
                }
                catch (Exception e)
                {
                    message = string.Format("Cannot assign license to user \"{0}\": {1}", user.Id, e.Message);

                    return false;
                }
            }

            return true;
        }

        #endregion
    }
}
