﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using QCommon.Extensions;
using Qis.Common;
using Qis.Common.AttributeValues;
using Qis.Common.Qsql.Operations;
using Qis.Common.Qsql.SelectQuery;
using Qis.Common.Scripting;
using Qis.Common.Scripting.Events.EventHandlerArguments;
using Qis.Common.Scripting.Events.EventHandlerAttributes;

namespace Qis.Module.Scripts
{
    public sealed class PersonStructureCache
    {
        #region Nested Types

        private struct Node
        {
            public Node(Guid[] containers, Guid[] persons)
            {
                Containers = containers;
                Persons = persons;
            }

            public readonly Guid[] Containers;
            public readonly Guid[] Persons;
        }

        #endregion

        #region Fields

        private readonly ConcurrentDictionary<string, IReadOnlyDictionary<Guid, Node>> m_cache = 
            new ConcurrentDictionary<string, IReadOnlyDictionary<Guid, Node>>();

        private readonly HashSet<string> m_templates;
        private readonly IReadOnlyCollection<string> m_attributes;

        #endregion

        #region Constructors

        public PersonStructureCache(IReadOnlyDictionary<string, IReadOnlyCollection<string>> settings)
        {
            m_templates = settings.Keys.ToHashSet();
            m_attributes = settings.Values.SelectMany(a => a).ToHashSet();
        }

        #endregion

        #region Private Methods

        private static IReadOnlyDictionary<Guid, Node> ReadStructure(
            IConfiguration configuration,
            IReadOnlyCollection<string> templates,
            IReadOnlyCollection<string> attributes)
        {
            var links = configuration.RunTableValuedQsqlExpression(
                new SelectExpression(
                    new TableValuedColumnExpressions(
                        new []
                        {
                            new AttributeColumnExpression(new AttributeValueOperand(PredefinedObjectAttribute.ObjectId.Name))
                        }
                        .Union(attributes
                            .Select(a => new AttributeColumnExpression(new AttributeValueOperand(a, assumedType: typeof(string)))))
                        .ToArray()),
                    new TemplateSetTable(templates)));

            var containerIds = Enumerable.Range(0, links.RowsCount)
                .Select(i => links
                    .GetValue<ObjectId>(i, PredefinedObjectAttribute.ObjectId.Name)
                    .Value)
                .ToHashSet();

            var result = Enumerable.Range(0, links.RowsCount)
                .Select(
                    i =>
                        new
                        {
                            Id = links.GetValue<ObjectId>(i, PredefinedObjectAttribute.ObjectId.Name).Value,
                            Containers = attributes
                                .Select(a => links.GetValue<string>(i, a))
                                .SelectMany(s => QHyperLink.GetLinksFromText(s))
                                .Where(l => l.IsLocal)
                                .Select(l => l.ObjectId.Id.Value)
                                .Split(id => containerIds.Contains(id))
                        })
                .ToDictionary(
                    r => r.Id, 
                    r => new Node(r.Containers.True.ToArray(), r.Containers.False.ToArray()));

            return result;
        }

        private IReadOnlyDictionary<Guid, Node> GetCachedPersonStructure(
            ConfigurationContext configurationCtx)
        {
            return m_cache.GetOrAdd(
                ConfigurationContextKeyGenerator.GetCacheKey(configurationCtx),
                _ =>
                {
                    var storage = new GovernanceSettingsStorage(configurationCtx.Configuration);

                    return ReadStructure(configurationCtx.Configuration, m_templates, m_attributes);
                });
        }

        private static void GatherPersons(
            Guid personOrContainerId,
            IReadOnlyDictionary<Guid, Node> personStructure,
            HashSet<Guid> processedContainers,
            HashSet<Guid> result)
        {
            Node node;
            if (!personStructure.TryGetValue(personOrContainerId, out node))
            {
                result.Add(personOrContainerId);
                return;
            }

            if (!processedContainers.Add(personOrContainerId))
            {
                return;
            }

            node.Persons.DoForEach(id => result.Add(id));
            node.Containers.DoForEach(id => GatherPersons(id, personStructure, processedContainers, result));
        }

        private void ClearCachedStructure()
        {
            m_cache.Clear();
        }

        #endregion

        #region Public Methods

        public IReadOnlyCollection<ObjectId> ResolvePersons(
            ConfigurationContext configurationCtx, 
            IReadOnlyCollection<ObjectId> ids)
        {
            var cache = GetCachedPersonStructure(configurationCtx);

            var processedContainers = new HashSet<Guid>();
            var result = new HashSet<Guid>();

            ids.DoForEach(id => GatherPersons(id.Value, cache, processedContainers, result));

            return result.Select(id => ObjectId.Parse(id)).ToList();
        }

        public bool IsCached(string template)
        {
            return m_templates.Contains(template);
        }

        public void Clear()
        {
            ClearCachedStructure();
        }

        #endregion
    }

    public static class PersonStructure
    {
        #region Fields

        private static readonly PersonStructureCache s_cache = new PersonStructureCache(
            new Dictionary<string, IReadOnlyCollection<string>>{
                { "Position", new[] { "HasPositionHolder" } },
                { "OrganizationUnit", new[] { "HasResources", "HasResponsible" } },
                { "InterestGroup", new[] { "HasParticipant" } },
                { "Role", new [] { "HasActor" } },
            });

        private static readonly ObjectId[] s_emptyArray = new ObjectId[0];

        #endregion

        #region Private Methods

        private static IReadOnlyCollection<ObjectId> ReadResponsible(RepositoryObject revision)
        {
            IAttributeValue hasResponsible;
            // Expensive operation of getting attribute.
            if (!revision.Attributes.TryGetValue("HasResponsible", out hasResponsible))
            {
                return s_emptyArray;
            }

            // Here we assumes that objects are of cached templates.
            // That is true in current default metamodel.
            return hasResponsible.GetHyperLinks().
                Select(l => l.ObjectId.Id)
                .ToList();
        }

        #endregion

        #region Public Methods

        public static RepositoryObjectList GetListOfPerson(
            ConfigurationContext configurationCtx,
            RepositoryObjectList personOrContainers)
        {
            #region Argument Check

            configurationCtx.EnsureNotNull(nameof(configurationCtx));
            personOrContainers.EnsureNotNull(nameof(personOrContainers));

            #endregion

            var persons = personOrContainers
                .WithNameAndTemplate()
                .Split(r => r.Template == "Person");
            var result = new List<RepositoryObject>(persons.True);

            var cachedContainers = persons.False
                .Split(r => s_cache.IsCached(r.Template));
            if (cachedContainers.True.Any())
            {
                var cachedResult = s_cache.ResolvePersons(
                    configurationCtx,
                    cachedContainers.True.Select(r => r.Id.Id).ToList());

                result.AddRange(
                    configurationCtx.Configuration
                        .GetObjectSet(new OidList(cachedResult.Select(id => id.ToDefaultOid()))));
            }

            if (cachedContainers.False.Any())
            {
                var responsible = cachedContainers
                    .False
                    .SelectMany(ReadResponsible)
                    .ToList();
                if (responsible.Any())
                {
                    var cachedResult = s_cache.ResolvePersons(configurationCtx, responsible);
                    result.AddRange(
                        configurationCtx.Configuration
                            .GetObjectSet(new OidList(cachedResult.Select(id => id.ToDefaultOid()))));
                }
            }

            return new RepositoryObjectList(result);
        }

        public static IReadOnlyCollection<ObjectId> GetListOfPersonId(
            ConfigurationContext configurationCtx,
            RepositoryObject personOrContainer)
        {
            #region Argument Check

            configurationCtx.EnsureNotNull(nameof(configurationCtx));
            personOrContainer.EnsureNotNull(nameof(personOrContainer));

            #endregion

            if (personOrContainer.Template == "Person")
            {
                return personOrContainer.Id.Id.AsArray();
            }

            if (s_cache.IsCached(personOrContainer.Template))
            {
                return s_cache.ResolvePersons(
                    configurationCtx,
                    personOrContainer.Id.Id.AsArray());
            }

            var hasResponsible = ReadResponsible(personOrContainer);
            if (hasResponsible.Any())
            {
                return s_cache.ResolvePersons(configurationCtx, hasResponsible);
            }

            return s_emptyArray;
        }

        [ObjectCreated]
        [ObjectRevisionCreated]
        [ObjectChanged]
        [ObjectDeleted]
        public static void StructureChanged(ObjectWithIdEventArgs args)
        {
            if (s_cache.IsCached(args.ObjectTemplate))
            {
                s_cache.Clear();
            }
        }

        public static void Clear(GeneralScriptArgs args)
        {
            s_cache.Clear();
        }

        #endregion
    }
}