﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using QCommon.Extensions;
using QCommon.HashCodes;
using Qef.Common;
using Qis.Common;
using Qis.Common.AttributeValues;
using Qis.Common.Metamodel;
using Qis.Common.Qsql;
using Qis.Common.Qsql.Operations;
using Qis.Common.Qsql.Operations.Logical;
using Qis.Common.Qsql.SelectQuery;
using Qis.Common.Scripting;
using Qis.Common.Scripting.Events.EventHandlerArguments;
using Qis.Common.Scripting.Events.EventHandlerAttributes;
using Qis.Web.Forms.Common;
using Qis.Web.Forms.Common.ObjectDialog;
using Qis.Web.Forms.Common.Scripting;

namespace Qis.Module.Scripts
{
    /// <summary>
    ///     Provides methods relevant to repository objects.
    /// </summary>
    public static class ObjectScripts
    {
        #region Nested Types

        /// <summary>
        ///     Types of revision label update.
        /// </summary>
        public enum RevisionUpdateType
        {
            /// <summary>
            ///     Does not change the current revision label number.
            /// </summary>
            None,

            /// <summary>
            ///     Sets 0.0 as the current revision label number.
            /// </summary>
            Zero,

            /// <summary>
            ///     Adds 0.1 to the current revision label number.
            /// </summary>
            Minor,

            /// <summary>
            ///     Adds 0.1 to the latest revision label number.
            /// </summary>
            MinorToLast,

            /// <summary>
            ///     Adds 1.0 to the current revision label number.
            /// </summary>
            Major
        }

        private struct ValidRevision
        {
            #region Constructors

            public ValidRevision(
                ObjectId objectId,
                RevisionId revisionId,
                LanguageId language,
                DateTime validFrom,
                DateTime validTo)
            {
                ObjectId = objectId;
                RevisionId = revisionId;
                Language = language;
                ValidFrom = validFrom;
                ValidTo = validTo;
            }

            #endregion

            #region Public Properties

            public ObjectId ObjectId { get; }

            public RevisionId RevisionId { get; }

            public LanguageId Language { get; }

            public DateTime ValidFrom { get; }

            public DateTime ValidTo { get; }

            #endregion
        }

        #endregion

        #region Constants

        private const string c_historyDateFormat = "dd-MM-yyyy HH:mm:ss";
        public const int DefaultRevisionLabel = 0;

        #endregion

        #region Private Methods

        private static List<ILogicalExpression> CreateAndExpression(List<ILogicalExpression> expressions)
        {
            if (expressions.Count <= 1)
            {
                return expressions;
            }

            var firstOperand = expressions[0];
            var secondOperant = expressions[1];

            var result = new List<ILogicalExpression>(expressions.Skip(2))
            {
                new AndOperationExpression(firstOperand, secondOperant)
            };

            return CreateAndExpression(result);
        }

        private static IWhere CreateWhereExpression(
            IMetamodel metamodel,
            IEnumerable<string> templates,
            Dictionary<string, string> request,
            IWhere uiFilterExpression = null)
        {
            var expressions = new List<ILogicalExpression>();

            if (uiFilterExpression != null)
            {
                expressions.Add(uiFilterExpression.Clause);
            }

            foreach (string key in request.Keys)
            {
                string attributeName = key;

                IAttributeDefinition attributeDefinition = metamodel
                    .Templates
                    .Where(item => templates.Contains(item.Name) && item.Attributes.Contains(key))
                    .Select(item => item.Attributes[key])
                    .FirstOrDefault();

                if (attributeDefinition != null)
                {
                    switch (attributeDefinition.Format)
                    {
                        case AttributeFormat.SingleLink:
                        case AttributeFormat.MultiLink:
                        case AttributeFormat.SingleFile:
                        case AttributeFormat.MultiFile:

                            attributeName += AttributeValueOperand.AttributeChainDelimiter
                                + PredefinedObjectAttribute.Name.Name;

                            break;
                    }
                }

                expressions.Add(
                    new LikeOperationExpression(
                        new AttributeValueOperand(attributeName),
                        new ConstantOperand(request[key])));
            }

            var result = CreateAndExpression(expressions);

            return result.Count == 1 ? new WhereExpression(result.Single()) : null;
        }

        private static HashSet<string> ParseTemplates(string template)
        {
            return new HashSet<string>(
                template
                    .EnsureNotBlank("template")
                    .Split(new[] {";", ",", " "}, StringSplitOptions.RemoveEmptyEntries));
        }

        private static IReadOnlyCollection<Oid> SelectValidRevisions(
            IReadOnlyCollection<ValidRevision> revisions,
            DateTime validAt)
        {
            var result = new List<Oid>();
            var languages = new List<LanguageId>();

            revisions
                .OrderByDescending(r => r.ValidFrom)
                .DoForEach(r =>
                {
                    if (r.ValidTo.IsMinimum() || r.ValidTo >= validAt)
                    {
                        if (!languages.Contains(r.Language))
                        {
                            result.Add(new Oid(r.ObjectId, r.RevisionId));
                            languages.Add(r.Language);
                        }
                    }
                });

            return result;
        }

        #endregion

        #region Internal Methods

        internal static IReadOnlyCollection<Oid> SelectRevisionsForPointInTime(
            ConfigurationContext configurationCtx,
            DateTime validAt)
        {
            var query = new SelectExpression(
                new TableValuedColumnExpressions(
                    new[]
                    {
                        new AttributeColumnExpression(new AttributeValueOperand(PredefinedObjectAttribute.ObjectId.Name)),
                        new AttributeColumnExpression(new AttributeValueOperand(PredefinedObjectAttribute.RevisionId.Name)),
                        new AttributeColumnExpression(new AttributeValueOperand(PredefinedObjectAttribute.Language.Name)),
                        new AttributeColumnExpression(new AttributeValueOperand(Metamodel.Attributes.RevisionValidFrom, assumedType: typeof(DateTime))),
                        new AttributeColumnExpression(new AttributeValueOperand(Metamodel.Attributes.RevisionValidTo, assumedType: typeof(DateTime))),
                    }),
                new AllTemplatesTable(),
                new WhereExpression(
                    new AndOperationExpression(
                        new LessOrEqualOperationExpression(
                            new AttributeValueOperand(Metamodel.Attributes.RevisionValidFrom),
                            new ConstantOperand(QDateTime.ToSortableString(validAt))),
                        new EqualOperationExpression(
                            new AttributeValueOperand(PredefinedObjectAttribute.IsDefaultRevision.Name),
                            new ConstantOperand("0")))),
                new OrderExpression(
                    new OrderItemExpression(new AttributeValueOperand(Metamodel.Attributes.RevisionValidFrom))));

            var options = new QueryOptions()
            {
                Scope = ObjectsScope.AllRevisionsAndLanguageVariants,
            };

            var allRevisionTable = configurationCtx.Configuration.RunTableValuedQsqlExpression(query, options);

            var allRevisions = Enumerable.Range(0, allRevisionTable.RowsCount)
                .Select(i =>
                    new ValidRevision(
                        allRevisionTable
                            .GetValue<ObjectId>(i, PredefinedObjectAttribute.ObjectId.Name),
                        allRevisionTable
                            .GetValue<RevisionId>(i, PredefinedObjectAttribute.RevisionId.Name),
                        allRevisionTable
                            .GetValue<LanguageId>(i, PredefinedObjectAttribute.Language.Name),
                        allRevisionTable
                            .GetValue<DateTime>(i, Metamodel.Attributes.RevisionValidFrom),
                        allRevisionTable
                            .GetValue<DateTime>(i, Metamodel.Attributes.RevisionValidTo)))
                .ToList();

            var selectedRevisions = allRevisions
                .OrderedGroupBy(x => x.ObjectId.Value)
                .SelectMany(g => SelectValidRevisions(g.ToList(), validAt))
                .ToList();

            return selectedRevisions;
        }

        internal static IReadOnlyCollection<Oid> SelectRevisionsForPointInTimeComplex(
            ConfigurationContext configurationCtx,
            DateTime validAt)
        {

            var ValidFrom = new AttributeValueOperand(Metamodel.Attributes.RevisionValidFrom);
            var query = new SelectExpression(
                new TableValuedColumnExpressions(
                    new[]
                    {
                        new AttributeColumnExpression(new AttributeValueOperand(PredefinedObjectAttribute.ObjectId.Name)),
                        new AttributeColumnExpression(new AttributeValueOperand(PredefinedObjectAttribute.RevisionId.Name)),
                        new AttributeColumnExpression(new AttributeValueOperand(PredefinedObjectAttribute.Language.Name)),
                        new AttributeColumnExpression(new AttributeValueOperand(PredefinedObjectAttribute.Created.Name)),
                        new AttributeColumnExpression(new AttributeValueOperand(PredefinedObjectAttribute.Modified.Name)),
                        new AttributeColumnExpression(new AttributeValueOperand(Metamodel.Attributes.RevisionValidFrom, assumedType: typeof(DateTime))),
                        new AttributeColumnExpression(new AttributeValueOperand(Metamodel.Attributes.RevisionValidTo, assumedType: typeof(DateTime))),
                    }),
                new AllTemplatesTable(),
                new WhereExpression(
                    new OrOperationExpression(
                        new AndOperationExpression(
                            new EqualOperationExpression(ValidFrom, new ConstantOperand("")),
                            new LessOrEqualOperationExpression(ValidFrom, new ConstantOperand(QDateTime.ToSortableString(validAt)))),
                        new AndOperationExpression(new EqualOperationExpression(ValidFrom, new ConstantOperand("")),
                        new AndOperationExpression(
                            new LessOrEqualOperationExpression(new AttributeValueOperand(PredefinedObjectAttribute.Created.Name), new ConstantOperand(QDateTime.ToSortableString(validAt))), 
                            new LessOrEqualOperationExpression(new AttributeValueOperand(PredefinedObjectAttribute.Modified.Name), new ConstantOperand(QDateTime.ToSortableString(validAt)))))
                            )),
                new OrderExpression(
                    new OrderItemExpression(new AttributeValueOperand(PredefinedObjectAttribute.ObjectId.Name))));

            var options = new QueryOptions()
            {
                Scope = ObjectsScope.AllRevisionsAndLanguageVariants,
            };

            var allRevisionTable = configurationCtx.Configuration.RunTableValuedQsqlExpression(query, options);

            var allRevisions = Enumerable.Range(0, allRevisionTable.RowsCount)
                .Select(i =>
                    new ValidRevision(
                        allRevisionTable
                            .GetValue<ObjectId>(i, PredefinedObjectAttribute.ObjectId.Name),
                        allRevisionTable
                            .GetValue<RevisionId>(i, PredefinedObjectAttribute.RevisionId.Name),
                        allRevisionTable
                            .GetValue<LanguageId>(i, PredefinedObjectAttribute.Language.Name),
                        allRevisionTable
                            .GetValue<DateTime>(i, Metamodel.Attributes.RevisionValidFrom),
                        allRevisionTable
                            .GetValue<DateTime>(i, Metamodel.Attributes.RevisionValidTo)))
                .ToList();

            var selectedRevisions = allRevisions
                .OrderedGroupBy(x => x.ObjectId.Value)
                .SelectMany(g => SelectValidRevisions(g.ToList(), validAt))
                .ToList();

            return selectedRevisions;
        }

        #endregion

        #region Public Methods

        public static RepositoryObjectList WithFullData(
            this RepositoryObjectList repositoryObjectList)
        {
            if (repositoryObjectList == null)
            {
                return null;
            }

            var result = new RepositoryObjectList(repositoryObjectList);

            result.InstantiationLevel = ObjectInstantiationLevel.FullData;

            return result;
        }

        public static RepositoryObjectList WithNameAndTemplate(
            this RepositoryObjectList repositoryObjectList)
        {
            if (repositoryObjectList == null)
            {
                return null;
            }

            var result = new RepositoryObjectList(repositoryObjectList);
            result.InstantiationLevel = ObjectInstantiationLevel.NameAndTemplate;
            result.PageSize = 200;

            return result;
        }

        public static RepositoryObjectList WithAuditAndConfiguration(
            this RepositoryObjectList repositoryObjectList)
        {
            if (repositoryObjectList == null)
            {
                return null;
            }

            var result = new RepositoryObjectList(repositoryObjectList);

            result.InstantiationLevel = ObjectInstantiationLevel.AuditAndConfiguration;

            return result;
        }

        /// <summary>
        ///     Clears attribute of the specified object.
        /// </summary>
        /// <param name="obj">
        ///     Object, the attributes of which to clear.
        /// </param>
        /// <param name="attrName">
        ///     Name of the attribute to clear.
        /// </param>
        /// <exception cref="ArgumentNullException">
        ///     <paramref name="obj" /> is null.
        /// </exception>
        public static void ClearAttributeValue(this RepositoryObject obj, string attrName)
        {
            #region Argument Check

            if (obj == null)
            {
                throw new ArgumentNullException("obj");
            }

            #endregion

            IAttribute attr;
            if (obj.Attributes.TryGetAttribute(attrName, out attr))
            {
                attr.Value.Clear();
            }
        }

        /// <summary>
        ///     Creates a new object revision. If object contains the <see cref="Metamodel.Attributes.RevisionLabel" />
        ///     attribute, it is updated accordingly. If object is not frozen, new revision is not created and
        ///     the <see cref="Metamodel.Attributes.RevisionLabel" /> attribute is not updated.
        /// </summary>
        /// <param name="obj">
        ///     Object, new revision of which to create.
        /// </param>
        /// <returns>
        ///     New object revision if object is frozen, otherwise <paramref name="obj" />.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        ///     <paramref name="obj" /> is null.
        /// </exception>
        /// <exception cref="QInvalidScriptOperationException">
        ///     New object revision cannot be created.
        /// </exception>
        public static RepositoryObject CreateRevision(RepositoryObject obj)
        {
            #region Argument Check

            if (obj == null)
            {
                throw new ArgumentNullException("obj");
            }

            #endregion

            IConfiguration configuration = obj.GetConfiguration();

            return CreateRevision(configuration, obj);
        }

        /// <summary>
        ///     Creates a new object revision. If object contains the <see cref="Metamodel.Attributes.RevisionLabel" />
        ///     attribute, it is updated accordingly. If object is not frozen, new revision is not created and
        ///     the <see cref="Metamodel.Attributes.RevisionLabel" /> attribute is not updated.
        /// </summary>
        /// <param name="configuration">
        ///     Configuration, in which new revision to create.
        /// </param>
        /// <param name="obj">
        ///     Object, new revision of which to create.
        /// </param>
        /// <returns>
        ///     New object revision if object is frozen, otherwise <paramref name="obj" />.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        ///     <paramref name="obj" /> is null.
        /// </exception>
        /// <exception cref="QInvalidScriptOperationException">
        ///     New object revision cannot be created.
        /// </exception>
        public static RepositoryObject CreateRevision(
            IConfiguration configuration,
            RepositoryObject obj)
        {
            #region Argument Check

            if (configuration == null)
            {
                throw new ArgumentNullException("configuration");
            }

            if (obj == null)
            {
                throw new ArgumentNullException("obj");
            }

            #endregion

            if (!configuration.IsConnected)
            {
                configuration.Connect();
            }

            RepositoryObject repositoryObject = configuration.FindObject(obj.Id);

            if (repositoryObject == null)
            {
                throw new QInvalidScriptOperationException(
                    string.Format(
                        "{0} \"{1}\": {2}.",
                        Messages.CannotCreateRevisionMessage,
                        obj.Name,
                        "Failed to get reference to revision"));
            }

            RepositoryObject objectRevision = configuration.CreateObjectRevision(obj.Id);

            if (objectRevision == null)
            {
                throw new QInvalidScriptOperationException(
                    string.Format(
                        "{0} \"{1}\": {2}.",
                        Messages.CannotCreateRevisionMessage,
                        obj.Name,
                        "Failed to get reference to new revision"));
            }

            // Clear approve attributes.
            CMS.ClearApproveAttributes(objectRevision);

            objectRevision.SaveData();

            return objectRevision;
        }

        /// <summary>
        ///     Gets a list of distinct objects.
        /// </summary>
        /// <param name="objects">
        ///     List of objects.
        /// </param>
        /// <returns>
        ///     List of distinct objects.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        ///     <paramref name="objects" /> is null.
        /// </exception>
        public static RepositoryObjectList Distinct(this IEnumerable<RepositoryObject> objects)
        {
            #region Argument Check

            if (objects == null)
            {
                throw new ArgumentNullException("objects");
            }

            #endregion

            return new RepositoryObjectList(objects.Distinct(RepositoryObjectByIdComparer.Instance));
        }

        /// <summary>
        ///     Iterates over <see cref="RepositoryObjectList"/>  unloading already processed revisions.
        /// </summary>
        /// <param name="list">
        ///     Revision list.
        /// </param>
        /// <param name="func">
        ///     Action to be applied.
        /// </param>
        public static void DoForEach(this RepositoryObjectList list, Action<RepositoryObject> func)
        {
            int objListIt = 0;

            while (objListIt < list.Count)
            {
                var blockSize = Math.Min(list.PageSize, list.Count - objListIt);

                for (int i = 0; i < blockSize; i++, objListIt++)
                {
                    var revision = list[objListIt];
                    func(revision);
                }

                list.UnloadRange(objListIt - blockSize, blockSize);
            }
        }

        /// <summary>
        ///     Iterates over <see cref="RepositoryObjectList"/>  unloading already processed revisions.
        /// </summary>
        /// <param name="list">
        ///     Revision list.
        /// </param>
        /// <param name="func">
        ///     Action to be applied.
        /// </param>
        public static void DoForEach(this RepositoryObjectList list, Action<RepositoryObject, int> func)
        {
            int objListIt = 0;

            while (objListIt < list.Count)
            {
                var blockSize = Math.Min(list.PageSize, list.Count - objListIt);

                for (int i = 0; i < blockSize; i++, objListIt++)
                {
                    var revision = list[objListIt];
                    func(revision, i);
                }

                list.UnloadRange(objListIt - blockSize, blockSize);
            }
        }

        /// <summary>
        ///     Filter a RepositoryObjectList, returning only objects of a specified template.
        /// </summary>
        /// <param name="repositoryObjectList">
        ///     Source <see cref="RepositoryObjectList" /> to filter.
        /// </param>
        /// <param name="template">
        ///     Template(s) to filter by. Can accept list of templates delimited by ';' or ',' or ' '.
        /// </param>
        public static RepositoryObjectList FilterByTemplate(
            this RepositoryObjectList repositoryObjectList,
            string template)
        {
            #region Argument Check

            repositoryObjectList.EnsureNotNull("objects");

            #endregion

            var result = new RepositoryObjectList();
            result.InstantiationLevel = repositoryObjectList.InstantiationLevel
                < ObjectInstantiationLevel.NameAndTemplate
                ? ObjectInstantiationLevel.NameAndTemplate
                : repositoryObjectList.InstantiationLevel;

            if (repositoryObjectList.InstantiationLevel < ObjectInstantiationLevel.NameAndTemplate)
            {
                repositoryObjectList.InstantiationLevel = ObjectInstantiationLevel.NameAndTemplate;
            }

            var templates = ParseTemplates(template);

            result.AddRange(repositoryObjectList.Where(x => templates.Contains(x.Template)));

            return result;
        }

        /// <summary>
        ///     Finds the base object revision. The base object revision is found by the
        ///     <see cref="Metamodel.Attributes.RevisionLabel" /> attribute.
        /// </summary>
        /// <param name="obj">
        ///     Object, which the base revision should be searched.
        /// </param>
        /// <returns>
        ///     The base object revision.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        ///     <paramref name="obj" /> is null.
        /// </exception>
        public static RepositoryObject FindBaseObjectRevision(RepositoryObject obj)
        {
            #region Argument Check

            if (obj == null)
            {
                throw new ArgumentNullException("obj");
            }

            #endregion

            RepositoryObjectList objects = FindObjectRevisionsOrderedByRevisionLabel(obj);

            if (objects == null || objects.Count == 0)
            {
                return null;
            }

            return objects.First();
        }

        /// <summary>
        ///     Finds the last object revision. The last object revision is found by the
        ///     <see cref="Metamodel.Attributes.RevisionLabel" /> attribute.
        /// </summary>
        /// <param name="obj">
        ///     Object, which the last revision should be searched.
        /// </param>
        /// <returns>
        ///     The last object revision.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        ///     <paramref name="obj" /> is null.
        /// </exception>
        public static RepositoryObject FindLastObjectRevision(RepositoryObject obj)
        {
            #region Argument Check

            if (obj == null)
            {
                throw new ArgumentNullException("obj");
            }

            #endregion

            RepositoryObjectList objects = FindObjectRevisionsOrderedByRevisionLabel(obj);

            if (objects == null || objects.Count == 0)
            {
                return null;
            }

            return objects.Last();
        }

        /// <summary>
        ///     Returns the objects of a specific template(s) (could be separated by ';' or ',' or ' ') with matched name if
        ///     specified.
        ///     Usage:
        ///     var objects = configuration.FindName("BrowserDiagram;ArchitectureFramework");
        ///     var objects = configuration.FindName("BrowserDiagram", "Core process", 1);
        /// </summary>
        /// <param name="configuration">
        ///     Configuration to find in.
        /// </param>
        /// <param name="template">
        ///     Template or template set (delimited with ';' or ',' or ' ' ) to find.
        /// </param>
        /// <param name="name">
        ///     Name to find. Optional.
        /// </param>
        /// <param name="top">
        ///     Number of objects to return. Optional.
        /// </param>
        /// <param name="skip">
        ///     Number of objects to skip from the start. Optional.
        /// </param>
        public static RepositoryObjectList FindName(
            this IConfiguration configuration,
            string template,
            string name = null,
            int top = 0,
            int skip = 0,
            bool useLikeExpression = false)
        {
            var templates = ParseTemplates(template);

            IQuery query = new SelectExpression(
                new ColumnExpressions(new[] {new ObjectIdExpression()}),
                templates.Count == 1
                    ? (ITableSource)new TemplateTable(templates.First())
                    : new TemplateSetTable(templates),
                name.IsBlank()
                    ? null
                    : (useLikeExpression
                    ? (new WhereExpression(
                        new LikeOperationExpression(
                            new AttributeValueOperand(PredefinedObjectAttribute.Name.Name),
                            new ConstantOperand(name))))
                    : (new WhereExpression(
                        new EqualOperationExpression(
                            new AttributeValueOperand(PredefinedObjectAttribute.Name.Name),
                            new ConstantOperand(name))))),
                null,
                top,
                skip);

            return configuration.EnsureNotNull("configuration").RunQsqlExpression(query);
        }

        /// <summary>
        ///     Finds object revisions ordered by the specified attribute.
        /// </summary>
        /// <param name="obj">
        ///     Object, which revisions should be searched.
        /// </param>
        /// <param name="attrName">
        ///     Name of attribute, by which object revisions are ordered.
        /// </param>
        /// <returns>
        ///     List of object revisions ordered by the <see cref="Metamodel.Attributes.RevisionLabel" /> attribute.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        ///     <paramref name="obj" /> is null.
        /// </exception>
        public static RepositoryObjectList FindObjectRevisionsOrderedByAttr(RepositoryObject obj, string attrName)
        {
            #region Argument Check

            if (obj == null)
            {
                throw new ArgumentNullException("obj");
            }

            if (string.IsNullOrEmpty(attrName))
            {
                throw new ArgumentException("Attribute name cannot be null or empty.", "attrName");
            }

            #endregion

            IConfiguration configuration = obj.GetConfiguration();

            if (configuration == null)
            {
                throw new QInvalidRepositoryOperationException("Cannot find configuration.");
            }

            IRepository repository = configuration.GetRepository();

            if (repository == null)
            {
                throw new QInvalidRepositoryOperationException("Cannot find repository.");
            }

            RepositoryObjectList objects = repository.FindObjectRevisions(obj.Id);

            IQuery query = new SelectExpression(
                new ColumnExpressions(new[] {new ObjectIdExpression()}),
                new ObjectListTable(objects),
                null,
                new OrderExpression(
                    new OrderItemExpression(new AttributeValueOperand(attrName)),
                    new OrderItemExpression(new AttributeValueOperand(Metamodel.Attributes.Created)),
                    new OrderItemExpression(new AttributeValueOperand(Metamodel.Attributes.Modified))));

            QueryOptions options = new QueryOptions();
            options.Scope = ObjectsScope.AllRevisionsAndLanguageVariants;

            return repository.RunQsqlExpression(query, configuration.Id, options);
        }

        /// <summary>
        ///     Finds object revisions ordered by the <see cref="PredefinedObjectAttribute.LocalRevision" /> attribute.
        /// </summary>
        /// <param name="obj">
        ///     Object, which revisions should be searched.
        /// </param>
        /// <returns>
        ///     List of object revisions ordered by the <see cref="PredefinedObjectAttribute.LocalRevision" /> attribute.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        ///     <paramref name="obj" /> is null.
        /// </exception>
        public static RepositoryObjectList FindObjectRevisionsOrderedByRevisionLabel(RepositoryObject obj)
        {
            #region Argument Check

            if (obj == null)
            {
                throw new ArgumentNullException("obj");
            }

            #endregion

            return FindObjectRevisionsOrderedByAttr(obj, PredefinedObjectAttribute.LocalRevision.Name);
        }

        /// <summary>
        ///     Finds objects of the specified template, ordered by the specified attribute.
        /// </summary>
        /// <param name="repository">
        ///     Repository.
        /// </param>
        /// <param name="configuration">
        ///     Configuration.
        /// </param>
        /// <param name="template">
        ///     Object template.
        /// </param>
        /// <param name="attrName">
        ///     Attribute name.
        /// </param>
        /// <returns>
        ///     Finds objects of the specified template, ordered by the specified attribute.
        /// </returns>
        public static RepositoryObjectList FindObjectsOrderedByAttr(
            IRepository repository,
            IConfiguration configuration,
            string template,
            string attrName)
        {
            #region Argument Check

            if (repository == null)
            {
                throw new ArgumentNullException("repository");
            }

            if (configuration == null)
            {
                throw new ArgumentNullException("configuration");
            }

            if (template.IsBlank())
            {
                throw new ArgumentException("template name cannot be null or empty.", "template");
            }

            if (attrName.IsBlank())
            {
                throw new ArgumentException("Attribute name cannot be null or empty.", "attrName");
            }

            #endregion

            IQuery query = new SelectExpression(
                new ColumnExpressions(new[] {new ObjectIdExpression()}),
                new TemplateTable(template),
                new WhereExpression(
                    new NotEqualOperationExpression(
                        new AttributeValueOperand(attrName),
                        new ConstantOperand(string.Empty))),
                new OrderExpression(
                    new OrderItemExpression(new AttributeValueOperand(attrName)),
                    new OrderItemExpression(new AttributeValueOperand(Metamodel.Attributes.Created)),
                    new OrderItemExpression(new AttributeValueOperand(Metamodel.Attributes.Modified))));

            QueryOptions options = new QueryOptions();
            options.Scope = ObjectsScope.AllRevisionsAndLanguageVariants;

            return repository.RunQsqlExpression(query, configuration.Id, options);
        }

        /// <summary>
        ///     Freezes the object.
        /// </summary>
        /// <param name="obj">
        ///     Object to freeze.
        /// </param>
        /// <exception cref="ArgumentNullException">
        ///     <paramref name="obj" /> is null.
        /// </exception>
        /// <exception cref="QInvalidScriptOperationException">
        ///     Object is already frozen.<br />
        ///     -or-<br />
        ///     Object cannot be frozen.
        /// </exception>
        public static void Freeze(this RepositoryObject obj)
        {
            #region Argumen check

            if (obj == null)
            {
                throw new ArgumentNullException("obj");
            }

            #endregion

            if (obj.IsFrozen)
            {
                throw new QInvalidScriptOperationException(
                    string.Format(
                        "{0} \"{1}\": {2}.",
                        Messages.CannotFreezeMessage,
                        obj.Name,
                        Messages.IsFrozenMessage));
            }

            obj.FreezeObject();

            if (!obj.IsFrozen)
            {
                throw new QInvalidScriptOperationException(
                    string.Format(
                        "{0} \"{1}\": {2}.",
                        Messages.CannotFreezeMessage,
                        obj.Name,
                        Messages.ReasonUnknownMessage));
            }
        }

        /// <summary>
        ///     Freezes the object.
        /// </summary>
        /// <param name="obj">
        ///     Object to freeze.
        /// </param>
        /// <exception cref="ArgumentNullException">
        ///     <paramref name="obj" /> is null.
        /// </exception>
        /// <exception cref="QInvalidScriptOperationException">
        ///     Object is already frozen.<br />
        ///     -or-<br />
        ///     Object cannot be frozen.
        /// </exception>
        public static void FreezeObject(RepositoryObject obj)
        {
            obj.Freeze();
        }

        /// <summary>
        ///     Gets list of objects linked by the specified object by means of attribute.
        /// </summary>
        /// <param name="configuration">
        ///     Configuration, in which objects are searched.
        /// </param>
        /// <param name="objectId">
        ///     Identifier of object that must link to objects to return.
        /// </param>
        /// <param name="attrName">
        ///     Name of attribute, which must contain link to object with identifier specified by the
        ///     <see cref="objectId" /> argument.
        /// </param>
        /// <returns>
        ///     List of objects.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        ///     <paramref name="configuration" /> is null.<br />
        ///     -or-<br />
        ///     <paramref name="objectId" /> is null.
        /// </exception>
        public static RepositoryObjectList GetAttributeRelations(
            IConfiguration configuration,
            Oid objectId,
            string attrName)
        {
            #region Argument Check

            if (configuration == null)
            {
                throw new ArgumentNullException("configuration");
            }

            if (objectId == null)
            {
                throw new ArgumentNullException("objectId");
            }

            #endregion

            return GetRelations(configuration, objectId, ObjectReferenceType.AttributeReference, attrName);
        }

        /// <summary>
        ///     Gets attribute of the specified object.
        /// </summary>
        /// <param name="obj">
        ///     Object, the attributes of which to get.
        /// </param>
        /// <param name="attrName">
        ///     Name of the attribute to get.
        /// </param>
        /// <param name="returnEmptyPlainText">
        ///     Optional. Whether to return <b>null</b> if attribute is not found or empty PlainText.
        ///     Default is <b>false</b>.
        /// </param>
        /// <returns>
        ///     Object attribute or null.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        ///     Thrown when <paramref name="obj" /> is null.
        /// </exception>
        /// <exception cref="ArgumentException">
        ///     Thrown when <paramref name="attrName" /> is <b>null</b> or empty.
        /// </exception>
        public static IAttributeValue GetAttributeValue(
            this RepositoryObject obj,
            string attrName,
            bool returnEmptyPlainText = false)
        {
            #region Argument Check

            if (obj == null)
            {
                throw new ArgumentNullException("obj");
            }

            if (attrName.IsBlank())
            {
                throw new ArgumentException("Attribute name cannot be null or empty string.", "attrName");
            }

            #endregion

            IAttributeValue attrValue;
            if (!obj.Attributes.TryGetValue(attrName, out attrValue) && returnEmptyPlainText)
            {
                attrValue = new PlainText();
            }

            return attrValue;
        }

        /// <summary>
        ///     Gets list of objects that link to the specified object by means of attribute.
        /// </summary>
        /// <param name="configuration">
        ///     Configuration, in which objects are searched.
        /// </param>
        /// <param name="objectId">
        ///     Identifier of object that must be linked by objects to return.
        /// </param>
        /// <param name="attrName">
        ///     Name of attribute, which must contain link to object with identifier specified by the
        ///     <see cref="objectId" /> argument.
        /// </param>
        /// <returns>
        ///     List of objects.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        ///     <paramref name="configuration" /> is null.<br />
        ///     -or-<br />
        ///     <paramref name="objectId" /> is null.
        /// </exception>
        public static RepositoryObjectList GetBackAttributeRelations(
            IConfiguration configuration,
            Oid objectId,
            string attrName)
        {
            #region Argument Check

            if (configuration == null)
            {
                throw new ArgumentNullException("configuration");
            }

            if (objectId == null)
            {
                throw new ArgumentNullException("objectId");
            }

            #endregion

            return GetBackRelations(
                configuration.GetRepository(),
                configuration.Id,
                objectId,
                ObjectReferenceType.AttributeReference,
                attrName);
        }

        /// <summary>
        ///     Gets list of objects that link to the specified object by means of graphical relations.
        /// </summary>
        /// <param name="configuration">
        ///     Configuration, in which objects are searched.
        /// </param>
        /// <param name="objectId">
        ///     Identifier of object that must be linked by objects to return.
        /// </param>
        /// <returns>
        ///     List of objects.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        ///     <paramref name="configuration" /> is null.<br />
        ///     -or-<br />
        ///     <paramref name="objectId" /> is null.
        /// </exception>
        public static RepositoryObjectList GetBackGraphicalRelations(IConfiguration configuration, Oid objectId)
        {
            #region Argument Check

            if (configuration == null)
            {
                throw new ArgumentNullException("configuration");
            }

            if (objectId == null)
            {
                throw new ArgumentNullException("objectId");
            }

            #endregion

            return GetBackRelations(
                configuration.GetRepository(),
                configuration.Id,
                objectId,
                ObjectReferenceType.Contains,
                null);
        }

        /// <summary>
        ///     Gets list of objects that link to the specified object.
        /// </summary>
        /// <param name="configuration">
        ///     Configuration, in which objects are searched.
        /// </param>
        /// <param name="objectId">
        ///     Identifier of object that must be linked by objects to return.
        /// </param>
        /// <param name="attrName">
        ///     Name of attribute, which must contain link to object with identifier specified by the
        ///     <see cref="objectId" /> argument.
        /// </param>
        /// <returns>
        ///     List of objects.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        ///     <paramref name="configuration" /> is null.<br />
        ///     -or-<br />
        ///     <paramref name="objectId" /> is null.
        /// </exception>
        public static RepositoryObjectList GetBackRelations(IConfiguration configuration, Oid objectId, string attrName)
        {
            #region Argument Check

            if (configuration == null)
            {
                throw new ArgumentNullException("configuration");
            }

            if (objectId == null)
            {
                throw new ArgumentNullException("objectId");
            }

            #endregion

            return GetBackRelations(
                configuration.GetRepository(),
                configuration.Id,
                objectId,
                ObjectReferenceType.Empty,
                attrName);
        }

        /// <summary>
        ///     Gets list of objects that link to the specified object.
        /// </summary>
        /// <param name="repository">
        ///     Repository, in which objects are searched.
        /// </param>
        /// <param name="configurationId">
        ///     Identifier of configuration, in which objects are searched.
        /// </param>
        /// <param name="objectId">
        ///     Identifier of object that must be linked by objects to return.
        /// </param>
        /// <param name="refType">
        ///     One of the <see cref="ObjectReferenceType" /> types, specifing which type of relations to use.
        /// </param>
        /// <param name="attrName">
        ///     Name of attribute, which must contain link to object with identifier specified by the
        ///     <see cref="objectId" /> argument.
        /// </param>
        /// <returns>
        ///     List of objects.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        ///     <paramref name="configurationId" /> is null.<br />
        ///     -or-<br />
        ///     <paramref name="objectId" /> is null.
        /// </exception>
        public static RepositoryObjectList GetBackRelations(
            IRepository repository,
            Cid configurationId,
            Oid objectId,
            ObjectReferenceType refType,
            string attrName)
        {
            #region Argument Check

            if (repository == null)
            {
                throw new ArgumentNullException("repository");
            }

            if (configurationId == null)
            {
                throw new ArgumentNullException("configurationId");
            }

            if (objectId == null)
            {
                throw new ArgumentNullException("objectId");
            }

            #endregion

            RepositoryObjectList references;
            if (string.IsNullOrEmpty(attrName))
            {
                references = repository.GetUsingObjects(
                    objectId,
                    configurationId,
                    false,
                    refType);
            }
            else
            {
                references = repository.GetUsingObjects(
                    objectId,
                    configurationId,
                    false,
                    refType,
                    attrName);
            }

            references = GetDistinct(references);

            return references;
        }

        /// <summary>
        ///     Gets a list of distinct objects.
        /// </summary>
        /// <param name="objects">
        ///     List of objects.
        /// </param>
        /// <returns>
        ///     List of distinct objects.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        ///     <paramref name="objects" /> is null.
        /// </exception>
        public static RepositoryObjectList GetDistinct(IEnumerable<RepositoryObject> objects)
        {
            return objects.Distinct();
        }

        /// <summary>
        ///     Gets list of objects linked by the specified object by means of graphical relations.
        /// </summary>
        /// <param name="configuration">
        ///     Configuration, in which objects are searched.
        /// </param>
        /// <param name="objectId">
        ///     Identifier of object that must link to objects to return.
        /// </param>
        /// <returns>
        ///     List of objects.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        ///     <paramref name="configuration" /> is null.<br />
        ///     -or-<br />
        ///     <paramref name="objectId" /> is null.
        /// </exception>
        public static RepositoryObjectList GetGraphicalRelations(IConfiguration configuration, Oid objectId)
        {
            #region Argument Check

            if (configuration == null)
            {
                throw new ArgumentNullException("configuration");
            }

            if (objectId == null)
            {
                throw new ArgumentNullException("objectId");
            }

            #endregion

            return GetRelations(configuration, objectId, ObjectReferenceType.Contains, null);
        }

        /// <summary>
        ///     Gets list of objects linked by the specified objects.
        /// </summary>
        /// <param name="configuration">
        ///     Configuration, in which objects are searched.
        /// </param>
        /// <param name="templates">
        ///     Name of templates, of which objects to return.
        /// </param>
        /// <param name="links">
        ///     List of links that must be linked by objects to return.
        /// </param>
        /// <param name="attrName">
        ///     Name of attribute, which must contain the links specified by the <see cref="links" /> argument.
        /// </param>
        /// <returns>
        ///     List of object.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        ///     <paramref name="configuration" /> is null.<br />
        ///     -or-<br />
        ///     <paramref name="links" /> is null.
        /// </exception>
        public static RepositoryObjectList GetLinksToObjects(
            IConfiguration configuration,
            string[] templates,
            QHyperLinkList links,
            string attrName)
        {
            #region Argument Check

            if (configuration == null)
            {
                throw new ArgumentNullException("configuration");
            }

            if (links == null)
            {
                throw new ArgumentNullException("links");
            }

            #endregion

            List<IEnumerable<RepositoryObject>> usingObjects = new List<IEnumerable<RepositoryObject>>();

            links.DoForEach(
                link =>
                    usingObjects.Add(
                        GetBackAttributeRelations(configuration, link.ObjectId, attrName)
                            .Where(obj => templates.Contains(obj.Template))));

            RepositoryObjectList result = new RepositoryObjectList();

            if (usingObjects.Count > 0)
            {
                IEnumerable<RepositoryObject> obj = usingObjects[0];

                for (int i = 1; i < usingObjects.Count; i++)
                {
                    obj = obj.Join(
                        usingObjects[i],
                        outerObject => outerObject.Id,
                        innerObject => innerObject.Id,
                        (outerObject, innerObject) => outerObject);
                }

                result.AddRange(obj);
            }

            return result;
        }

        /// <summary>
        ///     Gets list of objects linked by the specified object.
        /// </summary>
        /// <param name="configuration">
        ///     Configuration, in which objects are searched.
        /// </param>
        /// <param name="objectId">
        ///     Identifier of object that must link to objects to return.
        /// </param>
        /// <param name="attrName">
        ///     Name of attribute, which must contain link to object with identifier specified by the
        ///     <see cref="objectId" /> argument.
        /// </param>
        /// <returns>
        ///     List of objects.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        ///     <paramref name="configuration" /> is null.<br />
        ///     -or<br />
        ///     <paramref name="objectId" /> is null.
        /// </exception>
        public static RepositoryObjectList GetRelations(IConfiguration configuration, Oid objectId, string attrName)
        {
            #region Argument Check

            if (configuration == null)
            {
                throw new ArgumentNullException("configuration");
            }

            if (objectId == null)
            {
                throw new ArgumentNullException("objectId");
            }

            #endregion

            return GetRelations(configuration, objectId, ObjectReferenceType.Empty, attrName);
        }

        /// <summary>
        ///     Gets list of objects linked by the specified object.
        /// </summary>
        /// <param name="configuration">
        ///     Configuration, in which objects are searched.
        /// </param>
        /// <param name="objectId">
        ///     Identifier of object that must link to objects to return.
        /// </param>
        /// <param name="refType">
        ///     One of the <see cref="ObjectReferenceType" /> types, specifing which type of relations to use.
        /// </param>
        /// <param name="attrName">
        ///     Name of attribute, which must contain link to object with identifier specified by the
        ///     <see cref="objectId" /> argument.
        /// </param>
        /// <returns>
        ///     List of objects.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        ///     <paramref name="configuration" /> is null.<br />
        ///     -or-<br />
        ///     <paramref name="objectId" /> is null.
        /// </exception>
        public static RepositoryObjectList GetRelations(
            IConfiguration configuration,
            Oid objectId,
            ObjectReferenceType refType,
            string attrName)
        {
            #region Argument Check

            if (configuration == null)
            {
                throw new ArgumentNullException("configuration");
            }

            if (objectId == null)
            {
                throw new ArgumentNullException("objectId");
            }

            #endregion

            RepositoryObjectList references;
            if (string.IsNullOrEmpty(attrName))
            {
                references = configuration.GetRepository().GetUsedObjects(
                    objectId,
                    configuration.Id,
                    refType);
            }
            else
            {
                references = configuration.GetRepository().GetUsedObjects(
                    objectId,
                    configuration.Id,
                    refType,
                    attrName);
            }

            return references;
        }

        /// <summary>
        ///     Sets attribute of the specified object.
        /// </summary>
        /// <param name="obj">
        ///     Object, the attributes of which to set.
        /// </param>
        /// <param name="attrName">
        ///     Name of the attribute to set.
        /// </param>
        /// <param name="attrValue">
        ///     New value of the attribute.
        /// </param>
        /// <param name="addIfAbsent">
        ///     Optional. <b>true</b> to add attribute if it is absent in object; <b>false</b> otherwise. Default is <b>false</b>.
        /// </param>
        /// <exception cref="ArgumentNullException">
        ///     Thrown when <paramref name="obj" /> is null.
        /// </exception>
        /// <exception cref="ArgumentException">
        ///     Thrown when <paramref name="attrName" /> is <b>null</b> or empty.
        /// </exception>
        public static void SetAttributeValue(
            this RepositoryObject obj,
            string attrName,
            IAttributeValue attrValue,
            bool addIfAbsent = false)
        {
            #region Argument Check

            if (obj == null)
            {
                throw new ArgumentNullException("obj");
            }

            if (attrName.IsBlank())
            {
                throw new ArgumentException("Attribute name cannot be null or empty string.", "attrName");
            }

            #endregion

            IAttribute attr;
            if (!obj.Attributes.TryGetAttribute(attrName, out attr))
            {
                if (addIfAbsent)
                {
                    obj.Attributes.Add(attrName, attrValue);
                }
            }
            else
            {
                attr.Value = attrValue;
            }
        }

        /// <summary>
        ///     Converts a list of objects to a list of hypelinks.
        /// </summary>
        /// <param name="objects">
        ///     List of objects to convert.
        /// </param>
        /// <param name="asLocal">
        ///     <b>true</b> to return links in local format (without repository); otherwise <b>false</b>. Default is <b>false</b>.
        /// </param>
        /// <returns>
        ///     List of hyperlinks.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        ///     <paramref objects="obj" /> is null.
        /// </exception>
        public static QHyperLinkList ToLinks(this RepositoryObjectList objects, bool asLocal = false)
        {
            #region Argument Check

            if (objects == null)
            {
                throw new ArgumentNullException("objects");
            }

            #endregion

            return
                new QHyperLinkList(
                    objects.Select(o => asLocal ? o.GetLocalHyperlink() : o.GetRemoteRepositoryHyperlink()));
        }

        /// <summary>
        ///     Converts a list of links to a list of objects using specified configuration.
        /// </summary>
        /// <param name="links">
        ///     List of links to convert.
        /// </param>
        /// <param name="configuration">
        ///     <see cref="IConfiguration" /> to resolve links in.
        /// </param>
        /// <returns>
        ///     List of hyperlinks.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        ///     <paramref objects="obj" /> is null.
        /// </exception>
        public static RepositoryObjectList ToObjects(
            this IEnumerable<QHyperLink> links,
            IConfiguration configuration)
        {
            #region Argument Check

            if (configuration == null)
            {
                throw new ArgumentNullException("configuration");
            }

            #endregion

            var list = configuration.GetObjectSet(new QHyperLinkList(links ?? Enumerable.Empty<QHyperLink>()));
            return list;
        }

        /// <summary>
        ///     Updates attributes of the specified object from the controls.
        /// </summary>
        /// <param name="obj">
        ///     Object, the attributes of which to update.
        /// </param>
        /// <param name="controlStates">
        ///     Controls with new values for the attributes.
        /// </param>
        /// <param name="processReadOnly">
        ///     <b>true</b> to persist read-only values, otherwise <b>false</b> (default).
        /// </param>
        /// <exception cref="ArgumentNullException">
        ///     <paramref name="obj" /> is null.<br />
        ///     -or-<br />
        ///     <paramref name="controlStates" /> is null.
        /// </exception>
        public static void UpdateAttributeValues(
            RepositoryObject obj,
            ControlMap controlStates,
            bool processReadOnly = false)
        {
            #region Argument Check

            if (obj == null)
            {
                throw new ArgumentNullException("obj");
            }

            if (controlStates == null)
            {
                throw new ArgumentNullException("controlStates");
            }

            #endregion

            foreach (var attributeState in controlStates.AttributeControls)
            {
                if (attributeState.InMemory || (attributeState.ReadOnly && !processReadOnly)
                    || !obj.Attributes.Contains(attributeState.DataKey))
                {
                    continue;
                }

                ObjectAttribute attribute = obj.Attributes[attributeState.DataKey];
                if (!attribute.IsReadOnly)
                {
                    attribute.Value = attributeState.Value;
                }
            }
        }

        /// <summary>
        ///     Updates attributes of the specified object from the controls.
        /// </summary>
        /// <param name="obj">
        ///     Object, the attributes of which to update.
        /// </param>
        /// <param name="controlStates">
        ///     Controls with new values for the attributes.
        /// </param>
        /// <param name="processReadOnly">
        ///     <b>true</b> to persist read-only values, otherwise <b>false</b> (default).
        /// </param>
        /// <exception cref="ArgumentNullException">
        ///     <paramref name="obj" /> is null.<br />
        ///     -or-<br />
        ///     <paramref name="controlStates" /> is null.
        /// </exception>
        [Obsolete]
        public static void UpdateAttributeValues(
            RepositoryObject obj,
            IDictionary<string, IWebControlState> controlStates,
            bool processReadOnly = false)
        {
            #region Argument Check

            if (obj == null)
            {
                throw new ArgumentNullException("obj");
            }

            if (controlStates == null)
            {
                throw new ArgumentNullException("controlStates");
            }

            #endregion

            foreach (var state in controlStates)
            {
                if ((state.Value.ReadOnly && !processReadOnly) || !obj.Attributes.Contains(state.Key))
                {
                    continue;
                }

                ObjectAttribute attribute = obj.Attributes[state.Key];
                if (!attribute.IsReadOnly)
                {
                    attribute.Value = state.Value.GetControlValue();
                }
            }
        }

        /// <summary>
        ///     Updates revision history like QLM does.
        /// </summary>
        /// <param name="revision">
        ///     New revision data.
        /// </param>
        /// <param name="parentRevision">
        ///     Parent revision data. Can be null if called for new object.
        /// </param>
        public static void UpdateHistoryOfChanges(RepositoryObject revision, RepositoryObject parentRevision)
        {
            #region Argument Check

            if (revision == null)
            {
                throw new ArgumentNullException("revision");
            }

            #endregion

            IAttributeValue historyAttr = null;
            if (!revision.Attributes.TryGetValue(Metamodel.Attributes.HistoryOfChanges, out historyAttr))
            {
                // If object has no such attribute, most probably it is not defined by metamodel.
                // So we do not need to fill it in.
                return;
            }
   
            string historyValue = historyAttr.AsPlainText(null);

            IAttributeValue descriptionAttr = null;
            var descriptionValue =
                revision.Attributes.TryGetValue(Metamodel.Attributes.DescriptionOfChange, out descriptionAttr) 
                ? descriptionAttr.AsPlainText(null)
                : string.Empty;

            string[] parentRevisionInfo = new string[0];

            var splitters = new string[]
            {
                "<br \\>" + Environment.NewLine,
                Environment.NewLine,
                "<br \\>",
                "<br>",
                "<br\\>"
            };

            var newLine =
                parentRevision != null && parentRevision.LocalRevision != LocalRevision.Empty
                ? string.Format(
                    "{0}: Revision {1} created from Revision {2} ({3}) by {4}",
                    DateTime.UtcNow,
                    revision.LocalRevision,
                    parentRevision.LocalRevision,
                    parentRevision.Language,
                    revision.CreatedBy)
                 : string.Format(
                    "{0}: Revision {1} created by {2}",
                    DateTime.UtcNow,
                    revision.LocalRevision,
                    revision.CreatedBy);

            var lines = newLine
                .AsArray()
                .Concat(parentRevisionInfo)
                .Concat(new string[] { "*****", string.Empty })
                .Concat(new string[] { descriptionValue })
                .Concat(historyValue.Split(splitters, StringSplitOptions.None));

            string newHistoryValue = lines.Select(HttpUtility.HtmlEncode).Join("<br \\>");
            revision.Attributes[Metamodel.Attributes.HistoryOfChanges].Value = new XhtmlText(newHistoryValue);
            if (descriptionAttr != null)
            {
                descriptionAttr.SetContent("");
            }
        }


        /// <summary>
        ///     Checks if current user can grab the lock, of locked object.
        /// </summary>
        /// <param name="repository">
        ///     Repository <see cref="IRepository">instance</see>, where locked object is placed.
        /// </param>
        /// <param name="id">
        ///     Identifier of the object, which lock user is grabbing.
        /// </param>
        /// <returns>
        ///     <para><b>true</b> if current user is allowed to grab the lock.</para>
        ///     <para><b>false</b> otherwise.</para>
        /// </returns>
        public static bool CanGrabObjectLock(IRepository repository, Oid id)
        {
            #region Argument Checks

            if (repository == null)
            {
                throw new ArgumentNullException("Repository cannot be null.");
            }

            if (id == null || id == Oid.Empty)
            {
                throw new ArgumentNullException("Revision identifier cannot be null or empty.");
            }

            #endregion

            var lockStatus = repository.GetLockStatus(id);

            return lockStatus.Type == ObjectLockResultType.Locked &&
                lockStatus.UserId == repository.CurrentSession.UserId &&
                lockStatus.SessionId != repository.CurrentSession.Id;
        }

        /// <summary>
        ///     Attempts to grab the lock of the specified object.
        /// </summary>
        /// <param name="repository">
        ///     Repository <see cref="IRepository">instance</see>, where locked object is placed.
        /// </param>
        /// <param name="id">
        ///     Identifier of the object, which lock user is grabbing.
        /// </param>
        public static void GrabObjectLock(IRepository repository, Oid id)
        {
            #region Argument Checks

            if (repository == null)
            {
                throw new ArgumentNullException("Repository cannot be null.");
            }

            if (id == null || id == Oid.Empty)
            {
                throw new ArgumentNullException("Revision identifier cannot be null or empty.");
            }

            #endregion

            var grabResult = repository.GrabObjectLock(id);

            switch (grabResult.Type)
            {
                case ObjectLockResultType.Locked:
                    return;

                case ObjectLockResultType.NotLocked:
                    throw new QInvalidRepositoryOperationException("Object was not previously locked by current user.");

                case ObjectLockResultType.NotLockOwner:
                    throw new QInvalidRepositoryOperationException("Object is locked by another user. Cannot grab others locks.");
            }
        }

        /// <summary>
        ///     Attempts to set the lock of the specified object.
        /// </summary>
        /// <param name="repository">
        ///     Repository <see cref="IRepository">instance</see>, where locked object is placed.
        /// </param>
        /// <param name="id">
        ///     Identifier of the object to be locked.
        /// </param>
        public static void SetObjectLock(IRepository repository, Oid id)
        {
            #region Argument Checks

            if (repository == null)
            {
                throw new ArgumentNullException("Repository cannot be null.");
            }

            if (id == null || id == Oid.Empty)
            {
                throw new ArgumentNullException("Revision identifier cannot be null or empty.");
            }

            #endregion

            var lockResult = repository.LockObject(id);

            switch (lockResult.Type)
            {
                case ObjectLockResultType.Locked:
                    return;

                case ObjectLockResultType.NotLocked:
                    throw new QInvalidRepositoryOperationException("Object can not locked by current user.");

                case ObjectLockResultType.NotLockOwner:
                    throw new QInvalidRepositoryOperationException("Object is locked by another user.");
            }
        }

        /// <summary>
        ///     Attempts to release the lock of the specified object.
        /// </summary>
        /// <param name="repository">
        ///     Repository <see cref="IRepository">instance</see>, where locked object exists.
        /// </param>
        /// <param name="id">
        ///     Identifier of the object, which lock is released.
        /// </param>
        public static void ReleaseObjectLock(IRepository repository, Oid id)
        {
            #region Argument Checks

            if (repository == null)
            {
                throw new ArgumentNullException("Repository cannot be null.");
            }

            if (id == null || id == Oid.Empty)
            {
                throw new ArgumentNullException("Revision identifier cannot be null or empty.");
            }

            #endregion

            var releaseResult = repository.ReleaseObjectLocks(id);

            switch (releaseResult.Type)
            {
                case ObjectLockResultType.NotLocked:
                    return;

                case ObjectLockResultType.Locked:
                    throw new QInvalidRepositoryOperationException("Object was not previously locked by current user.");

                case ObjectLockResultType.NotLockOwner:
                    throw new QInvalidRepositoryOperationException("Object is locked by another user. Cannot release lock.");
            }
        }

        public static IConfiguration CreatePointInTimePWS(
            ConfigurationContext configurationCtx, 
            DateTime validAt,bool relyonaudits)
        {
            IReadOnlyCollection<Oid> selectedRevisions;

            if (!relyonaudits)
                selectedRevisions = SelectRevisionsForPointInTime(configurationCtx, validAt);
            else
                selectedRevisions = SelectRevisionsForPointInTimeComplex(configurationCtx, validAt);

            var repository = configurationCtx.Repository;
            string dat = validAt.Year.ToString("D4") + " " + validAt.Month.ToString("D2") + " " + validAt.Day.ToString("D2");
            var name = $"Point in time({dat})".Replace(':', ' ');
            var previousPointInTime = repository.FindConfiguration(name);
            if (previousPointInTime != null)
            {
                repository.DeleteConfiguration(previousPointInTime.Id);
            }

            var pointInTime = repository.CreateConfiguration(
                configurationCtx.Configuration.Id, 
                name, 
                string.Empty, 
                true);

            selectedRevisions.DoForEach(pointInTime.SetDefaultObjectRevision);

            return pointInTime;
        }

        #endregion
    }

    /// <summary>
    ///     Provides functionality relevant to repository objects.
    /// </summary>
    public static class ObjectScriptsHandlers
    {
        #region Public Methods

        /// <summary>
        ///     Creates a new object revision. If object contains the <see cref="Metamodel.Attributes.RevisionLabel" />
        ///     attribute, it is updated accordingly. After the revision is created, the form  is refreshed.
        /// </summary>
        /// <param name="args">
        ///     ObjectDialog event arguments.
        /// </param>
        /// <returns>
        ///     New object revision.
        /// </returns>
        public static RepositoryObject CreateRevision(ObjectDialogEventArgs args)
        {
            try
            {
                const string configurationIdParam = "NewConfId";

                RepositoryObject repositoryObject = args.Object;
                IConfiguration configuration = repositoryObject.GetConfiguration();

                if (!args.Request[configurationIdParam].IsBlank())
                {
                    Cid configurationId = Cid.Parse(args.Request[configurationIdParam]);

                    IRepository repository = configuration.GetRepository();
                    configuration = repository.FindConfiguration(configurationId);
                }

                RepositoryObject repositoryObjectRevision = ObjectScripts.CreateRevision(
                    configuration,
                    repositoryObject);

                UIScripts.RefreshObjectForEditing(
                    args.Session.Id,
                    repositoryObjectRevision,
                    string.Empty,
                    args.UIActions);

                return repositoryObjectRevision;
            }
            catch (QInvalidRepositoryOperationException invalidScriptOperationException)
            {
                args.Message = invalidScriptOperationException.Message;

                return null;
            }
            catch (QInvalidScriptOperationException invalidScriptOperationException)
            {
                args.Message = invalidScriptOperationException.Message;

                return null;
            }
            catch (Exception exception)
            {
                args.Message = exception.Message + string.Format("\nStack Trace:\n{0}", exception.StackTrace);

                return null;
            }
        }

        /// <summary>
        ///     Freezes the object. After the object is freezed, the form  is refreshed.
        /// </summary>
        /// <param name="args">
        ///     ObjectDialog event arguments.
        /// </param>
        public static void FreezeObject(ObjectDialogEventArgs args)
        {
            try
            {
                ObjectScripts.FreezeObject(args.Object);

                UIScripts.RefreshForm(args.UIActions);
            }
            catch (QInvalidScriptOperationException invalidScriptOperationException)
            {
                args.Message = invalidScriptOperationException.Message;

                return;
            }
            catch (Exception exception)
            {
                args.Message = exception.Message + string.Format("\nStack Trace:\n{0}", exception.StackTrace);

                return;
            }
        }

        [ObjectCreating]
        [ObjectRevisionCreating]
        [ObjectLanguageVariantCreating]
        public static void OnHistoryOfChangesUpdate(ObjectPreEventArgs args)
        {
            var oldRevision =
                args.NewRevision.Id != args.ObjectId
                    ? args.Repository.FindObject(
                        args.ObjectId,
                        ObjectInstantiationLevel.AuditAndConfiguration)
                    : null;

            ObjectScripts.UpdateHistoryOfChanges(args.NewRevision, oldRevision);
        }

        /// <summary>
        ///     Sets the current object revision to default.
        /// </summary>
        /// <param name="args">
        ///     ObjectDialog event arguments.
        /// </param>
        public static void SetDefault(ObjectDialogEventArgs args)
        {
            try
            {
                args.Configuration.SetDefaultObjectRevision(args.Object.Id);
            }
            catch (QInvalidScriptOperationException invalidScriptOperationException)
            {
                args.Message = invalidScriptOperationException.Message;
            }
            catch (Exception exception)
            {
                args.Message = exception.Message + string.Format("\nStack Trace:\n{0}", exception.StackTrace);
            }
        }

        public static void CanGrabObjectLock(GeneralScriptArgs args)
        {
            #region Argument Checks
            
            if (args.Parameters.Take(2).Any(p => p.IsBlank()))
            {
                throw new ArgumentException("Method arguments should contain object identifier and revision identifier.");
            }

            #endregion

            try
            {
                var id = new Oid(ObjectId.Parse(args.Parameters[0]), RevisionId.Parse(args.Parameters[1]));

                args.Result = ObjectScripts.CanGrabObjectLock(args.Configuration.GetRepository(), id) ? "1" : "0";
            }
            catch(Exception ex)
            {
                args.Message = ex.BuildMessage();
            }
        }

        public static void GrabObjectLock(GeneralScriptArgs args)
        {
            #region Argument Checks

            if (args.Parameters.Take(2).Any(p => p.IsBlank()))
            {
                throw new ArgumentException("Method arguments should contain object identifier and revision identifier.");
            }

            #endregion

            try
            {
                var id = new Oid(ObjectId.Parse(args.Parameters[0]), RevisionId.Parse(args.Parameters[1]));

                ObjectScripts.GrabObjectLock(args.Configuration.GetRepository(), id);
                args.Result = "1";
            }
            catch(Exception ex)
            {
                args.Message = ex.BuildMessage();
                args.Result = "0";
            }
        }

        public static void CreatePointInTimePWS(GeneralScriptArgs args)
        {
            try
            {
                bool relyonaudits = false;
                var dateStr = args.Parameters[0];
                if (args.Parameters.Count > 1 && !string.IsNullOrEmpty(args.Parameters[1]))
                    relyonaudits = true;
                relyonaudits = false; //not ready yet
                if (dateStr.IsBlank())
                {
                    throw new ArgumentException("To create point-in-time configuration a valid date should be specified.");
                }

                DateTime date;
                if (!DateTime.TryParse(dateStr, out date))
                {
                    throw new ArgumentException($"String {dateStr} does not contain a valid date.");
                }

                var configurationCtx = ConfigurationContext.Create(args.Configuration);

                ObjectScripts.CreatePointInTimePWS(configurationCtx, date, relyonaudits);
            }
            catch(Exception ex)
            {
                args.Message = ex.BuildMessage();
                args.Result = "0";
            }
        }

        #endregion
    }

    public sealed class RepositoryObjectByIdComparer : IEqualityComparer<RepositoryObject>
    {
        #region Fields

        private static readonly RepositoryObjectByIdComparer s_instance = new RepositoryObjectByIdComparer();

        #endregion

        #region Constructors and Destructors

        private RepositoryObjectByIdComparer()
        {
            // Nothing to do
        }

        #endregion

        #region Public Properties

        public static RepositoryObjectByIdComparer Instance
        {
            [DebuggerStepThrough]
            get
            {
                return s_instance;
            }
        }

        #endregion

        #region IEqualityComparer<RepositoryObject> Members

        public bool Equals(RepositoryObject x, RepositoryObject y)
        {
            if (ReferenceEquals(x, y))
            {
                return true;
            }
            if (ReferenceEquals(x, null) || ReferenceEquals(y, null))
            {
                return false;
            }

            return x.Id == y.Id;
        }

        public int GetHashCode(RepositoryObject obj)
        {
            return ReferenceEquals(obj, null) ? 0 : obj.Id.GetHashCodeSafely();
        }

        #endregion
    }

}
