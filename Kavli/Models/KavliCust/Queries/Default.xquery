﻿<?xml version="1.0" encoding="utf-8" ?>
<!-- This query is used for interacting with Object Selector control in default mode
when embedded in Repositry Explorer. -->
<webQuery xmlns="http://qualiware.com/qis/query">
  <listViewSettings>
    <repositoryExplorerView>
      <columns>
        <addColumn name="Name" 
          showAsPlainText="false"
          headerText="Name"
          allowFiltering="true"
          wrapHeader="true" 
          wrapItem="true"
          width="30%" />
        <addColumn name="sys_Created" 
          headerText="Created"
          allowFiltering="true"
          wrapHeader="true" 
          wrapItem="true"
          width="15%" />
        <addColumn name="sys_Modified" 
          headerText="Modified"
          allowFiltering="true"
          wrapHeader="true" 
          wrapItem="true"
          width="15%" />
        <addColumn name="Description" 
          headerText="Description"
          allowFiltering="true"
          wrapHeader="true" 
          wrapItem="true"
          width="40%" />
      </columns>
      <viewButtons>
        <addViewButton id="create" tooltip="Add new object"/>
        <addViewButton id="edit" tooltip="Edit selected object"/>
        <addViewButton id="rename" tooltip="Rename selected object"/>
        <addViewButton id="delete" tooltip="Delete selected object(s)"/>
      </viewButtons>
      <fromRepository/>
    </repositoryExplorerView>

    <repositoryExplorerSelect>
      <columns>
        <addColumn name="Name" 
          headerText="Name"
	  showAsPlainText="true"
          allowFiltering="true"
          wrapHeader="true" 
          wrapItem="true"
          width="100%" />
      </columns>
      <selectButtons>
        <addSelectButton id="select" tooltip="Use selected object(s)"/>
        <addSelectButton id="create" tooltip="Add new object"/>
        <addSelectButton id="edit" tooltip="Edit selected object"/>
        <addSelectButton id="rename" tooltip="Rename selected object"/>
        <addSelectButton id="delete" tooltip="Delete selected object(s)"/>
        <addSelectButton id="close" tooltip="Cancel changes"/>
      </selectButtons>
      <fromRepository/>
    </repositoryExplorerSelect>

    <multiLinkView>
      <columns>
        <addColumn name="Name"
				  headerText="Name"
				  showAsPlainText="true"
				  allowFiltering="true"
				  wrapHeader="true" 
				  wrapItem="true"
				  width="50%" />
        <addColumn name="Template"
				  headerText="Template"
				  allowFiltering="true"
				  wrapHeader="true" 
				  wrapItem="true"
				  width="50%" />
      </columns>
      <viewButtons>
        <addViewButton id="create" tooltip="Add new object"/>
        <addViewButton id="edit" tooltip="Edit selected object"/>
        <addViewButton id="insert" tooltip="Insert existing object(s)"/>
        <addViewButton id="remove" tooltip="Remove selected object(s)"/>
      </viewButtons>
    </multiLinkView> 
  </listViewSettings>
</webQuery>
