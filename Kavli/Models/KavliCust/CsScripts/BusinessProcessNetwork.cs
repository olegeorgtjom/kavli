using QCommon.Extensions;
using Qis.Common.Scripting.Events.EventHandlerArguments;
using Qis.Common.Scripting.Events.EventHandlerAttributes;

namespace Qis.Module.Scripts
{
    public class BusinessProcessNetwork
    {
        [RepositoryConnected]
        public static void AddSubTemplates(RepositoryConnectedEventArgs args)
        {
            DynamicMetamodel.AddTemplateParent(
                args.Repository.Metamodel,
                "BusinessProcessNetwork",
                "subProcessHACCP",
                new TabDisplayName { Name = "subProcessHACCP", DisplayName = "HACCP" }.AsCollection());
        }
    }

}