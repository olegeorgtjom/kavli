using Qis.Common;
using Qis.Web.Forms.Common.Scripting;


namespace Qis.Module.Scripts
{
    public static class HACCPQRVScripts
    {
        public static RepositoryObjectList HACCPAnalysisForObject(FilterScriptEventArgs args)
        {
            RepositoryObject currInst = args.ScriptContext.Object;
            IConfiguration config = args.Configuration;
            if (currInst == null)
            {
                string objId = args.Request["ObjId"];
                currInst = args.Configuration.FindObject(new Oid(objId));
            }

            RepositoryObjectList list = ObjectScripts.GetBackAttributeRelations(config, currInst.Id, "FoodSafetyHazardConcerns");
            return list;
        }
        public static RepositoryObjectList Revisions(FilterScriptEventArgs args)
        {
            RepositoryObject currInst = args.ScriptContext.Object;
            if (currInst == null)
            {
                string objId = args.Request["ObjId"];
                currInst = args.Configuration.FindObject(new Oid(objId));
            }

            if (currInst == null)
                return new RepositoryObjectList();

            return args.Configuration.FindRevisions(currInst.Id);
        }
    }

}